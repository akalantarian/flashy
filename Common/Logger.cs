﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;

public static class Logger
{
    private static object lockObject;
    private static FileInfo file, exceptionFile;
    private static StreamWriter stream;
    private static MessageQueue<string> consoleQueue;

    private static DateTime startTime { get { return Process.GetCurrentProcess().StartTime; } }

    public static void Init(FileInfo logFile, FileInfo exceptionLogFile, bool consoleEnabled)
    {
        lockObject = new object();
        if(logFile != null)
        {
            file = logFile;
            file.Directory.Create();
            stream = new StreamWriter(file.Open(FileMode.Create, FileAccess.Write, FileShare.Read), Encoding.ASCII);
            stream.WriteLine(startTime.ToString("MM/dd/yyyy HH:mm:ss.fffffff"));
            stream.Flush();
        }
        if(exceptionLogFile != null)
        {
            exceptionFile = exceptionLogFile;
            exceptionFile.Directory.Create();
            AppDomain.CurrentDomain.UnhandledException += exceptionLogging;
        }
        if(consoleEnabled)
        {
            consoleQueue = new MessageQueue<string>();
            consoleQueue.Process += Console.WriteLine;
            consoleQueue.Start();
        }
    }

    private static void exceptionLogging(object sender, UnhandledExceptionEventArgs e)
    {
        AppDomain.CurrentDomain.UnhandledException -= exceptionLogging;
        using(StreamWriter exceptionStream = new StreamWriter(exceptionFile.Open(FileMode.Create, FileAccess.Write, FileShare.Read), Encoding.ASCII))
        {
            exceptionStream.WriteLine(startTime.ToString("MM/dd/yyyy HH:mm:ss.fffffff"));
            tryLogAppdomain(sender, exceptionStream);
            tryLogException(e, exceptionStream);
        }
        stream.Dispose();
    }
    private static void tryLogAppdomain(object sender, StreamWriter writer)
    {
        AppDomain appDomain = sender as AppDomain;
        if(appDomain != null)
            writer.WriteLine(appDomain.FriendlyName);
        else if(sender != null)
            writer.Write(sender.ToString());
    }
    private static void tryLogException(UnhandledExceptionEventArgs e, StreamWriter writer)
    {
        Exception exception = e.ExceptionObject as Exception;
        if(exception != null)
            logException(exception, writer);
        else if(e.ExceptionObject != null)
            writer.Write(e.ExceptionObject.ToString());
    }
    private static void logException(Exception e, StreamWriter writer)
    {
        writer.WriteLine(e.GetType());
        writer.WriteLine(e.Message);
        writer.WriteLine(e.StackTrace);
        if(e.InnerException != null)
        {
            writer.WriteLine();
            logException(e.InnerException, writer);
        }
    }

    public static void Log(string str)
    {
        if(lockObject == null) return;
        lock (lockObject)
        {
            str = DateTime.UtcNow.ToString("MM/dd/yyyy HH:mm:ss.fffffff") + "\t" + str;
            if(stream != null)
                stream.WriteLine(str);
            if(consoleQueue != null)
                consoleQueue.Enqueue(str);
        }
    }
}