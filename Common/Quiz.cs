﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Sockets;

namespace Flashy
{
    internal enum Session : ushort
    {
        All = 0x3FF,
        _0 = 1 << 0,
        _1 = 1 << 1,
        _2 = 1 << 2,
        _3 = 1 << 3,
        _4 = 1 << 4,
        _5 = 1 << 5,
        _6 = 1 << 6,
        _7 = 1 << 7,
        _8 = 1 << 8,
        _9 = 1 << 9,
    }
    internal sealed class Deck
    {
        public static readonly Deck Current, deck0, deck1, deck2, deck3, deck4, deck5, deck6, deck7, deck8, deck9;
        static Deck()
        {
            Current = new Deck(Session.All);
            deck0 = new Deck(Session._2 | Session._5 | Session._9, Session._9, new Deck(Session._4 | Session._9));
            deck1 = new Deck(Session._3 | Session._6 | Session._0, Session._0, new Deck(Session._5 | Session._0));
            deck2 = new Deck(Session._4 | Session._7 | Session._1, Session._1, new Deck(Session._6 | Session._1));
            deck3 = new Deck(Session._5 | Session._8 | Session._2, Session._2, new Deck(Session._7 | Session._2));
            deck4 = new Deck(Session._6 | Session._9 | Session._3, Session._3, new Deck(Session._8 | Session._3));
            deck5 = new Deck(Session._7 | Session._0 | Session._4, Session._4, new Deck(Session._9 | Session._4));
            deck6 = new Deck(Session._8 | Session._1 | Session._5, Session._5, new Deck(Session._0 | Session._5));
            deck7 = new Deck(Session._9 | Session._2 | Session._6, Session._6, new Deck(Session._1 | Session._6));
            deck8 = new Deck(Session._0 | Session._3 | Session._7, Session._7, new Deck(Session._2 | Session._7));
            deck9 = new Deck(Session._1 | Session._4 | Session._8, Session._8, new Deck(Session._3 | Session._8));
        }

        #region Static
        internal static Deck Decode(byte[] data) { return Decode(data, Encoding.ASCII); }
        internal static Deck Decode(byte[] data, Encoding encoding) { return Decode(new MemoryStream(data), encoding); }
        internal static Deck Decode(Stream stream) { return Decode(stream, Encoding.ASCII); }
        internal static Deck Decode(Stream stream, Encoding encoding) { return Decode(new BinaryReader(stream, encoding)); }
        internal static Deck Decode(BinaryReader reader)
        {
            switch(reader.ReadByte())
            {
                case 0: return deck0;
                case 1: return deck1;
                case 2: return deck2;
                case 3: return deck3;
                case 4: return deck4;
                case 5: return deck5;
                case 6: return deck6;
                case 7: return deck7;
                case 8: return deck8;
                case 9: return deck9;

                case 10: return deck0.nextDeck;
                case 11: return deck1.nextDeck;
                case 12: return deck2.nextDeck;
                case 13: return deck3.nextDeck;
                case 14: return deck4.nextDeck;
                case 15: return deck5.nextDeck;
                case 16: return deck6.nextDeck;
                case 17: return deck7.nextDeck;
                case 18: return deck8.nextDeck;
                case 19: return deck9.nextDeck;

                default: return Current;
            }
        }
        internal static byte[] Encode(Deck deck) { return Encode(deck, Encoding.ASCII); }
        internal static byte[] Encode(Deck deck, Encoding encoding) { MemoryStream stream = new MemoryStream(); Encode(deck, stream, encoding); stream.Close(); return stream.ToArray(); }
        internal static void Encode(Deck deck, Stream stream) { Encode(deck, stream, Encoding.ASCII); }
        internal static void Encode(Deck deck, Stream stream, Encoding encoding) { Encode(deck, new BinaryWriter(stream, encoding)); }
        internal static void Encode(Deck deck, BinaryWriter writer)
        {
            if(deck == deck0) writer.Write((byte)0);
            else if(deck == deck1) writer.Write((byte)1);
            else if(deck == deck2) writer.Write((byte)2);
            else if(deck == deck3) writer.Write((byte)3);
            else if(deck == deck4) writer.Write((byte)4);
            else if(deck == deck5) writer.Write((byte)5);
            else if(deck == deck6) writer.Write((byte)6);
            else if(deck == deck7) writer.Write((byte)7);
            else if(deck == deck8) writer.Write((byte)8);
            else if(deck == deck9) writer.Write((byte)9);

            else if(deck == deck0.nextDeck) writer.Write((byte)10);
            else if(deck == deck1.nextDeck) writer.Write((byte)11);
            else if(deck == deck2.nextDeck) writer.Write((byte)12);
            else if(deck == deck3.nextDeck) writer.Write((byte)13);
            else if(deck == deck4.nextDeck) writer.Write((byte)14);
            else if(deck == deck5.nextDeck) writer.Write((byte)15);
            else if(deck == deck6.nextDeck) writer.Write((byte)16);
            else if(deck == deck7.nextDeck) writer.Write((byte)17);
            else if(deck == deck8.nextDeck) writer.Write((byte)18);
            else if(deck == deck9.nextDeck) writer.Write((byte)19);

            else writer.Write((byte)20);
        }
        #endregion

        public static Deck EntranceDeck(Session session)
        {
            switch(session)
            {
                case Session._0: return deck0;
                case Session._1: return deck1;
                case Session._2: return deck2;
                case Session._3: return deck3;
                case Session._4: return deck4;
                case Session._5: return deck5;
                case Session._6: return deck6;
                case Session._7: return deck7;
                case Session._8: return deck8;
                case Session._9: return deck9;
                default: return null;
            }
        }

        private readonly Session sessions;
        private readonly Session? lastSession;
        private readonly Deck nextDeck;
        private Deck(Session sessions, Session? lastSession = null, Deck nextDeck = null)
        {
            this.sessions = sessions;
            this.lastSession = lastSession;
            this.nextDeck = nextDeck;
        }

        public bool InSession(Session session) { return sessions.HasFlag(session); }
        public bool InSession(Session session, out bool isLast)
        {
            if(lastSession.HasValue && session == lastSession) { isLast = true; return true; }
            else if(sessions.HasFlag(session)) { isLast = false; return true; }
            else { isLast = false; return false; }
        }
        public Deck Next { get { return nextDeck; } }
    }
    internal sealed class QuizCard
    {

        #region Variables
        private readonly ReadOnlyCollection<string> libraryList;
        private readonly byte libraryIndex;
        private readonly CardIDData data;
        private Deck deck;
        #endregion

        #region Accessors
        public string Library { get { return libraryList[libraryIndex]; } }
        public uint ID { get { return data.ID; } }
        public CardData Data { get { return data.Data; } }
        public Deck Deck { get { return deck; } set { deck = value; } }
        #endregion

        #region Constructor
        public QuizCard(ReadOnlyCollection<string> libraryList, byte libraryIndex, CardIDData data, Deck deck)
        {
            this.libraryList = libraryList;
            this.libraryIndex = libraryIndex;
            this.data = data;
            this.deck = deck;
        }
        #endregion

        #region Serialization
        internal QuizCard(byte[] data) : this(data, Encoding.ASCII) { }
        internal QuizCard(byte[] data, Encoding encoding) : this(new MemoryStream(data), encoding) { }
        internal QuizCard(Stream stream) : this(stream, Encoding.ASCII) { }
        internal QuizCard(Stream stream, Encoding encoding) : this(new BinaryReader(stream, encoding)) { }
        internal QuizCard(BinaryReader reader)
        {
        }
        internal void Write(BinaryWriter writer)
        {

        }
        #endregion

        #region Functions
        public bool InSession(Session session) { return deck.InSession(session); }
        public void Pass(Session session)
        {
            if(deck == Deck.Current) deck = Deck.EntranceDeck(session);
            else if(deck.Next != null) deck = deck.Next;
        }
        public void Fail()
        {
            deck = Deck.Current;
        }
        #endregion
    }
    public sealed class Quiz
    {
        #region Variables
        private readonly List<string> libraries;
        private readonly Filter filter;
        private readonly SortedSet<QuizCard> cards;
        private readonly Session session;
        #endregion

        #region Serialization
        internal Quiz(byte[] data) : this(data, Encoding.ASCII) { }
        internal Quiz(byte[] data, Encoding encoding) : this(new MemoryStream(data), encoding) { }
        internal Quiz(Stream stream) : this(stream, Encoding.ASCII) { }
        internal Quiz(Stream stream, Encoding encoding) : this(new BinaryReader(stream, encoding)) { }
        internal Quiz(BinaryReader reader)
        {

        }
        internal void Write(BinaryWriter writer)
        {
            
        }
        #endregion
    }
}
