﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Sockets;

#if SERVER
namespace Flashy.Server
#elif CLIENT
namespace Flashy.Client.Editor
#endif
{
    public class Cache<T> where T : class
    {
        #region Events
        public event Action<CacheNode<T>> OnRemove;
        private void onRemove(CacheNode<T> node) { Action<CacheNode<T>> evt = OnRemove; if(evt != null) evt(node); }
        #endregion

        #region Subclasses
        public class CacheNode<Q> where Q : class
        {
            private readonly Q value;
            private CacheNode<Q> next;
            private CacheNode<Q> prev;
            private Cache<Q> list;

            internal CacheNode<Q> Next { get { return next; } set { next = value; } }
            internal CacheNode<Q> Prev { get { return prev; } set { prev = value; } }
            internal Cache<Q> List { get { return list; } set { list = value; } }
            public Q Value { get { return value; } }

            public CacheNode(Q value) { if(value == null) throw new ArgumentNullException("value"); this.value = value; this.next = this.prev = null; this.list = null; }
        }
        #endregion

        #region Variables
        private CacheNode<T> first, last;
        private int count;
        private int capacity;
        #endregion

        #region Accessors
        public int Capacity { get { return capacity; } set { if(value <= 0) throw new ArgumentOutOfRangeException("capacity <= 0"); capacity = value; } }
        public int Count { get { return count; } }
        #endregion

        #region Constructor
        public Cache(int capacity = 10000)
        {
            if(capacity <= 0) throw new ArgumentOutOfRangeException("capacity <= 0");

            this.first = this.last = null;
            this.count = 0;
            this.capacity = capacity;
        }
        #endregion

        #region Functions
        #region Private
        private CacheNode<T> removeLast()
        {
            CacheNode<T> node = last;
            if(count == 1)
            {
                first = last = null;
            }
            else
            {
                last.Prev.Next = null;
                last = last.Prev;
                node.Prev = null;
            }
            node.List = null;
            count--;
            return node;
        }
        private void addFirst(CacheNode<T> node)
        {
            if(count == 0)
            {
                last = first = node;
            }
            else
            {
                first.Prev = node;
                node.Next = first;
                first = node;
            }
            node.List = this;
            count++;
        }
        private void remove(CacheNode<T> node)
        {
            if(count == 1)
            {
                first = last = null;
            }
            else if(node.Prev == null)
            {
                node.Next.Prev = null;
                first = node.Next;
                node.Next = null;
            }
            else if(node.Next == null)
            {
                node.Prev.Next = null;
                last = node.Prev;
                node.Prev = null;
            }
            else
            {
                node.Prev.Next = node.Next;
                node.Next.Prev = node.Prev;
                node.Next = node.Prev = null;
            }
            node.List = null;
            count--;
        }
        #endregion

        #region Public
        public CacheNode<T> Add(T item)
        {
            CacheNode<T> node = new CacheNode<T>(item);
            Add(node);
            return node;
        }
        public void Add(CacheNode<T> node)
        {
            if(node == null) throw new ArgumentNullException("node");
            if(node.List != null && node.List != this) throw new ArgumentException("node.List != null && node.List != this");

            if(node.List == this)
            {
                if(first == node)
                {
                    while(count > capacity) onRemove(removeLast());
                    return;
                }
                remove(node);
            }
            while(count > capacity - 1) onRemove(removeLast());
            addFirst(node);
        }
        public void Remove(CacheNode<T> node)
        {
            if(node == null) throw new ArgumentNullException("node");
            if(node.List != null && node.List != this) throw new ArgumentException("node.List != null && node.List != this");
            if(node.List == this)
                remove(node);
            onRemove(node);
            while(count > capacity) onRemove(removeLast());
        }
        #endregion
        #endregion
    }

    internal class CachedCardData
    {
        #region Variables
        private readonly CardCache.CardCacheNode node;
        private readonly CardData data;
        #endregion

        #region Accessors
        public CardCache.CardCacheNode Node { get { return node; } }
        public Card Card { get { return node.Value; } }
        public CardData Data { get { return data; } }
        #endregion

        #region Constructor
        public CachedCardData(Card card, CardData data)
        {
            this.node = new CardCache.CardCacheNode(card);
            this.data = data;
        }
        #endregion
    }
    internal class CardCache : Cache<Card>
    {
        public class CardCacheNode : CacheNode<Card>
        {
            private readonly Card card;
            public Card Card { get { return card; } }

            public CardCacheNode(Card value) : base(value) { this.card = value; }
        }
        public CardCache(int capacity = 10000) : base(capacity) { }
    }
}