﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.ComponentModel;
using System.Threading;


public static class Extensions
{
    public static ICollection<string> IntersectionIgnoreCase(this IEnumerable<string> source, IEnumerable<string> target)
    {
        List<string> intersection = new List<string>();
        foreach(string str in source)
            if(!target.ContainsIgnoreCase(str))
                intersection.Add(str);
        foreach(string str in target)
            if(!source.ContainsIgnoreCase(str))
                intersection.Add(str);
        return intersection;
    }

    public static bool ContainsIgnoreCase(this IEnumerable<string> list, string target)
    {
        target = target.ToLower();
        foreach(string str in list)
            if(str.ToLower() == target)
                return true;
        return false;
    }


    public static void invokeAsync(this DispatcherObject dispatcherObject, Action action)
    {
        Dispatcher dispatcher = dispatcherObject.Dispatcher;
        if(dispatcher == null) { action(); return; }
        if(dispatcher.Thread != Thread.CurrentThread)
            dispatcher.BeginInvoke(action);
        else
            action();
    }
    public static void invokeAsync(this object obj, Dispatcher dispatcher, Action action)
    {
        if(dispatcher == null) { action(); return; }
        if(dispatcher.Thread != Thread.CurrentThread)
            dispatcher.BeginInvoke(action);
        else
            action();
    }
    public static void invoke(this DispatcherObject dispatcherObject, Action action)
    {
        Dispatcher dispatcher = dispatcherObject.Dispatcher;
        if(dispatcher == null) { action(); return; }
        if(dispatcher.Thread != Thread.CurrentThread)
            dispatcher.Invoke(action);
        else
            action();
    }
    public static void invoke(this object obj, Dispatcher dispatcher, Action action)
    {
        if(dispatcher == null) { action(); return; }
        if(dispatcher.Thread != Thread.CurrentThread)
            dispatcher.Invoke(action);
        else
            action();
    }
    public static void propertyChangedAsync(this DispatcherObject dispatcherObject, PropertyChangedEventHandler handler, params string[] properties)
    {
        propertyChangedAsync((object)dispatcherObject, dispatcherObject.Dispatcher, dispatcherObject, handler, properties);
    }
    public static void propertyChangedAsync(this DispatcherObject dispatcherObject, object owner, PropertyChangedEventHandler handler, params string[] properties)
    {
        propertyChangedAsync((object)dispatcherObject, dispatcherObject.Dispatcher, owner, handler, properties);
    }
    public static void propertyChangedAsync(this object obj, Dispatcher dispatcher, PropertyChangedEventHandler handler, params string[] properties)
    {
        propertyChangedAsync(obj, dispatcher, obj, handler, properties);
    }
    public static void propertyChangedAsync(this object obj, Dispatcher dispatcher, object owner, PropertyChangedEventHandler handler, params string[] properties)
    {
        if(handler == null || dispatcher == null) return;
        if(dispatcher.Thread != Thread.CurrentThread)
            dispatcher.BeginInvoke(new Action(() =>
            {
                foreach(string property in properties)
                    handler(owner, new PropertyChangedEventArgs(property));
            }));
        else
            foreach(string property in properties)
                handler(owner, new PropertyChangedEventArgs(property));
    }

    public static void propertyChanged(this DispatcherObject dispatcherObject, PropertyChangedEventHandler handler, params string[] properties)
    {
        propertyChangedAsync((object)dispatcherObject, dispatcherObject.Dispatcher, dispatcherObject, handler, properties);
    }
    public static void propertyChanged(this DispatcherObject dispatcherObject, object owner, PropertyChangedEventHandler handler, params string[] properties)
    {
        propertyChanged((object)dispatcherObject, dispatcherObject.Dispatcher, owner, handler, properties);
    }
    public static void propertyChanged(this object obj, Dispatcher dispatcher, PropertyChangedEventHandler handler, params string[] properties)
    {
        propertyChanged(obj, dispatcher, obj, handler, properties);
    }
    public static void propertyChanged(this object obj, Dispatcher dispatcher, object owner, PropertyChangedEventHandler handler, params string[] properties)
    {
        if(handler == null || dispatcher == null) return;
        if(dispatcher.Thread != Thread.CurrentThread)
            dispatcher.Invoke(new Action(() =>
            {
                foreach(string property in properties)
                    handler(owner, new PropertyChangedEventArgs(property));
            }));
        else
            foreach(string property in properties)
                handler(owner, new PropertyChangedEventArgs(property));
    }
}