﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Sockets;

namespace Flashy.Network
{
    public static class NetworkInfo
    {
        //public static bool IsConnected()
        //{
        //    return NetworkInterface.GetIsNetworkAvailable();
        //}

        public static IPAddress GetLocalIP()
        {
            //if (!IsConnected())
             //   return null;
            try
            {
                foreach (IPAddress ip in Dns.GetHostEntry(Dns.GetHostName()).AddressList)
                {
                    if (ip.AddressFamily == AddressFamily.InterNetwork)
                        return ip;
                }
            }
            catch (Exception) { }
            return null;
        }

        public static IPAddress GetPublicIP()
        {
            //if (!IsConnected())
            //    return null;

            string url = "http://checkip.dyndns.org";
            WebRequest req = System.Net.WebRequest.Create(url);
            WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] a = response.Split(':');
            string a2 = a[1].Substring(1);
            string[] a3 = a2.Split('<');
            string a4 = a3[0];
            return GetResolvedAddress(a4);
        }

        public static IPAddress GetResolvedAddress(string address)
        {
            //if (!IsConnected())
             //   return null;
            try
            {
                foreach (IPAddress ip in Dns.GetHostEntry(address).AddressList)
                {
                    if (ip.AddressFamily == AddressFamily.InterNetwork)
                        return ip;
                }
            }
            catch (Exception) { }
            return null;
        }
    }

    public sealed class TCPServer : IDisposable
    {
        #region Events
        public event Action<TCPConnection> OnConnect;
        public event Action<Exception> OnConnectFail;
        private void onConnect(TCPConnection connection) { Action<TCPConnection> evt = OnConnect; if (evt != null) evt(connection); }
        private void onConnectFail(Exception e) { Action<Exception> evt = OnConnectFail; if (evt != null) evt(e); }
        #endregion

        #region Variables
        private readonly Socket socket;
        private readonly IPEndPoint localEndPoint;
        private readonly int backlog;
        private readonly int reconnectTime;
        private readonly int timeoutLength;
        private readonly int maxTimeouts;
        private readonly ManualResetEvent disposeEvt, pauseEvt;
        private readonly AutoResetEvent forceEvt;
        private readonly Task task;
        #endregion

        #region Accessors
        public IPEndPoint LocalEndPoint { get { return localEndPoint; } }
        public int Backlog { get { return backlog; } }
        public int ReconnectTime { get { return reconnectTime; } }
        public int TimeoutLength { get { return timeoutLength; } }
        public int MaxTimeouts { get { return maxTimeouts; } }
        public bool Disposed { get { return disposeEvt.WaitOne(0); } }
        #endregion

        #region Constructor
        public TCPServer(IPEndPoint localEndPoint, int backlog = 100, int reconnectTime = 5000, int timeoutLength = 5000, int maxTimeouts = 1)
        {
            if (localEndPoint == null) throw new ArgumentNullException("localEndPoint");
            if (backlog <= 0) throw new ArgumentOutOfRangeException("backlog <= 0");
            if (reconnectTime < 0) throw new ArgumentOutOfRangeException("reconnectTime < 0");
            if (timeoutLength < 0) throw new ArgumentOutOfRangeException("timeoutLength < 0");
            if (maxTimeouts < 0) throw new ArgumentOutOfRangeException("maxTimeouts < 0");

            this.socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            this.socket.Bind(localEndPoint);
            this.localEndPoint = localEndPoint;
            this.backlog = backlog;
            this.reconnectTime = reconnectTime;
            this.timeoutLength = timeoutLength;
            this.maxTimeouts = maxTimeouts;
            this.disposeEvt = new ManualResetEvent(false);
            this.pauseEvt = new ManualResetEvent(true);
            this.forceEvt = new AutoResetEvent(false);
            this.task = new Task(connectionTaskProc, TaskCreationOptions.LongRunning);
        }
        ~TCPServer() { Dispose(); }
        #endregion

        #region Functions
        public void Dispose()
        {
            lock (task)
                if (disposeEvt != null) disposeEvt.Set();
        }

        public void Start()
        {
            lock (task)
                if (!disposeEvt.WaitOne(0) && task.Status == TaskStatus.Created)
                    task.Start();
        }

        public void Force()
        {
            forceEvt.Set();
        }
        public void Pause()
        {
            pauseEvt.Reset();
        }
        public void UnPause()
        {
            pauseEvt.Set();
        }

        private void connectionTaskProc()
        {
            TCPConnection newConnection;
            WaitHandle[] forceWait = new WaitHandle[] { disposeEvt, forceEvt };
            WaitHandle[] pauseWait = new WaitHandle[] { disposeEvt, pauseEvt };
            socket.Listen(backlog);
            while (!disposeEvt.WaitOne(0))
            {
                if (WaitHandle.WaitAny(pauseWait) == 0)
                    return;

                try
                {
                    newConnection = new TCPConnection(socket.Accept(), timeoutLength, maxTimeouts);
                }
                catch (Exception e)
                {
                    onConnectFail(e);
                    if (WaitHandle.WaitAny(forceWait, reconnectTime) == 0)
                        return;
                    continue;
                }
                onConnect(newConnection);
            }
        }
        #endregion
    }

    public sealed class TCPClient : IDisposable
    {
        #region Events
        public event Action<TCPConnection> OnConnect;
        public event Action<Exception> OnConnectFail;
        private void onConnect(TCPConnection connection) { Action<TCPConnection> evt = OnConnect; if (evt != null) evt(connection); }
        private void onConnectFail(Exception e) { Action<Exception> evt = OnConnectFail; if (evt != null) evt(e); }
        #endregion

        #region Variables
        private readonly IPEndPoint remoteEndPoint;
        private readonly int reconnectTime;
        private readonly int timeoutLength;
        private readonly int maxTimeouts;
        private readonly ManualResetEvent disposeEvt, pauseEvt;
        private readonly AutoResetEvent forceEvt, reconnectEvt;
        private readonly Task task;
        #endregion

        #region Accessors
        public IPEndPoint RemoteEndPoint { get { return remoteEndPoint; } }
        public int ReconnectTime { get { return reconnectTime; } }
        public int TimeoutLength { get { return timeoutLength; } }
        public int MaxTimeouts { get { return maxTimeouts; } }
        public bool Disposed { get { return disposeEvt.WaitOne(0); } }
        #endregion

        #region Constructor
        public TCPClient(IPEndPoint remoteEndPoint, int reconnectTime = 5000, int timeoutLength = 5000, int maxTimeouts = 1)
        {
            if (remoteEndPoint == null) throw new ArgumentNullException("remoteEndPoint");
            if (reconnectTime < 0) throw new ArgumentOutOfRangeException("reconnectTime < 0");
            if (timeoutLength < 0) throw new ArgumentOutOfRangeException("timeoutLength < 0");
            if (maxTimeouts < 0) throw new ArgumentOutOfRangeException("maxTimeouts < 0");

            this.remoteEndPoint = remoteEndPoint;
            this.reconnectTime = reconnectTime;
            this.timeoutLength = timeoutLength;
            this.maxTimeouts = maxTimeouts;
            this.disposeEvt = new ManualResetEvent(false);
            this.pauseEvt = new ManualResetEvent(true);
            this.forceEvt = new AutoResetEvent(false);
            this.reconnectEvt = new AutoResetEvent(true);
            this.task = new Task(connectionTaskProc, TaskCreationOptions.LongRunning);
        }
        ~TCPClient() { Dispose(); }
        #endregion

        #region Functions
        public void Dispose()
        {
            lock (task)
                if (disposeEvt != null) disposeEvt.Set();
        }

        public void Start()
        {
            lock (task)
                if (!disposeEvt.WaitOne(0) && task.Status == TaskStatus.Created)
                    task.Start();
        }

        public void Force()
        {
            forceEvt.Set();
        }
        public void Pause()
        {
            pauseEvt.Reset();
        }
        public void UnPause()
        {
            pauseEvt.Set();
        }

        private void connectionTaskProc()
        {
            TCPConnection newConnection;
            WaitHandle[] reconnectWait = new WaitHandle[] { disposeEvt, reconnectEvt };
            WaitHandle[] forceWait = new WaitHandle[] { disposeEvt, forceEvt };
            WaitHandle[] pauseWait = new WaitHandle[] { disposeEvt, pauseEvt };
            while (!disposeEvt.WaitOne(0))
            {
                if (WaitHandle.WaitAny(reconnectWait) == 0)
                    return;
                if (WaitHandle.WaitAny(pauseWait) == 0)
                    return;

                try
                {
                    newConnection = new TCPConnection(remoteEndPoint, timeoutLength, maxTimeouts);
                }
                catch (Exception e)
                {
                    onConnectFail(e);
                    if (WaitHandle.WaitAny(forceWait, reconnectTime) == 0)
                        return;
                    reconnectEvt.Set();
                    continue;
                }
                newConnection.OnDisconnect += disconnect;
                onConnect(newConnection);
            }
        }

        private void disconnect(TCPConnection connection, Exception e)
        {
            reconnectEvt.Set();
        }
        #endregion
    }

    public sealed class TCPConnection : IDisposable
    {
        #region Events
        public event Action<TCPConnection, Exception> OnDisconnect;
        public event Action<TCPConnection, byte[]> OnReceive;
        public event Action<TCPConnection> OnTimeout;

        private void onDisconnect(Exception e) { Action<TCPConnection, Exception> evt = OnDisconnect; if (evt != null) evt(this, e); }
        private void onReceive(byte[] message) { Action<TCPConnection, byte[]> evt = OnReceive; if (evt != null) evt(this, message); }
        private void onTimeout() { Action<TCPConnection> evt = OnTimeout; if (evt != null) evt(this); }
        #endregion

        #region Variables
        private readonly Socket socket;
        private readonly IPEndPoint localEndPoint, remoteEndPoint;
        private readonly int timeoutLength;
        private readonly int maxTimeouts;
        private readonly ManualResetEvent disposeEvt;
        private readonly Task task;
        #endregion

        #region Accessors
        public IPEndPoint LocalEndPoint { get { return localEndPoint; } }
        public IPEndPoint RemoteEndPoint { get { return remoteEndPoint; } }
        public int TimeoutLength { get { return timeoutLength; } }
        public int MaxTimeouts { get { return maxTimeouts; } }
        public bool Disposed { get { return disposeEvt.WaitOne(0); } }
        public bool Connected { get { return socket.Connected; } }
        #endregion

        #region Constructor
        public TCPConnection(IPEndPoint remoteEndPoint, int timeoutLength = 5000, int maxTimeouts = 1)
            : this(timeoutLength, maxTimeouts)
        {
            if (remoteEndPoint == null) throw new ArgumentNullException("remoteEndPoint");
            if (remoteEndPoint.AddressFamily != AddressFamily.InterNetwork) throw new ArgumentException("remoteEndpoint.AddressFamily != AddressFamily.InterNetwork");

            this.socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            this.socket.Connect(remoteEndPoint);
            this.localEndPoint = (IPEndPoint)this.socket.LocalEndPoint;
            this.remoteEndPoint = remoteEndPoint;
        }
        internal TCPConnection(Socket socket, int timeoutLength = 5000, int maxTimeouts = 1)
            : this(timeoutLength, maxTimeouts)
        {
            if (socket == null) throw new ArgumentNullException("socket");
            if (socket.AddressFamily != AddressFamily.InterNetwork) throw new ArgumentException("socket.AddressFamily != AddressFamily.InterNetwork");

            this.socket = socket;
            this.localEndPoint = (IPEndPoint)socket.LocalEndPoint;
            this.remoteEndPoint = (IPEndPoint)socket.RemoteEndPoint;
        }
        private TCPConnection(int timeoutLength, int maxTimeouts)
        {
            if (timeoutLength < 0) throw new ArgumentOutOfRangeException("timeoutLength < 0");
            if (maxTimeouts < 0) throw new ArgumentOutOfRangeException("maxTimeouts < 0");

            this.timeoutLength = timeoutLength;
            this.maxTimeouts = maxTimeouts;
            this.disposeEvt = new ManualResetEvent(false);
            this.task = new Task(receiveTaskProc, TaskCreationOptions.LongRunning);
        }
        ~TCPConnection() { Dispose(); }
        #endregion

        #region Functions
        public void Dispose()
        {
            lock (task)
                if (disposeEvt != null) disposeEvt.Set();
        }

        public void Start()
        {
            lock (task)
                if (!disposeEvt.WaitOne(0) && task.Status == TaskStatus.Created)
                    task.Start();
        }

        public bool Send(byte[] message) { Exception e; return Send(message, out e); }
        public bool Send(byte[] message, out Exception exception)
        {
            if (!socket.Connected) { exception = null; return false; }
            try
            {
                socket.Send(new List<ArraySegment<byte>>(2) { 
                new ArraySegment<byte>(BitConverter.GetBytes(message.Length)), 
                new ArraySegment<byte>(message) });
                exception = null;
                return true;
            }
            catch (Exception e)
            {
                disconnect();
                exception = e;
                return false;
            }
        }

        private void disconnect()
        {
            try { socket.Shutdown(SocketShutdown.Both); }
            catch (Exception) { }
            try { socket.Close(); }
            catch (Exception) { }
            try { socket.Dispose(); }
            catch (Exception) { }
        }

        private void receiveTaskProc()
        {
            int timeouts = 0;
            byte[] lengthBuffer = new byte[4];
            byte[] messageBuffer = null;
            int bufferPos = 0;
            int readCount = 0;

            timeouts = 0;
            while (true)
            {
                bufferPos = 0;
                do
                {
                    while (true)
                        try
                        {
                            readCount = socket.Receive(lengthBuffer, bufferPos, 4 - bufferPos, SocketFlags.None);
                            timeouts = 0;
                            break;
                        }
                        catch (SocketException e)
                        {
                            if (e.SocketErrorCode != SocketError.TimedOut || ++timeouts > maxTimeouts)
                            {
                                disconnect();
                                onDisconnect(e);
                                return;
                            }
                            else
                            {
                                onTimeout();
                                continue;
                            }
                        }
                        catch (Exception e)
                        {
                            disconnect();
                            onDisconnect(e);
                            return;
                        }
                    if (readCount == 0) throw new Exception("Connection closed");
                    bufferPos += readCount;
                } while (bufferPos < 4);

                bufferPos = BitConverter.ToInt32(lengthBuffer, 0);
                if (bufferPos <= 0) throw new Exception("<=0 length message received");

                messageBuffer = new byte[bufferPos];
                bufferPos = 0;
                do
                {
                    while (true)
                        try
                        {
                            readCount = socket.Receive(messageBuffer, bufferPos, messageBuffer.Length - bufferPos, SocketFlags.None);
                            timeouts = 0;
                            break;
                        }
                        catch (SocketException e)
                        {
                            if (e.SocketErrorCode != SocketError.TimedOut || ++timeouts > maxTimeouts)
                            {
                                disconnect();
                                onDisconnect(e);
                                return;
                            }
                            else
                            {
                                onTimeout();
                                continue;
                            }
                        }
                        catch (Exception e)
                        {
                            disconnect();
                            onDisconnect(e);
                            return;
                        }
                    if (readCount == 0) throw new Exception("Connection closed");
                    bufferPos += readCount;
                } while (bufferPos < messageBuffer.Length);

                onReceive(messageBuffer);
            }
        }
        #endregion
    }
}