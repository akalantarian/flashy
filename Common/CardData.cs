﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;

namespace Flashy
{
    internal class CardIDLastEditData
    {
        #region Variables
        private readonly uint id;
        private readonly DateTime lastEditTime;
        private readonly CardData data;
        #endregion

        #region Accessors
        internal uint ID { get { return id; } }
        internal DateTime LastEditTime { get { return lastEditTime; } }
        internal CardData Data { get { return data; } }
        #endregion

        #region Constructor
        internal CardIDLastEditData(uint id, DateTime lastEditTime, CardData data)
        {
            this.id = id;
            this.lastEditTime = lastEditTime;
            this.data = data;
        }
        #endregion

        #region Serialization
        internal CardIDLastEditData(byte[] data) : this(data, Encoding.ASCII) { }
        internal CardIDLastEditData(byte[] data, Encoding encoding) : this(new MemoryStream(data), encoding) { }
        internal CardIDLastEditData(Stream stream) : this(stream, Encoding.ASCII) { }
        internal CardIDLastEditData(Stream stream, Encoding encoding) : this(new BinaryReader(stream, encoding)) { }
        internal CardIDLastEditData(BinaryReader reader)
        {
            this.id = reader.ReadUInt32();
            this.lastEditTime = new DateTime(reader.ReadInt64());
            this.data = reader.ReadBoolean() ? new CardData(reader) : null;
        }
        internal byte[] Write() { return Write(Encoding.ASCII); }
        internal byte[] Write(Encoding encoding) { MemoryStream stream = new MemoryStream(); Write(stream, encoding); stream.Close(); return stream.ToArray(); }
        internal void Write(Stream stream) { Write(stream, Encoding.ASCII); }
        internal void Write(Stream stream, Encoding encoding) { Write(new BinaryWriter(stream, encoding)); }
        internal void Write(BinaryWriter writer)
        {
            writer.Write(id);
            writer.Write(lastEditTime.Ticks);
            if(data == null)
                writer.Write(false);
            else
            {
                writer.Write(true);
                data.Write(writer);
            }
        }
        #endregion

        #region Functions
        public override string ToString()
        {
            return "ID=" + id + " EditTime=" + lastEditTime.Ticks + " Data=[" + (data == null ? "void" : data.ToString()) + "]";
        }
        #endregion
    }

    internal class CardIDData
    {
        #region Variables
        private readonly uint id;
        private readonly CardData data;
        #endregion

        #region Accessors
        internal uint ID { get { return id; } }
        internal CardData Data { get { return data; } }
        #endregion

        #region Constructor
        internal CardIDData(uint id, CardData data)
        {
            this.id = id;
            this.data = data;
        }
        #endregion

        #region Serialization
        internal CardIDData(byte[] data) : this(data, Encoding.ASCII) { }
        internal CardIDData(byte[] data, Encoding encoding) : this(new MemoryStream(data), encoding) { }
        internal CardIDData(Stream stream) : this(stream, Encoding.ASCII) { }
        internal CardIDData(Stream stream, Encoding encoding) : this(new BinaryReader(stream, encoding)) { }
        internal CardIDData(BinaryReader reader)
        {
            this.id = reader.ReadUInt32();
            this.data = reader.ReadBoolean() ? new CardData(reader) : null;
        }
        internal byte[] Write() { return Write(Encoding.ASCII); }
        internal byte[] Write(Encoding encoding) { MemoryStream stream = new MemoryStream(); Write(stream, encoding); stream.Close(); return stream.ToArray(); }
        internal void Write(Stream stream) { Write(stream, Encoding.ASCII); }
        internal void Write(Stream stream, Encoding encoding) { Write(new BinaryWriter(stream, encoding)); }
        internal void Write(BinaryWriter writer)
        {
            writer.Write(id);
            if(data == null)
                writer.Write(false);
            else
            {
                writer.Write(true);
                data.Write(writer);
            }
        }
        #endregion

        #region Functions
        public override string ToString()
        {
            return "ID=" + id + " Data=[" + (data == null ? "void" : data.ToString()) + "]";
        }
        #endregion
    }

    public class CardData
    {
        #region Variables
        private TagCollection tags;
        private Field question;
        private Field answer;
        private byte bin;
        #endregion

        #region Accessors
        public TagCollection Tags { get { return tags; } internal set { if(tags == null) throw new ArgumentNullException("Null tags"); tags = value; } }
        public Field Question { get { return question; } internal set { if(question == null) throw new ArgumentNullException("Null question"); question = value; } }
        public Field Answer { get { return answer; } internal set { if(answer == null) throw new ArgumentNullException("Null answer"); answer = value; } }
        public byte Bin { get { return bin; } internal set { bin = value; } }
        #endregion

        #region Constructor
        public CardData()
        {
            this.tags = new TagCollection();
            this.question = new Field.RTF();
            this.answer = new Field.RTF();
            this.bin = 0;
        }
        public CardData(TagCollection tags, Field question, Field answer, byte bin)
        {
            if(tags == null || question == null || answer == null) throw new ArgumentNullException("Null tags/question/answer");
            this.tags = tags;
            this.question = question;
            this.answer = answer;
            this.bin = bin;
        }
        #endregion

        #region Serialization
        internal CardData(byte[] data) : this(data, Encoding.ASCII) { }
        internal CardData(byte[] data, Encoding encoding) : this(new MemoryStream(data), encoding) { }
        internal CardData(Stream stream) : this(stream, Encoding.ASCII) { }
        internal CardData(Stream stream, Encoding encoding) : this(new BinaryReader(stream, encoding)) { }
        internal CardData(BinaryReader reader)
        {
            this.tags = new TagCollection(reader);
            this.question = Field.Decode(reader);
            this.answer = Field.Decode(reader);
            this.bin = reader.ReadByte();
        }
        internal byte[] Write() { return Write(Encoding.ASCII); }
        internal byte[] Write(Encoding encoding) { MemoryStream stream = new MemoryStream(); Write(stream, encoding); stream.Close(); return stream.ToArray(); }
        internal void Write(Stream stream) { Write(stream, Encoding.ASCII); }
        internal void Write(Stream stream, Encoding encoding) { Write(new BinaryWriter(stream, encoding)); }
        internal virtual void Write(BinaryWriter writer)
        {
            tags.Write(writer);
            Field.Encode(question, writer);
            Field.Encode(answer, writer);
            writer.Write(bin);
        }
        #endregion

        #region Functions
        public static bool Equivalent(CardData a, CardData b)
        {
            if(a == null) return b == null;
            if(b == null) return false;
            return a.bin == b.bin && TagCollection.Equivalent(a.tags, b.tags) && Field.Equivalent(a.question, b.question) && Field.Equivalent(a.answer, b.answer);
        }
        public override string ToString()
        {
            return "Tags={" + tags + "} Q=" + question + " A=" + answer + " Bin=" + bin;
        }
        #endregion
    }

    public abstract class Field
    {
        #region Static
        private static readonly byte[] defaultData = new byte[] { 0 };

        internal static Field Decode(byte[] data) { return Decode(data, Encoding.ASCII); }
        internal static Field Decode(byte[] data, Encoding encoding) { return Decode(new MemoryStream(data), encoding); }
        internal static Field Decode(Stream stream) { return Decode(stream, Encoding.ASCII); }
        internal static Field Decode(Stream stream, Encoding encoding) { return Decode(new BinaryReader(stream, encoding)); }
        internal static Field Decode(BinaryReader reader)
        {
            if(reader.ReadBoolean())
                return new Image(reader);
            else
                return new RTF(reader);
        }

        internal static byte[] Encode(Field field) { return Encode(field, Encoding.ASCII); }
        internal static byte[] Encode(Field field, Encoding encoding) { MemoryStream stream = new MemoryStream(); Encode(field, stream, encoding); stream.Close(); return stream.ToArray(); }
        internal static void Encode(Field field, Stream stream) { Encode(field, stream, Encoding.ASCII); }
        internal static void Encode(Field field, Stream stream, Encoding encoding) { Encode(field, new BinaryWriter(stream, encoding)); }
        internal static void Encode(Field field, BinaryWriter writer)
        {
            if(field is Image)
                writer.Write(true);
            else
                writer.Write(false);
            field.Write(writer);
        }
        #endregion

        #region Variables
        private readonly byte[] data;
        #endregion

        #region Accessors
        public byte[] Data { get { return data; } }
        #endregion

        #region Constructor
        protected Field() { this.data = defaultData; }
        protected Field(byte[] data) { if(data == null || data.Length == 0) throw new Exception("Invalid field"); this.data = data; }
        #endregion

        #region Serialization
        protected Field(BinaryReader reader) { this.data = reader.ReadBytes(reader.ReadInt32()); }
        private void Write(BinaryWriter writer)
        {
            writer.Write(data.Length);
            writer.Write(data);
        }
        #endregion

        #region Functions
        [DllImport("msvcrt.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern int memcmp(byte[] b1, byte[] b2, long count);
        public static bool Equivalent(Field a, Field b)
        {
            if(a == null) return b == null;
            if(b == null) return false;
            if(a.GetType() != b.GetType()) return false;
            return a.data.Length == b.data.Length && memcmp(a.data, b.data, a.data.Length) == 0;
        }
        #endregion

        #region Subclasses
        public sealed class Image : Field
        {
            public Image() : base() { }
            public Image(byte[] data) : base(data) { }

            internal Image(BinaryReader reader) : this(reader.ReadBytes(reader.ReadInt32())) { }
            public override string ToString() { return "Image"; }
        }
        public sealed class RTF : Field
        {
            public RTF() : base() { }
            public RTF(byte[] data) : base(data) { }

            internal RTF(BinaryReader reader) : this(reader.ReadBytes(reader.ReadInt32())) { }
            public override string ToString() { return "RTF"; }
        }
        #endregion
    }
    public sealed class TagCollection : ReadOnlyCollection<string>
    {
        #region Static
        private static readonly Regex tagChecker = new Regex(@"^[\w\d_-]+$", RegexOptions.Compiled);

        public static bool IsValidTag(string tag)
        {
            if(tag == null) return false;
            return tagChecker.IsMatch(tag);
        }
        public static bool IsValidTagCollection(IEnumerable<string> tags)
        {
            if(tags == null) return false;
            foreach(string tag in tags)
                if(!IsValidTag(tag)) return false;
            return true;
        }
        public static bool IsValidTagString(string tagString)
        {
            if(tagString == null) return false;
            string[] tags = tagString.Split(new string[] { ", " }, StringSplitOptions.None);
            foreach(string tag in tags)
                if(!IsValidTag(tag)) return false;
            return true;
        }

        private static ReadOnlyCollection<string> convert(string tagString)
        {
            if(tagString == null) return null;
            string[] tags = tagString.Split(new string[] { ", " }, StringSplitOptions.None);
            List<string> returnTags = new List<string>(tags.Length);
            foreach(string tag in tags)
            {
                if(!IsValidTag(tag)) return null;
                if(!returnTags.Contains(tag)) returnTags.Add(tag);
            }
            return returnTags.AsReadOnly();
        }
        private static ReadOnlyCollection<string> convert(IEnumerable<string> tags)
        {
            if(tags == null) return null;
            List<string> returnTags = new List<string>();
            foreach(string tag in tags)
            {
                if(!IsValidTag(tag)) return null;
                if(!returnTags.Contains(tag)) returnTags.Add(tag);
            }
            return returnTags.AsReadOnly();
        }
        private static ReadOnlyCollection<string> convert(ICollection<string> tags)
        {
            if(tags == null) return null;
            if(tags.Count == 0) return new ReadOnlyCollection<string>(new List<string>(0));
            List<string> returnTags = new List<string>(tags.Count);
            foreach(string tag in tags)
            {
                if(!IsValidTag(tag)) return null;
                if(!returnTags.Contains(tag)) returnTags.Add(tag);
            }
            return returnTags.AsReadOnly();
        }
        #endregion

        #region Constructor
        public TagCollection() : base(new List<string>(0)) { }
        public TagCollection(string tagString) : base(convert(tagString)) { }
        public TagCollection(IEnumerable<string> tags) : base(convert(tags)) { }
        public TagCollection(ICollection<string> tags) : base(convert(tags)) { }
        #endregion

        #region Serialization
        internal TagCollection(byte[] bytes) : this(bytes, Encoding.ASCII) { }
        internal TagCollection(byte[] bytes, Encoding encoding) : this(new MemoryStream(bytes), encoding) { }
        internal TagCollection(Stream stream) : this(stream, Encoding.ASCII) { }
        internal TagCollection(Stream stream, Encoding encoding) : this(new BinaryReader(stream, encoding)) { }
        internal TagCollection(BinaryReader reader) : this(readStrings(reader)) { }
        private static List<string> readStrings(BinaryReader reader)
        {
            int count = reader.ReadInt32();
            List<string> list = new List<string>(count);
            for(int i = 0; i < count; i++)
                list.Add(reader.ReadString());
            return list;
        }
        internal byte[] Write() { return Write(Encoding.ASCII); }
        internal byte[] Write(Encoding encoding) { MemoryStream stream = new MemoryStream(); Write(stream, encoding); stream.Close(); return stream.ToArray(); }
        internal void Write(Stream stream) { Write(stream, Encoding.ASCII); }
        internal void Write(Stream stream, Encoding encoding) { Write(new BinaryWriter(stream, encoding)); }
        internal void Write(BinaryWriter writer)
        {
            writer.Write(Count);
            foreach(string tag in this)
                writer.Write(tag);
        }
        #endregion

        #region Operators
        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            for(int i = 0; i < Count - 1; i++)
                str.Append(this[i] + ", ");
            if(Count > 0)
                str.Append(this[Count - 1]);
            return str.ToString();
        }
        #endregion

        #region Functions
        public static bool Equivalent(TagCollection a, TagCollection b)
        {
            if(a == null) return b == null;
            if(b == null) return false;

            if(a.Count != b.Count) return false;
            for(int i = 0; i < a.Count; i++)
                if(a[i] != b[i])
                    return false;
            return true;
        }
        #endregion
    }
}