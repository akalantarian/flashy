﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Sockets;

using Flashy.Network;

namespace Flashy
{
    public static class Settings
    {
        public static bool CheckLibraryName(string name) { if(name == null) return false; return libraryNameChecker.IsMatch(name); }
        private static readonly Regex libraryNameChecker = new Regex(@"^[\w\d_-]{1,16}$", RegexOptions.Compiled);

#if CLIENT
        internal const int ReconnectTime = 5000;
        internal const int TimeoutLength = 10000;
        internal const int MaxTimeouts = 2;
#if DEBUG
        internal const int CacheCapacity = 2;
        internal static readonly IPEndPoint RemoteEndPoint = new IPEndPoint(NetworkInfo.GetLocalIP(), 18236);
#else
        internal const int CacheCapacity = 10000;
        internal static readonly IPEndPoint RemoteEndPoint = new IPEndPoint(NetworkInfo.GetResolvedAddress(@"flashy.no-ip.org"), 18236);
#endif
#elif SERVER
        internal const int BackLog = 100;
        internal const int ReconnectTime = 5000;
        internal const int TimeoutLength = 15000;
        internal const int MaxTimeouts = 1;

        internal static readonly IPEndPoint LocalEndPoint = new IPEndPoint(IPAddress.Any, 18236);
#if DEBUG
        internal const int CacheCapacity = 2;
#else
        internal const int CacheCapacity = 10000;
#endif
#endif
    }
}