﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Sockets;

namespace Flashy.Network
{
    internal abstract class NetworkMessage
    {
        #region Enums
        private enum MessageID : byte
        {
            PingReq,
            PingResp,

            GetInfoReq,
            GetInfoResp,

            SyncLibraryReq,
            SyncLibraryResp,

            SyncCardsReq,
            SyncCardsResp,

            OpenLibraryReq,
            OpenLibraryResp,

            GetFilterListReq,
            GetFilterListResp,

            SaveCardsReq,
            SaveCardsResp,

            GetCardsReq,
            GetCardsResp,
        }
        #endregion

        #region Static
        internal static NetworkMessage Decode(byte[] data) { return Decode(data, Encoding.ASCII); }
        internal static NetworkMessage Decode(byte[] data, Encoding encoding) { return Decode(new MemoryStream(data), encoding); }
        internal static NetworkMessage Decode(Stream stream) { return Decode(stream, Encoding.ASCII); }
        internal static NetworkMessage Decode(Stream stream, Encoding encoding) { return Decode(new BinaryReader(stream, encoding)); }
        internal static NetworkMessage Decode(BinaryReader reader)
        {
            switch ((MessageID)reader.ReadByte())
            {
                case MessageID.PingReq: return PingRequest.Instance;
                case MessageID.PingResp: return PingResponse.Instance;

                case MessageID.GetInfoReq: return GetInfoRequest.Instance;
                case MessageID.GetInfoResp: return new GetInfoResponse(reader);

                case MessageID.SyncLibraryReq: return new SyncLibraryRequest(reader);
                case MessageID.SyncLibraryResp: return new SyncLibraryResponse(reader);

                case MessageID.SyncCardsReq: return new SyncCardsRequest(reader);
                case MessageID.SyncCardsResp: return new SyncCardsResponse(reader);

                case MessageID.OpenLibraryReq: return new OpenLibraryRequest(reader);
                case MessageID.OpenLibraryResp: return new OpenLibraryResponse(reader);

                case MessageID.GetFilterListReq: return new GetFilterListRequest(reader);
                case MessageID.GetFilterListResp: return new GetFilterListResponse(reader);

                case MessageID.SaveCardsReq: return new SaveCardsRequest(reader);
                case MessageID.SaveCardsResp: return new SaveCardsResponse(reader);

                case MessageID.GetCardsReq: return new GetCardsRequest(reader);
                case MessageID.GetCardsResp: return new GetCardsResponse(reader);
                default: return null;
            }
        }
        internal static byte[] Encode(NetworkMessage message) { return Encode(message, Encoding.ASCII); }
        internal static byte[] Encode(NetworkMessage message, Encoding encoding) { MemoryStream stream = new MemoryStream(); Encode(message, stream, encoding); stream.Close(); return stream.ToArray(); }
        internal static void Encode(NetworkMessage message, Stream stream) { Encode(message, stream, Encoding.ASCII); }
        internal static void Encode(NetworkMessage message, Stream stream, Encoding encoding) { Encode(message, new BinaryWriter(stream, encoding)); }
        internal static void Encode(NetworkMessage message, BinaryWriter writer)
        {
            if(message is PingRequest) writer.Write((byte)MessageID.PingReq);
            else if(message is PingResponse) writer.Write((byte)MessageID.PingResp);

            else if(message is GetInfoRequest) writer.Write((byte)MessageID.GetInfoReq);
            else if(message is GetInfoResponse) writer.Write((byte)MessageID.GetInfoResp);

            else if(message is SyncLibraryRequest) writer.Write((byte)MessageID.SyncLibraryReq);
            else if(message is SyncLibraryResponse) writer.Write((byte)MessageID.SyncLibraryResp);

            else if(message is SyncCardsRequest) writer.Write((byte)MessageID.SyncCardsReq);
            else if(message is SyncCardsResponse) writer.Write((byte)MessageID.SyncCardsResp);

            else if(message is OpenLibraryRequest) writer.Write((byte)MessageID.OpenLibraryReq);
            else if(message is OpenLibraryResponse) writer.Write((byte)MessageID.OpenLibraryResp);

            else if(message is GetFilterListRequest) writer.Write((byte)MessageID.GetFilterListReq);
            else if(message is GetFilterListResponse) writer.Write((byte)MessageID.GetFilterListResp);

            else if(message is SaveCardsRequest) writer.Write((byte)MessageID.SaveCardsReq);
            else if(message is SaveCardsResponse) writer.Write((byte)MessageID.SaveCardsResp);

            else if(message is GetCardsRequest) writer.Write((byte)MessageID.GetCardsReq);
            else if(message is GetCardsResponse) writer.Write((byte)MessageID.GetCardsResp);


            else throw new Exception("Unknown messagetype");

            message.Write(writer);
        }
        #endregion

        #region Functions
        protected abstract void Write(BinaryWriter writer);
        #endregion

        #region Subclasses
        internal sealed class PingRequest : NetworkMessage { private PingRequest() { } internal static readonly PingRequest Instance = new PingRequest(); protected override void Write(BinaryWriter writer) { } }
        internal sealed class PingResponse : NetworkMessage { private PingResponse() { } internal static readonly PingResponse Instance = new PingResponse(); protected override void Write(BinaryWriter writer) { } }

        internal sealed class GetInfoRequest : NetworkMessage { private GetInfoRequest() { } internal static readonly GetInfoRequest Instance = new GetInfoRequest(); protected override void Write(BinaryWriter writer) { } }
        internal sealed class GetInfoResponse : NetworkMessage
        {
            #region Variables
            private readonly List<string> libraries;
            private readonly List<string> quizzes;
            #endregion

            #region Accessors
            internal List<string> Libraries { get { return libraries; } }
            internal List<string> Quizzes { get { return quizzes; } }
            #endregion

            #region Constructor
            internal GetInfoResponse(List<string> libraries, List<string> quizzes)
            {
                this.libraries = libraries;
                this.quizzes = quizzes;
            }
            #endregion

            #region Serialization
            internal GetInfoResponse(BinaryReader reader)
            {
                int count = reader.ReadInt32();
                this.libraries = new List<string>(count);
                for(int i = 0; i < count; i++)
                    this.libraries.Add(reader.ReadString());
                count = reader.ReadInt32();
                this.quizzes = new List<string>(count);
                for(int i = 0; i < count; i++)
                    this.quizzes.Add(reader.ReadString());
            }
            protected override void Write(BinaryWriter writer)
            {
                writer.Write(libraries.Count);
                foreach(string library in libraries)
                    writer.Write(library);
                writer.Write(quizzes.Count);
                foreach(string quiz in quizzes)
                    writer.Write(quiz);
            }
            #endregion
        }

        #region Edit Mode
        internal sealed class SyncLibraryRequest : NetworkMessage
        {
            #region Variables
            private readonly string library;
            private readonly DateTime lastEdit;
            private readonly bool kick;
            #endregion

            #region Accessors
            internal string Library { get { return library; } }
            internal DateTime LastEdit { get { return lastEdit; } }
            internal bool Kick { get { return kick; } }
            #endregion

            #region Constructor
            internal SyncLibraryRequest(string library, DateTime lastEdit, bool kick)
            {
                this.library = library;
                this.lastEdit = lastEdit;
                this.kick = kick;
            }
            #endregion

            #region Serialization
            internal SyncLibraryRequest(BinaryReader reader)
            {
                this.library = reader.ReadString();
                this.lastEdit = new DateTime(reader.ReadInt64());
                this.kick = reader.ReadBoolean();
            }
            protected override void Write(BinaryWriter writer)
            {
                writer.Write(library);
                writer.Write(lastEdit.Ticks);
                writer.Write(kick);
            }
            #endregion
        }
        internal sealed class SyncLibraryResponse : NetworkMessage
        {
            #region Variables
            private readonly ReadOnlyCollection<CardIDLastEditData> cards;
            #endregion

            #region Accessors
            internal ReadOnlyCollection<CardIDLastEditData> Cards { get { return cards; } }
            #endregion

            #region Constructor
            internal SyncLibraryResponse(ReadOnlyCollection<CardIDLastEditData> cards)
            {
                this.cards = cards;
            }
            #endregion

            #region Serialization
            internal SyncLibraryResponse(BinaryReader reader)
            {
                int count = reader.ReadInt32();
                List<CardIDLastEditData> list = new List<CardIDLastEditData>(count);
                for (int i = 0; i < count; i++)
                    list.Add(new CardIDLastEditData(reader));
                this.cards = list.AsReadOnly();
            }
            protected override void Write(BinaryWriter writer)
            {
                writer.Write(cards.Count);
                foreach (CardIDLastEditData card in cards)
                    card.Write(writer);
            }
            #endregion
        }

        internal sealed class SyncCardsRequest : NetworkMessage
        {
            #region Variables
            private readonly DateTime tag;
            private readonly ReadOnlyCollection<CardIDData> cards;
            #endregion

            #region Accessors
            internal DateTime Tag { get { return tag; } }
            internal ReadOnlyCollection<CardIDData> Cards { get { return cards; } }
            #endregion

            #region Constructor
            internal SyncCardsRequest(DateTime tag, ReadOnlyCollection<CardIDData> cards)
            {
                this.tag = tag;
                this.cards = cards;
            }
            internal SyncCardsRequest(DateTime tag, CardIDData card)
            {
                this.tag = tag;
                this.cards = new List<CardIDData>(1) { card }.AsReadOnly();
            }
            #endregion

            #region Serialization
            internal SyncCardsRequest(BinaryReader reader)
            {
                this.tag = new DateTime(reader.ReadInt64());

                int count = reader.ReadInt32();
                List<CardIDData> list = new List<CardIDData>(count);
                for (int i = 0; i < count; i++)
                    list.Add(new CardIDData(reader));

                this.cards = list.AsReadOnly();
            }
            protected override void Write(BinaryWriter writer)
            {
                writer.Write(tag.Ticks);
                writer.Write(cards.Count);
                foreach (CardIDData card in cards)
                    card.Write(writer);
            }
            #endregion
        }
        internal sealed class SyncCardsResponse: NetworkMessage
        {
            #region Variables
            private readonly DateTime tag, lastEditTime;
            #endregion

            #region Accessors
            internal DateTime Tag { get { return tag; } }
            internal DateTime LastEditTime { get { return lastEditTime; } }
            #endregion

            #region Constructor
            internal SyncCardsResponse(DateTime tag, DateTime lastEditTime)
            {
                this.tag = tag;
                this.lastEditTime = lastEditTime;
            }
            #endregion

            #region Serialization
            internal SyncCardsResponse(BinaryReader reader)
            {
                this.tag = new DateTime(reader.ReadInt64());
                this.lastEditTime = new DateTime(reader.ReadInt64());
            }
            protected override void Write(BinaryWriter writer)
            {
                writer.Write(tag.Ticks);
                writer.Write(lastEditTime.Ticks);
            }
            #endregion
        }
        #endregion

        #region Quiz mode
        internal sealed class OpenLibraryRequest : NetworkMessage
        {
            #region Variables
            private readonly ReadOnlyCollection<string> libraries;
            private readonly bool kick;
            #endregion

            #region Accessors
            internal ReadOnlyCollection<string> Libraries { get { return libraries; } }
            internal bool Kick { get { return kick; } }
            #endregion

            #region Constructor
            internal OpenLibraryRequest(ReadOnlyCollection<string> libraries, bool kick)
            {
                this.libraries = libraries;
                this.kick = kick;
            }
            #endregion

            #region Serialization
            internal OpenLibraryRequest(BinaryReader reader)
            {
                int count = reader.ReadInt32();
                List<string> list = new List<string>(count);
                for (int i = 0; i < count; i++)
                    list.Add(reader.ReadString());

                this.libraries = list.AsReadOnly();

                this.kick = reader.ReadBoolean();
            }
            protected override void Write(BinaryWriter writer)
            {
                writer.Write(libraries.Count);
                foreach (string library in libraries)
                    writer.Write(library);

                writer.Write(kick);
            }
            #endregion
        }
        internal sealed class OpenLibraryResponse : NetworkMessage
        {
            #region Variables
            private readonly string name;
            private readonly ReadOnlyCollection<uint> cards;
            #endregion

            #region Accessors
            internal string Name { get { return name; } }
            internal ReadOnlyCollection<uint> Cards { get { return cards; } }
            #endregion

            #region Constructor
            internal OpenLibraryResponse(string name, ReadOnlyCollection<uint> cards)
            {
                this.name = name;
                this.cards = cards;
            }
            #endregion

            #region Serialization
            internal OpenLibraryResponse(BinaryReader reader)
            {
                this.name = reader.ReadString();
                int count = reader.ReadInt32();
                List<uint> list = new List<uint>(count);
                for (int i = 0; i < count; i++)
                    list.Add(reader.ReadUInt32());
                this.cards = list.AsReadOnly();
            }
            protected override void Write(BinaryWriter writer)
            {
                writer.Write(name);
                writer.Write(cards.Count);
                foreach (uint id in cards)
                    writer.Write(id);
            }
            #endregion
        }

        internal sealed class GetFilterListRequest : NetworkMessage
        {
            #region Variables
            #endregion

            #region Accessors
            #endregion

            #region Constructor
            internal GetFilterListRequest()
            {
            }
            #endregion

            #region Serialization
            internal GetFilterListRequest(BinaryReader reader)
            {
            }
            protected override void Write(BinaryWriter writer)
            {
            }
            #endregion
        }

        internal sealed class GetFilterListResponse : NetworkMessage
        {
            #region Variables
            #endregion

            #region Accessors
            #endregion

            #region Constructor
            internal GetFilterListResponse()
            {
            }
            #endregion

            #region Serialization
            internal GetFilterListResponse(BinaryReader reader)
            {
            }
            protected override void Write(BinaryWriter writer)
            {
            }
            #endregion
        }

        internal sealed class SaveCardsRequest : NetworkMessage
        {
            #region Variables
            private readonly string name;
            private readonly DateTime tag;
            private readonly ReadOnlyCollection<CardIDData> cards;
            #endregion

            #region Accessors
            internal string Name { get { return name; } }
            internal DateTime Tag { get { return tag; } }
            internal ReadOnlyCollection<CardIDData> Cards { get { return cards; } }
            #endregion

            #region Constructor
            internal SaveCardsRequest(string name, DateTime tag, ReadOnlyCollection<CardIDData> cards)
            {
                this.name = name;
                this.tag = tag;
                this.cards = cards;
            }
            #endregion

            #region Serialization
            internal SaveCardsRequest(BinaryReader reader)
            {
                this.name = reader.ReadString();
                this.tag = new DateTime(reader.ReadInt64());

                int count = reader.ReadInt32();
                List<CardIDData> list = new List<CardIDData>(count);
                for (int i = 0; i < count; i++)
                    list.Add(new CardIDData(reader));
                this.cards = list.AsReadOnly();
            }
            protected override void Write(BinaryWriter writer)
            {
                writer.Write(name);
                writer.Write(tag.Ticks);

                writer.Write(cards.Count);
                foreach (CardIDData card in cards)
                    card.Write(writer);
            }
            #endregion
        }
        internal sealed class SaveCardsResponse : NetworkMessage
        {
            #region Variables
            private readonly string name;
            private readonly DateTime tag;
            #endregion

            #region Accessors
            internal string Name { get { return name; } }
            internal DateTime Tag { get { return tag; } }
            #endregion

            #region Constructor
            internal SaveCardsResponse(string name, DateTime tag)
            {
                this.name = name;
                this.tag = tag;
            }
            #endregion

            #region Serialization
            internal SaveCardsResponse(BinaryReader reader)
            {
                this.name = reader.ReadString();
                this.tag = new DateTime(reader.ReadInt64());
            }
            protected override void Write(BinaryWriter writer)
            {
                writer.Write(name);
                writer.Write(tag.Ticks);
            }
            #endregion
        }

        internal sealed class GetCardsRequest : NetworkMessage
        {
            #region Variables
            private readonly string name;
            private readonly ReadOnlyCollection<uint> cards;
            #endregion

            #region Accessors
            internal string Name { get { return name; } }
            internal ReadOnlyCollection<uint> Cards { get { return cards; } }
            #endregion

            #region Constructor
            internal GetCardsRequest(string name, ReadOnlyCollection<uint> cards)
            {
                this.name = name;
                this.cards = cards;
            }
            #endregion

            #region Serialization
            internal GetCardsRequest(BinaryReader reader)
            {
                this.name = reader.ReadString();

                int count = reader.ReadInt32();
                List<uint> list = new List<uint>(count);
                for (int i = 0; i < count; i++)
                    list.Add(reader.ReadUInt32());
                this.cards = list.AsReadOnly();
            }
            protected override void Write(BinaryWriter writer)
            {
                writer.Write(name);

                writer.Write(cards.Count);
                foreach (uint id in cards)
                    writer.Write(id);
            }
            #endregion
        }
        internal sealed class GetCardsResponse : NetworkMessage
        {
            #region Variables
            private readonly string name;
            private readonly ReadOnlyCollection<CardIDData> cards;
            #endregion

            #region Accessors
            internal string Name { get { return name; } }
            internal ReadOnlyCollection<CardIDData> Cards { get { return cards; } }
            #endregion

            #region Constructor
            internal GetCardsResponse(string name, ReadOnlyCollection<CardIDData> cards)
            {
                this.name = name;
                this.cards = cards;
            }
            #endregion

            #region Serialization
            internal GetCardsResponse(BinaryReader reader)
            {
                this.name = reader.ReadString();
                int count = reader.ReadInt32();
                List<CardIDData> list = new List<CardIDData>(count);
                for (int i = 0; i < count; i++)
                    list.Add(new CardIDData(reader));

                this.cards = list.AsReadOnly();
            }
            protected override void Write(BinaryWriter writer)
            {
                writer.Write(name);
                writer.Write(cards.Count);
                foreach (CardIDData card in cards)
                    card.Write(writer);
            }
            #endregion
        }
        #endregion

        #endregion
    }
}
