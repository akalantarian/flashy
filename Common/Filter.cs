﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Sockets;

namespace Flashy
{
    public abstract class Filter
    {
        #region Static
        public static bool Evaluate(Filter filter, string library, CardData data)
        {
            if(filter == null) return true;
            return filter.Evaluate(library, data);
        }
        public static bool Equivalent(Filter a, Filter b)
        {
            if(a == null) return b == null;
            return a.Equivalent(b);
        }

        internal static Filter Decode(byte[] data) { return Decode(data, Encoding.ASCII); }
        internal static Filter Decode(byte[] data, Encoding encoding) { return Decode(new MemoryStream(data), encoding); }
        internal static Filter Decode(Stream stream) { return Decode(stream, Encoding.ASCII); }
        internal static Filter Decode(Stream stream, Encoding encoding) { return Decode(new BinaryReader(stream, encoding)); }
        internal static Filter Decode(BinaryReader reader)
        {
            switch(reader.ReadByte())
            {
                case 1: return new Group(reader);
                case 2: return new Library(reader);
                case 3: return new Tag(reader);
                case 4: return new Question(reader);
                case 5: return new Answer(reader);
                default: return null;
            }
        }

        internal static byte[] Encode(Filter filter) { return Encode(filter, Encoding.ASCII); }
        internal static byte[] Encode(Filter filter, Encoding encoding) { MemoryStream stream = new MemoryStream(); Encode(filter, stream, encoding); stream.Close(); return stream.ToArray(); }
        internal static void Encode(Filter predicate, Stream stream) { Encode(predicate, stream, Encoding.ASCII); }
        internal static void Encode(Filter filter, Stream stream, Encoding encoding) { Encode(filter, new BinaryWriter(stream, encoding)); }
        internal static void Encode(Filter filter, BinaryWriter writer)
        {
            if(filter is Group)
                writer.Write((byte)1);

            else if(filter is Library)
                writer.Write((byte)2);

            else if(filter is Tag)
                writer.Write((byte)3);

            else if(filter is Question)
                writer.Write((byte)4);

            else if(filter is Answer)
                writer.Write((byte)5);

            else
                writer.Write((byte)0);

            filter.Write(writer);
        }
        #endregion

        #region Subclasses
        public enum Operation : byte
        {
            And,
            Or,
        }
        public sealed class Group : Filter
        {
            #region Variables
            private List<List<Filter>> andGroups;
            #endregion

            #region Accessors
            public List<List<Filter>> AndGroups { get { return andGroups; } }
            #endregion

            #region Constructor
            public Group(Filter baseFilter, params KeyValuePair<Operation, Filter>[] filters)
            {
                List<List<Filter>> list = new List<List<Filter>>();
                List<Filter> andGroup = new List<Filter>() { baseFilter };
                if(filters != null)
                foreach(KeyValuePair<Operation, Filter> filter in filters)
                {
                    if(filter.Key == Operation.And)
                        andGroup.Add(filter.Value);
                    else
                    {
                        list.Add(andGroup);
                        andGroup = new List<Filter>() { filter.Value };
                    }
                }
                list.Add(andGroup);
                this.andGroups = list;
            }
            public Group(Filter filter1, Operation operation1, Filter filter2)
                : this(filter1, new KeyValuePair<Operation, Filter>(operation1, filter2))
            { }
            
            public Group(Filter filter1, Operation operation1, Filter filter2, Operation operation2, Filter filter3)
                : this(filter1, new KeyValuePair<Operation, Filter>(operation1, filter2), new KeyValuePair<Operation, Filter>(operation2, filter3))
            { }

            public Group(Filter filter1, Operation operation1, Filter filter2, Operation operation2, Filter filter3, Operation operation3, Filter filter4)
                : this(filter1, new KeyValuePair<Operation, Filter>(operation1, filter2), new KeyValuePair<Operation, Filter>(operation2, filter3), new KeyValuePair<Operation, Filter>(operation3, filter4))
            { }
            #endregion

            #region Serialization
            internal Group(byte[] data) : this(data, Encoding.ASCII) { }
            internal Group(byte[] data, Encoding encoding) : this(new MemoryStream(data), encoding) { }
            internal Group(Stream stream) : this(stream, Encoding.ASCII) { }
            internal Group(Stream stream, Encoding encoding) : this(new BinaryReader(stream, encoding)) { }
            internal Group(BinaryReader reader)
            {
                int outerCount = reader.ReadInt32();
                if(outerCount == 0) { this.andGroups = null; return; }
                List<List<Filter>> outerGroup = new List<List<Filter>>(outerCount);
                for(int i = 0; i < outerCount; i++)
                {
                    int innerCount = reader.ReadInt32();
                    List<Filter> innerGroup = new List<Filter>(innerCount);
                    for(int j = 0; j < innerCount; j++)
                        innerGroup.Add(Filter.Decode(reader));
                    outerGroup.Add(innerGroup);
                }
                this.andGroups = outerGroup;
            }
            internal override void Write(BinaryWriter writer)
            {
                if(andGroups == null) { writer.Write(0); return; }
                writer.Write(andGroups.Count);
                foreach(List<Filter> andGroup in andGroups)
                {
                    writer.Write(andGroup.Count);
                    foreach(Filter filter in andGroup)
                        Filter.Encode(filter, writer);
                }
            }
            #endregion

            #region Functions
            public void Add(Operation operation, Filter filter)
            {
                if(operation == Operation.And)
                    andGroups.Last().Add(filter);
                else
                    andGroups.Add(new List<Filter>() { filter });
            }
            public override bool Evaluate(string library, CardData data)
            {
                if(andGroups == null) return true;
                foreach(List<Filter> andGroup in andGroups)
                    if(evaluate(library, data, andGroup)) return true;
                return false;
            }
            private bool evaluate(string library, CardData data, List<Filter> andGroup)
            {
                foreach(Filter filter in andGroup)
                    if(!Filter.Evaluate(filter, library, data)) return false;
                return true;
            }
            public override bool Equivalent(Filter other)
            {
                if(this == other) return true;
                if(!(other is Group)) return false;
                Group otherGroup = (Group)other;
                if(inverted != other.inverted) return false;

                if(andGroups.Count != otherGroup.andGroups.Count) return false;
                for(int i = 0; i < andGroups.Count; i++)
                    if(andGroups[i].Count != otherGroup.andGroups[i].Count) return false;

                for(int i = 0; i < andGroups.Count; i++)
                    for(int j = 0; j < andGroups[i].Count; j++)
                        if(!Filter.Equivalent(andGroups[i][j], otherGroup.andGroups[i][j])) return false;

                return true;
            }
            #endregion
        }

        public sealed class Library : Filter
        {
            #region Variables
            private readonly Predicate predicate;
            #endregion

            #region Accessors
            public new Predicate Predicate { get { return predicate; } }
            #endregion

            #region Constructor
            public Library(Predicate predicate, bool inverted = false) : base(inverted)
            {
                if(predicate == null) throw new ArgumentNullException("Null predicate");
                if(!(predicate is Predicate.Str) && !(predicate is Predicate.Rgx))
                    throw new ArgumentException("Predicate must be string or regex");
                this.predicate = predicate;
            }
            #endregion

            #region Serialization
            internal Library(byte[] data) : this(data, Encoding.ASCII) { }
            internal Library(byte[] data, Encoding encoding) : this(new MemoryStream(data), encoding) { }
            internal Library(Stream stream) : this(stream, Encoding.ASCII) { }
            internal Library(Stream stream, Encoding encoding) : this(new BinaryReader(stream, encoding)) { }
            internal Library(BinaryReader reader) : base(reader)
            {
                this.predicate = Predicate.Decode(reader);
            }

            internal override void Write(BinaryWriter writer)
            {
                Predicate.Encode(predicate, writer);
            }
            #endregion

            #region Functions
            public override bool Evaluate(string library, CardData data)
            {
                return inverted ? !predicate.Evaluate(library) : predicate.Evaluate(library);
            }
            public override bool Equivalent(Filter other)
            {
                if(this == other) return true;
                if(!(other is Library)) return false;
                Library otherLibrary = (Library)other;
                return predicate.Equivalent(otherLibrary.predicate) && inverted == other.inverted;
            }
            #endregion
        }

        public sealed class Tag : Filter
        {
            #region Variables
            private readonly Predicate predicate;
            #endregion

            #region Accessors
            public new Predicate Predicate { get { return predicate; } }
            #endregion

            #region Constructor
            public Tag(Predicate predicate, bool inverted = false) : base(inverted)
            {
                if(predicate == null) throw new ArgumentNullException("Null predicate");
                if(!(predicate is Predicate.Str) && !(predicate is Predicate.Rgx) && !(predicate is Predicate.Count))
                    throw new ArgumentException("Predicate must be string or regex or count");
                this.predicate = predicate;
            }
            #endregion

            #region Serialization
            internal Tag(byte[] data) : this(data, Encoding.ASCII) { }
            internal Tag(byte[] data, Encoding encoding) : this(new MemoryStream(data), encoding) { }
            internal Tag(Stream stream) : this(stream, Encoding.ASCII) { }
            internal Tag(Stream stream, Encoding encoding) : this(new BinaryReader(stream, encoding)) { }
            internal Tag(BinaryReader reader) : base(reader)
            {
                this.predicate = Predicate.Decode(reader);
            }

            internal override void Write(BinaryWriter writer)
            {
                Predicate.Encode(predicate, writer);
            }
            #endregion

            #region Functions
            public override bool Evaluate(string library, CardData data)
            {
                return inverted ? !predicate.Evaluate(data.Tags) : predicate.Evaluate(data.Tags);
            }
            public override bool Equivalent(Filter other)
            {
                if(this == other) return true;
                if(!(other is Tag)) return false;
                Tag otherTag = (Tag)other;
                return predicate.Equivalent(otherTag.predicate) && inverted == other.inverted;
            }
            #endregion
        }

        public sealed class Question : Filter
        {
            #region Variables
            private readonly Predicate predicate;
            #endregion

            #region Accessors
            public new Predicate Predicate { get { return predicate; } }
            #endregion

            #region Constructor
            public Question(Predicate predicate, bool inverted = false) : base(inverted)
            {
                if(predicate == null) throw new ArgumentNullException("Null predicate");
                if(!(predicate is Predicate.Str) && !(predicate is Predicate.Rgx) && !(predicate is Predicate.DocumentType) && !(predicate is Predicate.ImageType))
                    throw new ArgumentException("Predicate must be string or regex or document type or image type");
                this.predicate = predicate;
            }
            #endregion

            #region Serialization
            internal Question(byte[] data) : this(data, Encoding.ASCII) { }
            internal Question(byte[] data, Encoding encoding) : this(new MemoryStream(data), encoding) { }
            internal Question(Stream stream) : this(stream, Encoding.ASCII) { }
            internal Question(Stream stream, Encoding encoding) : this(new BinaryReader(stream, encoding)) { }
            internal Question(BinaryReader reader) : base(reader)
            {
                this.predicate = Predicate.Decode(reader);
            }

            internal override void Write(BinaryWriter writer)
            {
                Predicate.Encode(predicate, writer);
            }
            #endregion

            #region Functions
            public override bool Evaluate(string library, CardData data)
            {
                return inverted ? !predicate.Evaluate(data.Question) : predicate.Evaluate(data.Question);
            }
            public override bool Equivalent(Filter other)
            {
                if(!(other is Question)) return false;
                Question otherQuestion = (Question)other;
                return predicate.Equivalent(otherQuestion.predicate) && inverted == other.inverted;
            }
            #endregion
        }

        public sealed class Answer : Filter
        {
            #region Variables
            private readonly Predicate predicate;
            #endregion

            #region Accessors
            public new Predicate Predicate { get { return predicate; } }
            #endregion

            #region Constructor
            public Answer(Predicate predicate, bool inverted = false) : base(inverted)
            {
                if(predicate == null) throw new ArgumentNullException("Null predicate");
                if(!(predicate is Predicate.Str) && !(predicate is Predicate.Rgx) && !(predicate is Predicate.DocumentType) && !(predicate is Predicate.ImageType))
                    throw new ArgumentException("Predicate must be string or regex or document type or image type");
                this.predicate = predicate;
            }
            #endregion

            #region Serialization
            internal Answer(byte[] data) : this(data, Encoding.ASCII) { }
            internal Answer(byte[] data, Encoding encoding) : this(new MemoryStream(data), encoding) { }
            internal Answer(Stream stream) : this(stream, Encoding.ASCII) { }
            internal Answer(Stream stream, Encoding encoding) : this(new BinaryReader(stream, encoding)) { }
            internal Answer(BinaryReader reader) : base(reader)
            {
                this.predicate = Predicate.Decode(reader);
                this.inverted = reader.ReadBoolean();
            }

            internal override void Write(BinaryWriter writer)
            {
                Predicate.Encode(predicate, writer);
                writer.Write(inverted);
            }
            #endregion

            #region Functions
            public override bool Evaluate(string library, CardData data)
            {
                return inverted ? !predicate.Evaluate(data.Answer) : predicate.Evaluate(data.Answer);
            }
            public override bool Equivalent(Filter other)
            {
                if(this == other) return true;
                if(!(other is Answer)) return false;
                Answer otherAnswer = (Answer)other;
                return predicate.Equivalent(otherAnswer.predicate) && inverted == other.inverted;
            }
            #endregion
        }


        public abstract class Predicate
        {
            private static string convertToText(string rtf)
            {
                using(System.Windows.Forms.RichTextBox rtb = new System.Windows.Forms.RichTextBox())
                {
                    rtb.Rtf = rtf;
                    return rtb.Text;
                }
            }

            internal static Predicate Decode(byte[] data) { return Decode(data, Encoding.ASCII); }
            internal static Predicate Decode(byte[] data, Encoding encoding) { return Decode(new MemoryStream(data), encoding); }
            internal static Predicate Decode(Stream stream) { return Decode(stream, Encoding.ASCII); }
            internal static Predicate Decode(Stream stream, Encoding encoding) { return Decode(new BinaryReader(stream, encoding)); }
            internal static Predicate Decode(BinaryReader reader)
            {
                switch(reader.ReadByte())
                {
                    case 0: return new Str(reader);
                    case 1: return new Rgx(reader);
                    case 2: return new Count(reader);
                    case 3: return DocumentType.Instance;
                    case 4: return ImageType.Instance;
                    default: throw new Exception();
                }
            }
            internal static byte[] Encode(Predicate predicate) { return Encode(predicate, Encoding.ASCII); }
            internal static byte[] Encode(Predicate predicate, Encoding encoding) { MemoryStream stream = new MemoryStream(); Encode(predicate, stream, encoding); stream.Close(); return stream.ToArray(); }
            internal static void Encode(Predicate predicate, Stream stream) { Encode(predicate, stream, Encoding.ASCII); }
            internal static void Encode(Predicate predicate, Stream stream, Encoding encoding) { Encode(predicate, new BinaryWriter(stream, encoding)); }
            internal static void Encode(Predicate predicate, BinaryWriter writer)
            {
                if(predicate is Str)
                {
                    writer.Write((byte)0);
                    ((Str)predicate).Write(writer);
                    return;
                }

                if(predicate is Rgx)
                {
                    writer.Write((byte)1);
                    ((Rgx)predicate).Write(writer);
                    return;
                }

                if(predicate is Count)
                {
                    writer.Write((byte)2);
                    ((Count)predicate).Write(writer);
                    return;
                }

                if(predicate.GetType() == typeof(DocumentType))
                    writer.Write((byte)3);

                else if(predicate.GetType() == typeof(ImageType))
                    writer.Write((byte)4);
            }

            public virtual bool Evaluate(string library)
            {
                throw new NotImplementedException();
            }
            public virtual bool Evaluate(TagCollection tags)
            {
                throw new NotImplementedException();
            }
            public virtual bool Evaluate(Field field)
            {
                throw new NotImplementedException();
            }
            public abstract bool Equivalent(Predicate other);

            public class Str : Predicate
            {
                private readonly string value;
                private readonly bool fullMatch;
                private readonly bool ignoreCase;
                public string Value { get { return value; } }
                public bool FullMatch { get { return fullMatch; } }
                public bool IgnoreCase { get { return ignoreCase; } }
                public Str(string value, bool fullMatch, bool ignoreCase) { if(value == null) throw new ArgumentNullException("null value"); this.value = value; this.fullMatch = fullMatch; this.ignoreCase = ignoreCase; }
                internal Str(BinaryReader reader)
                {
                    this.value = reader.ReadString();
                    this.fullMatch = reader.ReadBoolean();
                    this.ignoreCase = reader.ReadBoolean();
                }
                internal void Write(BinaryWriter writer)
                {
                    writer.Write(value);
                    writer.Write(fullMatch);
                    writer.Write(ignoreCase);
                }
                public override bool Evaluate(string library)
                {
                    if(fullMatch)
                    {
                        if(!ignoreCase)
                            return library.ToLower() == value.ToLower();
                        return library == value;
                    }
                    if(!ignoreCase)
                        return library.ToLower().Contains(value.ToLower());
                    return library.Contains(value);
                }
                public override bool Evaluate(TagCollection tags)
                {
                    if(fullMatch)
                    {
                        if(ignoreCase)
                        {
                            foreach(string tag in tags)
                                if(tag.ToLower() == value.ToLower())
                                    return true;
                            return false;
                        }
                        foreach(string tag in tags)
                            if(tag == value)
                                return true;
                        return false;
                    }
                    if(ignoreCase)
                    {
                        foreach(string tag in tags)
                            if(tag.ToLower().Contains(value.ToLower()))
                                return true;
                        return false;
                    }
                    foreach(string tag in tags)
                        if(tag.Contains(value))
                            return true;
                    return false;
                }
                public override bool Evaluate(Field field)
                {
                    Field.RTF document = field as Field.RTF;
                    if(document == null) return true;
                    return Evaluate(convertToText(Encoding.ASCII.GetString(document.Data)));
                }
                public override bool Equivalent(Predicate other)
                {
                    if(this == other) return true;
                    if(!(other is Str)) return false;
                    Str otherString = (Str)other;
                    return value == otherString.value && fullMatch == otherString.fullMatch && ignoreCase == otherString.ignoreCase;
                }
            }
            public class Rgx : Predicate
            {
                private readonly Regex value;
                public Regex Value { get { return value; } }
                public Rgx(Regex value) { if(value == null) throw new ArgumentNullException("null value"); this.value = value; }
                internal Rgx(BinaryReader reader)
                {
                    this.value = new Regex(reader.ReadString());
                }
                internal void Write(BinaryWriter writer)
                {
                    writer.Write(value.ToString());
                }
                public override bool Evaluate(string library)
                {
                    return value.IsMatch(library);
                }
                public override bool Evaluate(TagCollection tags)
                {
                    foreach(string tag in tags)
                        if(value.IsMatch(tag)) return true;
                    return false;
                }
                public override bool Evaluate(Field field)
                {
                    Field.RTF document = field as Field.RTF;
                    if(document == null) return true;
                    return Evaluate(convertToText(Encoding.ASCII.GetString(document.Data)));
                }
                public override bool Equivalent(Predicate other)
                {
                    if(this == other) return true;
                    if(!(other is Rgx)) return false;
                    return value == ((Rgx)other).value;
                }
            }
            public class Count : Predicate
            {
                public enum Operation : byte
                {
                    Equals,
                    NotEquals,
                    Greater,
                    Less,
                    GreaterOrEqual,
                    LessOrEqual,
                }
                private readonly int value;
                private readonly Operation operation;
                public int Value { get { return value; } }
                public Operation Op { get { return operation; } }
                public Count(int value, Operation operation) { if(value < 0) throw new ArgumentOutOfRangeException("count < 0"); this.value = value; this.operation = operation; }
                internal Count(BinaryReader reader)
                {
                    this.value = reader.ReadInt32();
                    this.operation = (Operation)reader.ReadByte();
                }
                internal void Write(BinaryWriter writer)
                {
                    writer.Write(value);
                    writer.Write((byte)operation);
                }
                public override bool Evaluate(TagCollection tags)
                {
                    switch(operation)
                    {
                        case Operation.NotEquals: return tags.Count != value;
                        case Operation.Greater: return tags.Count > value;
                        case Operation.Less: return tags.Count < value;
                        case Operation.GreaterOrEqual: return tags.Count >= value;
                        case Operation.LessOrEqual: return tags.Count <= value;
                        default: return tags.Count == value;
                    }
                }
                public override bool Equivalent(Predicate other)
                {
                    if(!(other is Count)) return false;
                    Count otherCount = (Count)other;
                    return value == otherCount.value && operation == otherCount.operation;
                }
            }
            public class DocumentType : Predicate
            {
                public static readonly DocumentType Instance = new DocumentType();
                private DocumentType() { }
                public override bool Evaluate(Field field)
                {
                    return field is Field.RTF;
                }
                public override bool Equivalent(Predicate other)
                {
                    return other == this;
                }
            }
            public class ImageType : Predicate
            {
                public static readonly ImageType Instance = new ImageType();
                private ImageType() { }
                public override bool Evaluate(Field field)
                {
                    return field is Field.Image;
                }
                public override bool Equivalent(Predicate other)
                {
                    return other == this;
                }
            }
        }
        #endregion

        #region Variables
        protected bool inverted;
        #endregion

        #region Accessors
        public bool Inverted { get { return inverted; } }
        #endregion

        #region Constructor
        internal Filter(bool inverted = false) { this.inverted = inverted; }
        #endregion

        #region Serialization
        internal Filter(byte[] data) : this(data, Encoding.ASCII) { }
        internal Filter(byte[] data, Encoding encoding) : this(new MemoryStream(data), encoding) { }
        internal Filter(Stream stream) : this(stream, Encoding.ASCII) { }
        internal Filter(Stream stream, Encoding encoding) : this(new BinaryReader(stream, encoding)) { }
        internal Filter(BinaryReader reader)
        {
            inverted = reader.ReadBoolean();
        }

        internal byte[] Write() { return Write(Encoding.ASCII); }
        internal byte[] Write(Encoding encoding) { MemoryStream stream = new MemoryStream(); Write(stream, encoding); stream.Close(); return stream.ToArray(); }
        internal void Write(Stream stream) { Write(stream, Encoding.ASCII); }
        internal void Write(Stream stream, Encoding encoding) { Write(new BinaryWriter(stream, encoding)); }
        internal virtual void Write(BinaryWriter writer)
        {
            writer.Write(inverted);
        }
        #endregion
        
        #region Functions
        public abstract bool Evaluate(string library, CardData data);
        public abstract bool Equivalent(Filter other);
        #endregion
    }
}