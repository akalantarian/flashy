﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Sockets;

internal sealed class MessageQueue<T> : IDisposable
{
    #region Events
    public event Action<T> Process;
    #endregion

    #region Variables
    private readonly ConcurrentQueue<T> queue;
    private readonly AutoResetEvent messageEvt;
    private readonly ManualResetEvent disposeEvt;
    private readonly ManualResetEvent endEvt;
    private readonly Task task;
    private readonly object lockObject = new object();
    #endregion

    #region Accessors
    public object LockObject { get { return lockObject; } }
    #endregion

    #region Constructor
    public MessageQueue()
    {
        this.queue = new ConcurrentQueue<T>();
        this.messageEvt = new AutoResetEvent(false);
        this.disposeEvt = new ManualResetEvent(false);
        this.endEvt = new ManualResetEvent(false);
        this.task = new Task(proc, TaskCreationOptions.LongRunning);
    }
    ~MessageQueue() { Dispose(); }
    #endregion

    #region Functions
    public void Dispose()
    {
        lock (task)
            if(disposeEvt != null) disposeEvt.Set();
    }

    public void Start()
    {
        lock (task)
            if(!disposeEvt.WaitOne(0) && task.Status == TaskStatus.Created)
                task.Start();
    }

    public void Wait()
    {
        endEvt.WaitOne();
    }

    public void Enqueue(T message)
    {
        queue.Enqueue(message);
        messageEvt.Set();
    }

    private void proc()
    {
        WaitHandle[] handles = new WaitHandle[] { disposeEvt, messageEvt };
        while(WaitHandle.WaitAny(handles) == 1)
        {
            T message;
            while(queue.TryDequeue(out message))
            {
                lock (lockObject)
                {
                    Action<T> process = Process;
                    if(process != null)
                        process(message);
                }
            }
        }
        endEvt.Set();
    }
    #endregion
}