﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Flashy.Client.GUI.FilterControls.PredicateControls;

namespace Flashy.Client.GUI.FilterControls.SubjectControls
{
    internal partial class LibraryControl : UserControl
    {
        public event Action ItemChanged;
        private void onItemChanged() { Action del = ItemChanged; if(del != null) del(); }

        public LibraryControl() : this(null) { }
        public LibraryControl(Filter.Library filter)
        {
            InitializeComponent();
            Predicate.ItemChanged += onItemChanged;
            Value = filter;
            Predicate.Value = null;
        }

        public Filter.Library Value
        {
            get
            {
                return new Filter.Library(Predicate.Value, OpBox.SelectedIndex == 1);
            }
            set
            {
                this.invoke(() =>
                {
                    if(value == null)
                    {
                        OpBox.SelectedIndex = 0;
                        Predicate.Value = null;
                    }
                    else
                    {
                        OpBox.SelectedIndex = value.Inverted ? 1 : 0;
                        Predicate.Value = value.Predicate;
                    }
                });
            }
        }

        private void selectionChanged(object sender, SelectionChangedEventArgs e)
        {
            onItemChanged();
        }
    }
}
