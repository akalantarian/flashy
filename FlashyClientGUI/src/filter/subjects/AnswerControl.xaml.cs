﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Flashy.Client.GUI.FilterControls.PredicateControls;

namespace Flashy.Client.GUI.FilterControls.SubjectControls
{
    internal partial class AnswerControl : UserControl
    {
        public event Action ItemChanged;
        private void onItemChanged() { Action del = ItemChanged; if(del != null) del(); }

        private readonly FieldTypeControl fieldTypeControl = new FieldTypeControl();
        private readonly TextControl textControl = new TextControl();

        public AnswerControl() : this(null) { }
        public AnswerControl(Filter.Answer filter)
        {
            InitializeComponent();
            fieldTypeControl.ItemChanged += onItemChanged;
            textControl.ItemChanged += onItemChanged;
            Value = filter;
        }

        private void typeSelected(object sender, RoutedEventArgs e)
        {
            predicate = fieldTypeControl;
            onItemChanged();
        }
        private void textSelected(object sender, RoutedEventArgs e)
        {
            predicate = textControl;
            onItemChanged();
        }

        private UserControl predicate
        {
            get
            {
                if(Predicate.Children.Count == 0) return null;
                return (UserControl)Predicate.Children[0];
            }
            set
            {
                if(predicate == value) return;
                this.invoke(() =>
                {
                    Predicate.Children.Clear();
                    Predicate.Children.Add(value);
                });
            }
        }

        public Filter.Answer Value
        {
            get
            {
                UserControl pred = predicate;
                if(pred == fieldTypeControl)
                    return new Filter.Answer(fieldTypeControl.Value, OpBox.SelectedIndex == 1);
                else if(pred == textControl)
                    return new Filter.Answer(textControl.Value, OpBox.SelectedIndex == 3);
                else if(pred == null)
                    return null;
                else
                    throw new Exception();
            }
            set
            {
                this.invoke(() =>
                {
                    if(value == null)
                    {
                        OpBox.SelectedIndex = 0;
                        fieldTypeControl.Value = null;
                        textControl.Value = null;
                    }
                    else
                    {
                        if(value.Predicate is Filter.Predicate.DocumentType || value.Predicate is Filter.Predicate.ImageType)
                        {
                            OpBox.SelectedIndex = value.Inverted ? 1 : 0;
                            fieldTypeControl.Value = value.Predicate;
                            textControl.Value = null;
                        }
                        else if(value.Predicate is Filter.Predicate.Str || value.Predicate is Filter.Predicate.Rgx)
                        {
                            OpBox.SelectedIndex = value.Inverted ? 3 : 2;
                            fieldTypeControl.Value = null;
                            textControl.Value = value.Predicate;
                        }
                        else
                            throw new Exception();
                    }
                });
            }
        }
    }
}
