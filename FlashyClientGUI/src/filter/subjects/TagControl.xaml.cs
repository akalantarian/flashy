﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Flashy.Client.GUI.FilterControls.PredicateControls;

namespace Flashy.Client.GUI.FilterControls.SubjectControls
{
    internal partial class TagControl : UserControl
    {
        public event Action ItemChanged;
        private void onItemChanged() { Action del = ItemChanged; if(del != null) del(); }

        private readonly TextControl textControl = new TextControl();
        private readonly CountControl countControl = new CountControl();

        public TagControl() : this(null) { }
        public TagControl(Filter.Tag filter)
        {
            InitializeComponent();
            textControl.ItemChanged += onItemChanged;
            countControl.ItemChanged += onItemChanged;
            Value = filter;
        }

        private void textSelected(object sender, RoutedEventArgs e)
        {
            predicate = textControl;
            onItemChanged();
        }
        private void countSelected(object sender, RoutedEventArgs e)
        {
            predicate = countControl;
            onItemChanged();
        }

        private UserControl predicate
        {
            get
            {
                if(Predicate.Children.Count == 0) return null;
                return (UserControl)Predicate.Children[0];
            }
            set
            {
                if(predicate == value) return;
                this.invoke(() =>
                {
                    Predicate.Children.Clear();
                    Predicate.Children.Add(value);
                });
            }
        }

        public Filter.Tag Value
        {
            get
            {
                UserControl pred = predicate;
                if(pred == textControl)
                    return new Filter.Tag(textControl.Value, OpBox.SelectedIndex == 1);
                else if(pred == countControl)
                    return new Filter.Tag(countControl.Value, false);
                else if(pred == null)
                    return null;
                else
                    throw new Exception();
            }
            set
            {
                this.invoke(() =>
                {
                    if(value == null)
                    {
                        OpBox.SelectedIndex = 0;
                        textControl.Value = null;
                        countControl.Value = null;
                        predicate = textControl;
                    }
                    else
                    {
                        if(value.Predicate is Filter.Predicate.Str || value.Predicate is Filter.Predicate.Rgx)
                        {
                            OpBox.SelectedIndex = value.Inverted ? 1 : 0;
                            textControl.Value = value.Predicate;
                            countControl.Value = null;
                            predicate = textControl;
                        }
                        else if(value.Predicate is Filter.Predicate.Count)
                        {
                            OpBox.SelectedIndex = 3;
                            textControl.Value = null;
                            countControl.Value = (Filter.Predicate.Count)value.Predicate;
                            predicate = countControl;
                        }
                        else
                            throw new Exception();
                    }
                });
            }
        }
    }
}
