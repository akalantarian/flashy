﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Flashy.Client.GUI.FilterControls
{
    internal partial class LibrarySelector : UserControl
    {
        public event Action ListChanged;
        private void listChanged() { Action del = ListChanged; if(del != null) del(); }

        private readonly ObservableCollection<string> libraries = new ObservableCollection<string>();
        private readonly ObservableCollection<string> activeLibraries = new ObservableCollection<string>();
        public IList<string> Libraries
        {
            get
            {
                return new List<string>(libraries);
            }
            set
            {
                libraries.Clear();
                if(value != null)
                    foreach(string library in value.OrderBy((x) => x))
                        if(Settings.CheckLibraryName(library))
                            libraries.Add(library);
            }
        }
        public IList<string> ActiveLibraries
        {
            get
            {
                return new List<string>(activeLibraries);
            }
            set
            {
                activeLibraries.Clear();
                if(value != null)
                    foreach(string library in value.OrderBy((x) => x))
                        if(Settings.CheckLibraryName(library))
                            activeLibraries.Add(library);
            }
        }
        public LibrarySelector()
        {
            InitializeComponent();
            Chooser.ItemsSource = libraries;
            SelectedLibraries.ItemsSource = activeLibraries;
        }

        private void removeLibrary(object sender, RoutedEventArgs e)
        {
            List<string> removes = new List<string>(SelectedLibraries.SelectedItems.Count);
            foreach(string library in SelectedLibraries.SelectedItems)
                removes.Add(library);
            ActiveLibraries = new List<string>(libraries.Except(removes));
            ListChanged();
        }

        private void addLibrary(object sender, RoutedEventArgs e)
        {
            if(Settings.CheckLibraryName(Chooser.Text))
            {
                if(!activeLibraries.ContainsIgnoreCase(Chooser.Text))
                {
                    activeLibraries.Add(Chooser.Text);
                    ListChanged();
                }
            }
            else
                MessageBox.Show("\"" + Chooser.Text + "\" is not a valid library name");
        }

        private void textChanged(object sender, RoutedEventArgs e)
        {
            string libraryString = Chooser.Text;
            if(string.IsNullOrEmpty(libraryString)) return;
            if(Settings.CheckLibraryName(libraryString))
                Chooser.Foreground = new SolidColorBrush(Colors.Green);
            else
                Chooser.Foreground = new SolidColorBrush(Colors.Red);
        }
    }
}
