﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Flashy.Client.GUI.FilterControls.SubjectControls;

namespace Flashy.Client.GUI.FilterControls
{
    internal partial class ConditionControl : UserControl
    {
        internal enum ViewType
        {
            Solo,
            Start,
            Center,
            End
        }

        public event Action<ConditionControl> CloseClick;
        public event Action<ConditionControl> UpClick;
        public event Action<ConditionControl> DownClick;
        public event Action ItemChanged;
        private void onItemChanged() { Action del = ItemChanged; if(del != null) del(); }

        private readonly LibraryControl libraryControl = new LibraryControl();
        private readonly TagControl tagControl = new TagControl();
        private readonly QuestionControl questionControl = new QuestionControl();
        private readonly AnswerControl answerControl = new AnswerControl();

        private ViewType type = ViewType.Center;
        private OperationControl prevOp = null, postOp = null;

        public ViewType Type { get { return type; } }
        public OperationControl PrevOp { get { return prevOp; } set { prevOp = value; } }
        public OperationControl PostOp { get { return postOp; } set { postOp = value; } }

        public ConditionControl() : this(null) { }
        public ConditionControl(Filter filter = null)
        {
            InitializeComponent();
            libraryControl.ItemChanged += onItemChanged;
            tagControl.ItemChanged += onItemChanged;
            questionControl.ItemChanged += onItemChanged;
            answerControl.ItemChanged += onItemChanged;
            Value = filter;
        }

        private void CloseButtonClick(object sender, RoutedEventArgs e)
        {
            Action<ConditionControl> del = CloseClick;
            if(del != null) del(this);
        }
        private void UpButtonClick(object sender, RoutedEventArgs e)
        {
            Action<ConditionControl> del = UpClick;
            if(del != null) del(this);
        }
        private void DownButtonClick(object sender, RoutedEventArgs e)
        {
            Action<ConditionControl> del = DownClick;
            if(del != null) del(this);
        }

        public void SetType(ViewType type)
        {
            if(type == this.type) return;
            switch(type)
            {
                case ViewType.Solo: SetSolo(); break;
                case ViewType.Start: SetStart(); break;
                case ViewType.End: SetEnd(); break;
                default: SetCenter(); break;
            }
        }
        public void SetSolo()
        {
            type = ViewType.Solo;
            UpButton.Visibility = Visibility.Hidden;
            DownButton.Visibility = Visibility.Hidden;
        }
        public void SetStart()
        {
            type = ViewType.Start;
            UpButton.Visibility = Visibility.Hidden;
            DownButton.Visibility = Visibility.Visible;
        }
        public void SetEnd()
        {
            type = ViewType.End;
            UpButton.Visibility = Visibility.Visible;
            DownButton.Visibility = Visibility.Hidden;
        }
        public void SetCenter()
        {
            type = ViewType.Center;
            UpButton.Visibility = Visibility.Visible;
            DownButton.Visibility = Visibility.Visible;
        }

        private void librarySelected(object sender, RoutedEventArgs e)
        {
            subject = libraryControl;
            onItemChanged();
        }
        private void tagSelected(object sender, RoutedEventArgs e)
        {
            subject = tagControl;
            onItemChanged();
        }
        private void questionSelected(object sender, RoutedEventArgs e)
        {
            subject = questionControl;
            onItemChanged();
        }
        private void answerSelected(object sender, RoutedEventArgs e)
        {
            subject = answerControl;
            onItemChanged();
        }

        private UserControl subject
        {
            get
            {
                if(Subject.Children.Count == 0) return null;
                return (UserControl)Subject.Children[0];
            }
            set
            {
                if(subject == value) return;
                this.invoke(() =>
                {
                    Subject.Children.Clear();
                    Subject.Children.Add(value);
                });
            }
        }

        public Filter Value
        {
            get
            {
                UserControl subj = subject;
                if(subj == libraryControl)
                    return libraryControl.Value;
                if(subj == tagControl)
                    return tagControl.Value;
                if(subj == questionControl)
                    return questionControl.Value;
                if(subj == answerControl)
                    return answerControl.Value;
                else if(subj == null)
                    return null;
                else
                    throw new Exception();
            }
            set
            {
                this.invoke(() =>
                {
                    if(value == null)
                    {
                        TypeBox.SelectedIndex = -1;
                        libraryControl.Value = null;
                        tagControl.Value = null;
                        questionControl.Value = null;
                        answerControl.Value = null;
                        subject = null;
                    }
                    else
                    {
                        if(value is Filter.Library)
                        {
                            TypeBox.SelectedIndex = 0;
                            libraryControl.Value = (Filter.Library)value;
                            tagControl.Value = null;
                            questionControl.Value = null;
                            answerControl.Value = null;
                        }
                        else if(value is Filter.Tag)
                        {
                            TypeBox.SelectedIndex = 1;
                            libraryControl.Value = null;
                            tagControl.Value = (Filter.Tag)value;
                            questionControl.Value = null;
                            answerControl.Value = null;
                        }
                        else if(value is Filter.Question)
                        {
                            TypeBox.SelectedIndex = 2;
                            libraryControl.Value = null;
                            tagControl.Value = null;
                            questionControl.Value = (Filter.Question)value;
                            answerControl.Value = null;
                        }
                        else if(value is Filter.Answer)
                        {
                            TypeBox.SelectedIndex = 3;
                            libraryControl.Value = null;
                            tagControl.Value = null;
                            questionControl.Value = null;
                            answerControl.Value = (Filter.Answer)value;
                        }
                        else
                            throw new Exception();
                    }
                });
            }
        }
    }
}
