﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Runtime.InteropServices;
using System.Drawing.Imaging;

namespace Flashy.Client.GUI.FilterControls
{
    internal partial class OperationControl : UserControl
    {
        internal enum Operation
        {
            And,
            Or
        }
        internal enum ViewType
        {
            Start,
            Center,
            End
        }
        
        public event Action ItemChanged;
        public event Action BraceChanged;
        private void onItemChanged() { Action del = ItemChanged; if(del != null) del(); }
        private void onBraceChanged() { Action del = BraceChanged; if(del != null) del(); }

        private Operation operation;
        private ViewType type = ViewType.Center;
        private List<TextBlock> closeBraces = new List<TextBlock>();
        private List<TextBlock> openBraces = new List<TextBlock>();

        public Operation Op { get { return operation; } }
        public ViewType Type { get { return type; } }
        public int NumCloseBraces { get { return CloseBracePanel.Children.Count; } }
        public int NumOpenBraces { get { return OpenBracePanel.Children.Count; } }

        public OperationControl() : this(Operation.And, ViewType.Center) { }
        public OperationControl(int openBraces, int closeBraces = 0) : this(Operation.And, ViewType.Center, openBraces, closeBraces) { }
        public OperationControl(ViewType type, int openBraces = 0, int closeBraces = 0) : this(Operation.And, type, openBraces, closeBraces) { }
        public OperationControl(Operation operation, int openBraces = 0, int closeBraces = 0) : this(operation, ViewType.Center, openBraces, closeBraces) { }
        public OperationControl(Operation operation, ViewType type, int openBraces = 0, int closeBraces = 0)
        {
            InitializeComponent();
            
            SetOp(operation);
            SetType(type);

            for(int i = 0; i < closeBraces; i++)
                AddCloseBrace();

            for(int i = 0; i < openBraces; i++)
                AddOpenBrace();
        }

        public void AddCloseBrace(int colorIndex = -1)
        {
            TextBlock x = new TextBlock();
            x.Style = (Style)Resources["MouseOverBolder"];
            x.Text = ")";
            x.Foreground = getBrush(colorIndex);
            x.MouseDown += removeCloseBrace;
            CloseBracePanel.Children.Insert(0, x);
        }
        private void addCloseBrace(object sender, MouseButtonEventArgs e)
        {
            AddCloseBrace();
            onBraceChanged();
            onItemChanged();
        }
        public void SetCloseBrace(int braceIndex, int colorIndex = -1)
        {
            ((TextBlock)CloseBracePanel.Children[CloseBracePanel.Children.Count - braceIndex - 1]).Foreground = getBrush(colorIndex);
        }
        public void RemoveCloseBrace(int braceIndex)
        {
            CloseBracePanel.Children.RemoveAt(CloseBracePanel.Children.Count - braceIndex - 1);
        }
        private void removeCloseBrace(object sender, MouseButtonEventArgs e)
        {
            CloseBracePanel.Children.Remove((TextBlock)sender);
            onBraceChanged();
            onItemChanged();
        }
        public void RemoveCloseBraces()
        {
            CloseBracePanel.Children.Clear();
        }

        public void AddOpenBrace(int colorIndex = -1)
        {
            TextBlock x = new TextBlock();
            x.Style = (Style)Resources["MouseOverBolder"];
            x.Text = "(";
            x.Foreground = getBrush(colorIndex);
            x.MouseDown += removeOpenBrace;
            OpenBracePanel.Children.Add(x);
        }
        private void addOpenBrace(object sender, MouseButtonEventArgs e)
        {
            AddOpenBrace();
            onBraceChanged();
            onItemChanged();
        }
        public void SetOpenBrace(int braceIndex, int colorIndex = -1)
        {
            ((TextBlock)OpenBracePanel.Children[braceIndex]).Foreground = getBrush(colorIndex);
        }
        public void RemoveOpenBrace(int braceIndex)
        {
            OpenBracePanel.Children.RemoveAt(braceIndex);
        }
        private void removeOpenBrace(object sender, MouseButtonEventArgs e)
        {
            OpenBracePanel.Children.Remove((TextBlock)sender);
            onBraceChanged();
            onItemChanged();
        }
        public void RemoveOpenBraces()
        {
            OpenBracePanel.Children.Clear();
        }
        public void RemoveBraces()
        {
            RemoveCloseBraces();
            RemoveOpenBraces();
        }

        private Brush getBrush(int colorIndex)
        {
            if(colorIndex == -1) return (Brush)Resources["ActiveBrush"];
            else if(colorIndex == -2) return (Brush)Resources["ErrorBrush"];
            switch(colorIndex % 3)
            {
                case 0: return (Brush)Resources["Level1Brush"];
                case 1: return (Brush)Resources["Level2Brush"];
                case 2: return (Brush)Resources["Level3Brush"];
                default: return (Brush)Resources["ErrorBrush"];
            }
        }

        public void SetOp(Operation operation)
        {
            if(operation == this.operation) return;
            switch(operation)
            {
                case Operation.Or: SetOr(); break;
                default: SetAnd(); break;
            }
        }
        public void SwapOp()
        {
            switch(operation)
            {
                case Operation.And: SetOr(); break;
                default: SetAnd(); break;
            }
        }
        public void SetAnd()
        {
            operation = Operation.And;
            OpText.Text = "And";
        }
        public void SetOr()
        {
            operation = Operation.Or;
            OpText.Text = "Or";
        }
        private void swapOp(object sender, MouseButtonEventArgs e)
        {
            SwapOp();
            onItemChanged();
        }
        

        public void SetType(ViewType type)
        {
            if(type == this.type) return;
            switch(type)
            {
                case ViewType.Start: SetStart(); break;
                case ViewType.End: SetEnd(); break;
                default: SetCenter(); break;
            }
        }
        public void SetStart()
        {
            type = ViewType.Start;
            CloseBraceView.Visibility = Visibility.Hidden;
            OpText.Visibility = Visibility.Hidden;
            OpenBraceView.Visibility = Visibility.Visible;
        }
        public void SetEnd()
        {
            type = ViewType.End;
            CloseBraceView.Visibility = Visibility.Visible;
            OpText.Visibility = Visibility.Hidden;
            OpenBraceView.Visibility = Visibility.Hidden;
        }
        public void SetCenter()
        {
            type = ViewType.Center;
            CloseBraceView.Visibility = Visibility.Visible;
            OpText.Visibility = Visibility.Visible;
            OpenBraceView.Visibility = Visibility.Visible;
        }
    }
}
