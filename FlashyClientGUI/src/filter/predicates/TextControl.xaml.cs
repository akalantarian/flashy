﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Flashy.Client.GUI.FilterControls.PredicateControls
{
    internal partial class TextControl : UserControl
    {
        public event Action ItemChanged;
        private void onItemChanged() { Action del = ItemChanged; if(del != null) del(); }

        public TextControl() : this(null) { }
        public TextControl(Filter.Predicate predicate)
        {
            if(predicate != null && !(predicate is Filter.Predicate.Str) && !(predicate is Filter.Predicate.Rgx))
                throw new Exception();
            InitializeComponent();
            Value = predicate;
        }

        public Filter.Predicate Value
        {
            get
            {
                if(StrButton.IsChecked.Value)
                    return new Filter.Predicate.Str(StringBox.Text, FullMatchBox.IsChecked.Value, IgnoreCaseBox.IsChecked.Value);
                else if(RgxButton.IsChecked.Value)
                    return new Filter.Predicate.Rgx(new Regex(StringBox.Text, IgnoreCaseBox.IsChecked.Value ? RegexOptions.Compiled | RegexOptions.IgnoreCase : RegexOptions.Compiled));
                else throw new Exception();
            }
            set
            {
                this.invoke(() =>
                {
                    if(value == null)
                    {
                        StringBox.Text = string.Empty;
                        StrButton.IsChecked = true;
                        FullMatchBox.IsChecked = false;
                        IgnoreCaseBox.IsChecked = false;
                    }
                    else
                    {
                        if(value is Filter.Predicate.Str)
                        {
                            Filter.Predicate.Str predicate = (Filter.Predicate.Str)value;
                            StringBox.Text = predicate.Value;
                            StrButton.IsChecked = true;
                            FullMatchBox.IsChecked = predicate.FullMatch;
                            IgnoreCaseBox.IsChecked = predicate.IgnoreCase;
                        }
                        else if(value is Filter.Predicate.Rgx)
                        {
                            Filter.Predicate.Rgx predicate = (Filter.Predicate.Rgx)value;
                            StringBox.Text = predicate.Value.ToString();
                            RgxButton.IsChecked = true;
                            FullMatchBox.IsChecked = false;
                            IgnoreCaseBox.IsChecked = false;
                        }
                        else
                            throw new Exception();
                    }
                });
            }
        }

        private void checkRegex()
        {
            try
            {
                new Regex(StringBox.Text);
                StringBox.Foreground = new SolidColorBrush(Colors.Green);
            }
            catch(Exception)
            {
                StringBox.Foreground = new SolidColorBrush(Colors.Red);
            }
        }

        private void textChanged(object sender, TextChangedEventArgs e)
        {
            if(RgxButton.IsChecked.Value) checkRegex();
            onItemChanged();
        }
        private void button_Checked(object sender, RoutedEventArgs e)
        {
            onItemChanged();
        }
        private void strButton_Checked(object sender, RoutedEventArgs e)
        {
            StringBox.Foreground = new SolidColorBrush(Colors.Black);
            onItemChanged();
        }
        private void rgxButton_Checked(object sender, RoutedEventArgs e)
        {
            checkRegex();
            onItemChanged();
        }
    }
}
