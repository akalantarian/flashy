﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Flashy.Client.GUI.FilterControls.PredicateControls
{
    internal partial class CountControl : UserControl
    {
        public event Action ItemChanged;
        private void onItemChanged() { Action del = ItemChanged; if(del != null) del(); }

        private static readonly Regex digitRegex = new Regex(@"^\d+$", RegexOptions.Compiled);

        public CountControl() : this(null) { }
        public CountControl(Filter.Predicate.Count predicate)
        {
            InitializeComponent();
            Value = predicate;
        }
        

        public Filter.Predicate.Count Value
        {
            get
            {
                int count = 0;
                int.TryParse(CountTextBox.Text, out count);
                

                switch(CountOpBox.SelectedIndex)
                {
                    case 0: return new Filter.Predicate.Count(count, Filter.Predicate.Count.Operation.Equals);
                    case 1: return new Filter.Predicate.Count(count, Filter.Predicate.Count.Operation.NotEquals);
                    case 2: return new Filter.Predicate.Count(count, Filter.Predicate.Count.Operation.Greater);
                    case 3: return new Filter.Predicate.Count(count, Filter.Predicate.Count.Operation.Less);
                    case 4: return new Filter.Predicate.Count(count, Filter.Predicate.Count.Operation.GreaterOrEqual);
                    default: return new Filter.Predicate.Count(count, Filter.Predicate.Count.Operation.LessOrEqual);
                }
            }
            set
            {
                this.invoke(() =>
                {
                    if(value == null)
                    {
                        CountTextBox.Text = "0";
                        CountOpBox.SelectedIndex = 0;
                    }
                    else
                    {
                        CountTextBox.Text = value.Value.ToString();
                        switch(value.Op)
                        {
                            case Filter.Predicate.Count.Operation.Equals: CountOpBox.SelectedIndex = 0; break;
                            case Filter.Predicate.Count.Operation.NotEquals: CountOpBox.SelectedIndex = 1; break;
                            case Filter.Predicate.Count.Operation.Greater: CountOpBox.SelectedIndex = 2; break;
                            case Filter.Predicate.Count.Operation.Less: CountOpBox.SelectedIndex = 3; break;
                            case Filter.Predicate.Count.Operation.GreaterOrEqual: CountOpBox.SelectedIndex = 4; break;
                            default: CountOpBox.SelectedIndex = 5; break;
                        }
                    }
                });
            }
        }

        private void CountTextPreview(object sender, TextCompositionEventArgs e)
        {
            if(!digitRegex.IsMatch(e.Text)) e.Handled = true;
        }
        private void CountUpButton(object sender, RoutedEventArgs e)
        {
            int count;
            if(!int.TryParse(CountTextBox.Text, out count)) return;
            CountTextBox.Text = (count + 1).ToString();
        }
        private void CountDownButton(object sender, RoutedEventArgs e)
        {
            int count;
            if(!int.TryParse(CountTextBox.Text, out count) || count == 0) return;
            CountTextBox.Text = (count - 1).ToString();
        }

        private void opChanged(object sender, SelectionChangedEventArgs e)
        {
            onItemChanged();
        }

        private void countChanged(object sender, TextChangedEventArgs e)
        {
            onItemChanged();
        }
    }
}
