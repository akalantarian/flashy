﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Flashy.Client.GUI.FilterControls.PredicateControls
{
    internal partial class FieldTypeControl : UserControl
    {
        public event Action ItemChanged;
        private void onItemChanged() { Action del = ItemChanged; if(del != null) del(); }

        public FieldTypeControl() : this(null) { }
        public FieldTypeControl(Filter.Predicate predicate)
        {
            if(predicate != null && !(predicate is Filter.Predicate.DocumentType) && !(predicate is Filter.Predicate.ImageType))
                throw new Exception();
            InitializeComponent();
            Value = predicate;
        }

        public Filter.Predicate Value
        {
            get
            {
                if(FieldTypeBox.SelectedIndex == 0)
                    return Filter.Predicate.DocumentType.Instance;
                else if(FieldTypeBox.SelectedIndex == 1)
                    return Filter.Predicate.ImageType.Instance;
                else throw new Exception();
            }
            set
            {
                this.invoke(() =>
                {
                    if(value == null)
                        FieldTypeBox.SelectedIndex = 0;
                    else
                    {
                        if(value is Filter.Predicate.DocumentType)
                            FieldTypeBox.SelectedIndex = 0;
                        else if(value is Filter.Predicate.ImageType)
                            FieldTypeBox.SelectedIndex = 1;
                        else
                            throw new Exception();
                    }
                });
            }
        }

        private void selectionChanged(object sender, SelectionChangedEventArgs e)
        {
            onItemChanged();
        }
    }
}
