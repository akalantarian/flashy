﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Flashy.Client.GUI.FilterControls
{
    internal partial class FilterControl : UserControl
    {
        public event Action FilterChanged;
        private void filterChanged() { Action del = FilterChanged; if(del != null) del(); }

        public FilterControl() : this(null) { }
        public FilterControl(Filter filter = null)
        {
            InitializeComponent();

            OperationControl op = new OperationControl(OperationControl.ViewType.Start);
            op.Visibility = Visibility.Collapsed;
            op.ItemChanged += filterChanged;
            op.BraceChanged += braceChanged;
            FilterStack.Children.Add(op);
            op = new OperationControl(OperationControl.ViewType.End);
            op.ItemChanged += filterChanged;
            op.BraceChanged += braceChanged;
            op.Visibility = Visibility.Collapsed;
            FilterStack.Children.Add(op);
            Value = filter;
        }

        private int numOperations { get { if(FilterStack.Children.Count == 2) return 2; return FilterStack.Children.Count / 2 + 1; } }
        private OperationControl firstOperation { get { return (OperationControl)FilterStack.Children[0]; } }
        private OperationControl lastOperation { get { return (OperationControl)FilterStack.Children[FilterStack.Children.Count - 1]; } }
        private OperationControl operationAt(int index) { if(index >= numOperations) return null; return (OperationControl)FilterStack.Children[index * 2]; }

        private int numConditions { get { if(FilterStack.Children.Count == 2) return 0; return FilterStack.Children.Count / 2; } }
        private ConditionControl firstCondition { get { if(numConditions == 0) return null; return (ConditionControl)FilterStack.Children[1]; } }
        private ConditionControl lastCondition { get { if(numConditions == 0) return null; return (ConditionControl)FilterStack.Children[FilterStack.Children.Count - 2]; } }
        private ConditionControl conditionAt(int index) { if(index >= numConditions) return null; return (ConditionControl)FilterStack.Children[1 + 2 * index]; }


        private void braceChanged()
        {
            Filter v = Value;
        }

        public void AddCondition()
        {
            addCondition(new ConditionControl(), true);
            filterChanged();
            braceChanged();
        }
        private void addCondition(ConditionControl newCondition, bool isAnd)
        {
            newCondition.CloseClick += removeCondition;
            newCondition.UpClick += moveUp;
            newCondition.DownClick += moveDown;
            newCondition.ItemChanged += filterChanged;
            if(numConditions == 0)
            {
                newCondition.SetType(ConditionControl.ViewType.Solo);
                FilterStack.Children.Insert(1, newCondition);
            }
            else
            {
                if(numConditions == 1)
                {
                    firstOperation.Visibility = Visibility.Visible;
                    firstCondition.SetType(ConditionControl.ViewType.Start);
                    lastOperation.Visibility = Visibility.Visible;
                }
                else
                {
                    lastCondition.SetType(ConditionControl.ViewType.Center);
                }

                if(!isAnd)
                    lastOperation.SetOp(OperationControl.Operation.Or);
                lastOperation.SetType(OperationControl.ViewType.Center);

                newCondition.SetType(ConditionControl.ViewType.End);
                FilterStack.Children.Add(newCondition);
                OperationControl newOp = new OperationControl(OperationControl.ViewType.End);
                newOp.ItemChanged += filterChanged;
                newOp.BraceChanged += braceChanged;
                FilterStack.Children.Add(newOp);
            }
            newCondition.PrevOp = (OperationControl)FilterStack.Children[FilterStack.Children.Count - 3];
            newCondition.PostOp = (OperationControl)FilterStack.Children[FilterStack.Children.Count - 1];
        }
        private void addFilter(Filter filter, bool isAnd, ref int braceLevel)
        {
            if(filter is Filter.Group)
            {
                braceLevel++;
                if(numConditions > 1)
                    lastOperation.AddOpenBrace();
                else
                    firstOperation.AddOpenBrace();
                foreach(List<Filter> andGroup in ((Filter.Group)filter).AndGroups)
                    for(int i = 0; i < andGroup.Count; i++)
                        addFilter(andGroup[i], i != 0, ref braceLevel);
                lastOperation.AddCloseBrace();
            }
            else
            {
                addCondition(new ConditionControl(filter), isAnd);
            }
        }
        private void removeCondition(ConditionControl condition)
        {
            if(numConditions == 1)
            {
                FilterStack.Children.Remove(condition);
                lastOperation.SetAnd();
            }
            else if(numConditions == 2)
            {
                if(condition == lastCondition)
                {
                    FilterStack.Children.RemoveRange(3, 2);
                    firstCondition.PostOp = lastOperation;
                }
                else
                {
                    FilterStack.Children.RemoveRange(1, 2);
                    firstCondition.PrevOp = firstOperation;
                }
                firstCondition.SetType(ConditionControl.ViewType.Solo);
                lastOperation.SetType(OperationControl.ViewType.End);
                firstOperation.Visibility = Visibility.Collapsed;
                lastOperation.Visibility = Visibility.Collapsed;
            }
            else
            {
                if(condition == lastCondition)
                {
                    FilterStack.Children.RemoveRange(FilterStack.Children.Count - 2, 2);
                    lastOperation.SetType(OperationControl.ViewType.End);
                    lastCondition.SetType(ConditionControl.ViewType.End);
                }
                else if(condition == firstCondition)
                {
                    FilterStack.Children.RemoveRange(1, 2);
                    firstCondition.SetType(ConditionControl.ViewType.Start);
                    firstCondition.PrevOp = firstOperation;
                }
                else
                {
                    int index = FilterStack.Children.IndexOf(condition);
                    FilterStack.Children.RemoveRange(index, 2);
                    ((ConditionControl)FilterStack.Children[index]).PrevOp = condition.PrevOp;
                }
            }
            filterChanged();
            braceChanged();
        }
        private void moveUp(ConditionControl condition)
        {
            int index = FilterStack.Children.IndexOf(condition);

            ConditionControl target = (ConditionControl)FilterStack.Children[index - 2];
            OperationControl a = target.PrevOp;
            OperationControl b = target.PostOp;
            OperationControl c = condition.PostOp;

            FilterStack.Children.RemoveAt(index);
            FilterStack.Children.RemoveAt(index - 2);

            FilterStack.Children.Insert(index - 2, condition);
            FilterStack.Children.Insert(index, target);

            target.PrevOp = b;
            target.PostOp = c;
            condition.PrevOp = a;
            condition.PostOp = b;

            ConditionControl.ViewType tempType = condition.Type;
            condition.SetType(target.Type);
            target.SetType(tempType);
            filterChanged();
        }
        private void moveDown(ConditionControl condition)
        {
            int index = FilterStack.Children.IndexOf(condition);

            ConditionControl target = (ConditionControl)FilterStack.Children[index + 2];
            OperationControl a = condition.PrevOp;
            OperationControl b = condition.PostOp;
            OperationControl c = target.PostOp;

            FilterStack.Children.RemoveAt(index + 2);
            FilterStack.Children.RemoveAt(index);

            FilterStack.Children.Insert(index, target);
            FilterStack.Children.Insert(index + 2, condition);

            condition.PrevOp = b;
            condition.PostOp = c;
            target.PrevOp = a;
            target.PostOp = b;

            ConditionControl.ViewType tempType = condition.Type;
            condition.SetType(target.Type);
            target.SetType(tempType);
            filterChanged();
        }
        private Filter getFilter(ref ConditionControl condition, ref int openIndex, ref int closeIndex, ref List<KeyValuePair<OperationControl, int>> unclosedBraces)
        {

            Filter.Group group = null;
            while(true)
            {
                if(condition.PrevOp.NumOpenBraces == openIndex)
                {
                    if(group == null)
                        group = new Filter.Group(condition.Value);
                    else
                        group.Add((Filter.Operation)condition.PrevOp.Op, condition.Value);
                }
                else
                {
                    condition.PrevOp.SetOpenBrace(openIndex, unclosedBraces.Count);
                    unclosedBraces.Add(new KeyValuePair<OperationControl, int>(condition.PrevOp, openIndex));
                    openIndex++;
                    if(group == null)
                        group = new Filter.Group(getFilter(ref condition, ref openIndex, ref closeIndex, ref unclosedBraces));
                    else
                        group.Add((Filter.Operation)condition.PrevOp.Op, getFilter(ref condition, ref openIndex, ref closeIndex, ref unclosedBraces));
                }
                if(condition.PostOp.NumCloseBraces > closeIndex)
                {
                    if(unclosedBraces.Count == 0)
                    {
                        for(int i = closeIndex; i < condition.PostOp.NumCloseBraces; i++)
                            condition.PostOp.SetCloseBrace(condition.PostOp.NumCloseBraces - i - 1, -2);

                        if(condition == lastCondition) return null;

                        while(true)
                        {
                            condition = (ConditionControl)FilterStack.Children[FilterStack.Children.IndexOf(condition) + 2];

                            for(int i = 0; i < condition.PostOp.NumCloseBraces; i++)
                                condition.PostOp.SetCloseBrace(i, -2);

                            if(condition == lastCondition) return null;

                            for(int i = 0; i < condition.PostOp.NumOpenBraces; i++)
                                condition.PostOp.SetOpenBrace(i, -2);
                        }
                    }
                    unclosedBraces.RemoveAt(unclosedBraces.Count - 1);
                    condition.PostOp.SetCloseBrace(condition.PostOp.NumCloseBraces - closeIndex - 1, unclosedBraces.Count);
                    closeIndex++;
                    if(condition.PostOp.NumCloseBraces > unclosedBraces.Count + 1 && lastCondition == condition)
                    {
                        foreach(KeyValuePair<OperationControl, int> brace in unclosedBraces)
                            brace.Key.SetOpenBrace(brace.Value, -2);

                        return null;
                    }
                    return group;
                }
                else
                {
                    if(lastCondition == condition)
                    {
                        if(unclosedBraces.Count > 0)
                        {
                            foreach(KeyValuePair<OperationControl, int> brace in unclosedBraces)
                                brace.Key.SetOpenBrace(brace.Value, -2);
                            return null;
                        }
                        else
                            return group;
                    }
                    else
                        condition = (ConditionControl)FilterStack.Children[FilterStack.Children.IndexOf(condition) + 2];
                    openIndex = 0;
                    closeIndex = 0;
                }
            }
        }


        public Filter Value
        {
            get
            {
                if(numConditions == 0) return null;
                else if(numConditions == 1) return ((ConditionControl)FilterStack.Children[1]).Value;
                else
                {
                    ConditionControl control = (ConditionControl)FilterStack.Children[1];
                    int openIndex = 0, closeIndex = 0;
                    List<KeyValuePair<OperationControl, int>> braces = new List<KeyValuePair<OperationControl, int>>();
                    return getFilter(ref control, ref openIndex, ref closeIndex, ref braces);
                }
            }
            set
            {
                if(numConditions > 0)
                {
                    FilterStack.Children.RemoveRange(1, FilterStack.Children.Count - 2);
                    ((OperationControl)FilterStack.Children[0]).Visibility = Visibility.Collapsed;
                    ((OperationControl)FilterStack.Children[1]).Visibility = Visibility.Collapsed;
                    ((OperationControl)FilterStack.Children[0]).RemoveBraces();
                    ((OperationControl)FilterStack.Children[1]).RemoveBraces();
                }

                if(value != null)
                {
                    int braceLevel = 0;
                    addFilter(value, false, ref braceLevel);
                    Filter temp = Value;
                }
            }
        }
    }
}