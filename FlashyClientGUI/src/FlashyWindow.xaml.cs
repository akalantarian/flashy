﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using Flashy.Client.Editor;

namespace Flashy.Client.GUI
{
    public partial class FlashyWindow : Window
    {
        private readonly Database database;
        private Card currentCard = null;
        private AddWindow addWindow = null;
        private string lastLibrary = null;
        private CardData lastData = null;
        public FlashyWindow(Database database)
        {
            if(database == null) throw new ArgumentNullException("Null database");
            this.database = database;
            this.DataContext = database;

            this.database.PropertyChanged += databaseUpdated;

            InitializeComponent();

            CardFilter.FilterChanged += cardFilterChanged;
            CardFilter.Value = database.Filter;
            SaveFilterButton.IsEnabled = false;

            LibraryChooser.ListChanged += selectedLibrariesChanged;
            LibraryChooser.Libraries = database.Libraries.ToList();
            LibraryChooser.ActiveLibraries = database.ActiveLibraries.ToList();
            SaveLibrariesButton.IsEnabled = false;

            Preview.LibraryBox.IsEnabled = false;
            Preview.ItemChanged += cardPropertiesChanged;
            Preview.IsEnabled = false;
            Preview.Library = null;
            Preview.Data = null;
            SaveCardButton.IsEnabled = false;
        }

        private void databaseUpdated(object sender, PropertyChangedEventArgs args)
        {
            if(args.PropertyName == "Libraries")
                LibraryChooser.Libraries = database.Libraries.ToList();
            else if(args.PropertyName == "Quizzes") throw new Exception("TODO");
        }

        private void cardPropertiesChanged()
        {
            SaveCardButton.IsEnabled = true;
        }
        private void selectedLibrariesChanged()
        {
            SaveLibrariesButton.IsEnabled = true;
        }
        private void cardFilterChanged()
        {
            SaveFilterButton.IsEnabled = true;
        }

        private void cardSelected(object sender, SelectionChangedEventArgs e)
        {
            if(CardList.SelectedItems.Count == 1)
            {
                if(currentCard != (Card)CardList.SelectedItem)
                {
                    currentCard = (Card)CardList.SelectedItem;
                    currentCard.PropertyChanged += selectedCardUpdated;
                    Preview.IsEnabled = true;
                    Preview.Libraries = database.ActiveLibraries.ToList();
                    Preview.Library = currentCard.Library.Name;
                    Preview.Data = currentCard.Data;
                    SaveCardButton.IsEnabled = false;
                    if(lastLibrary == null)
                    {
                        lastLibrary = currentCard.Library.Name;
                        lastData = currentCard.Data;
                    }
                }
            }
            else if(currentCard != null)
            {
                currentCard.PropertyChanged -= selectedCardUpdated;
                currentCard = null;
                Preview.IsEnabled = false;
                Preview.Libraries = null;
                Preview.Library = null;
                Preview.Data = null;
                SaveCardButton.IsEnabled = false;
            }
        }

        private void selectedCardUpdated(object sender, PropertyChangedEventArgs args)
        {
            if(args.PropertyName == "Data" && currentCard != null) Preview.Data = currentCard.Data;
        }

        private void EditorSplitter_Reset(object sender, MouseButtonEventArgs e)
        {
            EditorGrid.ColumnDefinitions[0].Width = new GridLength(0.5, GridUnitType.Star);
            EditorGrid.ColumnDefinitions[2].Width = new GridLength(0.5, GridUnitType.Star);
        }

        private void SaveCardButton_Click(object sender, RoutedEventArgs e)
        {
            currentCard.Save(Preview.Data);
            lastLibrary = Preview.Library;
            lastData = Preview.Data;
            SaveCardButton.IsEnabled = false;
        }

        private void ReloadCardButton_Click(object sender, RoutedEventArgs e)
        {
            Preview.Data = currentCard.Data;
            SaveCardButton.IsEnabled = false;
        }

        private void SaveLibrariesButton_Click(object sender, RoutedEventArgs e)
        {
            foreach(string library in LibraryChooser.ActiveLibraries)
                if(!database.Libraries.ContainsIgnoreCase(library) && MessageBox.Show("Library \"" + library + "\" does not exist. Create?", "", MessageBoxButton.YesNo) != MessageBoxResult.Yes)
                    return;

            database.SetLibraries(LibraryChooser.ActiveLibraries.ToArray());
            if(addWindow != null)
            {
                if(database.ActiveLibraries.Count() == 0) { addWindow.Close(); addWindow = null; }
                else
                    addWindow.Libraries = database.ActiveLibraries.ToList();
            }
            SaveLibrariesButton.IsEnabled = false;
        }

        private void ReloadLibrariesButton_Click(object sender, RoutedEventArgs e)
        {
            LibraryChooser.ActiveLibraries = database.ActiveLibraries.ToList();
            SaveLibrariesButton.IsEnabled = false;
        }

        private void SaveFilterButton_Click(object sender, RoutedEventArgs e)
        {
            database.ApplyFilter(CardFilter.Value);
            SaveFilterButton.IsEnabled = false;
        }

        private void ReloadFilterButton_Click(object sender, RoutedEventArgs e)
        {
            CardFilter.Value = database.Filter;
            SaveFilterButton.IsEnabled = false;
        }

        private void AddFilterButton_Click(object sender, RoutedEventArgs e)
        {
            CardFilter.AddCondition();
        }

        private readonly List<string> singleCardOptions = new List<string>() { "Add", "Delete", "Clone" };
        private readonly List<string> multiCardOptions = new List<string>() { "Delete" };
        private void cardList_ContextMenuOpened(object sender, RoutedEventArgs e)
        {
            ContextMenu menu = (ContextMenu)sender;
            menu.Items.Clear();
            if(CardList.SelectedItems.Count > 1)
            {
                MenuItem item = new MenuItem();
                item.Header = "Delete";
                List<Card> selectedCards = new List<Card>(CardList.SelectedItems.Count);
                foreach(object card in CardList.SelectedItems)
                    selectedCards.Add((Card)card);
                item.Tag = selectedCards;
                item.Click += deleteCards;
                menu.Items.Add(item);
            }
            else if(CardList.SelectedItems.Count == 1)
            {
                MenuItem item = new MenuItem();
                item.Header = "Add";
                item.Click += addCard;
                menu.Items.Add(item);

                item = new MenuItem();
                item.Header = "Delete";
                item.Tag = CardList.SelectedItem;
                item.Click += deleteCards;
                menu.Items.Add(item);

                item = new MenuItem();
                item.Header = "Clone";
                item.Tag = CardList.SelectedItem;
                item.Click += cloneCard;
                menu.Items.Add(item);
            }
        }
        private void addCard(object sender, RoutedEventArgs e)
        {
            if(addWindow != null)
            {
                addWindow.Activate();
            }
            else
            {
                if(lastLibrary == null) addWindow = new AddWindow(database.ActiveLibraries.First(), database.ActiveLibraries.ToList());
                else addWindow = new AddWindow(lastLibrary, database.ActiveLibraries.ToList(), new CardData(lastData.Tags, new Field.RTF(), new Field.RTF(), 0));
                addWindow.Accepted += (library, data) => { lastLibrary = library; lastData = data; database[library].AddCard(data); addWindow = null; };
                addWindow.Closed += (a, b) => addWindow = null;
                addWindow.Show();
            }
        }
        private void deleteCards(object sender, RoutedEventArgs e)
        {
            if(((MenuItem)sender).Tag is Card)
                ((Card)((MenuItem)sender).Tag).Delete();
            else if(((MenuItem)sender).Tag is List<Card>)
                foreach(Card card in (List<Card>)((MenuItem)sender).Tag)
                    card.Delete();
        }
        private void cloneCard(object sender, RoutedEventArgs e)
        {
            Card card = ((MenuItem)sender).Tag as Card;
            if(card == null) return;

            if(addWindow != null)
            {
                addWindow.Library = card.Library.Name;
                addWindow.Data = card.Data;
                addWindow.Activate();
            }
            else
            {
                addWindow = new AddWindow(card.Library.Name, database.ActiveLibraries.ToList(), card.Data);
                addWindow.Accepted += (library, data) => { lastLibrary = library; lastData = data; database[library].AddCard(data); addWindow = null; };
                addWindow.Closed += (a, b) => addWindow = null;
                addWindow.Show();
            }
        }

        private void launchQuiz(object sender, RoutedEventArgs e)
        {

        }
    }
    internal class StatusConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
#if !NO_NETWORK
            if(value is Status)
                return "Flashy - " + value.ToString();
#endif
            return "Flashy";
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    internal class MaxHeightConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if(value == null) return null;
            return (((double)value) - 5) * 3 / 4;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    internal class ToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if(value != null)
                if(value is DateTime && parameter is string)
                    return ((DateTime)value).ToString((string)parameter);
                else
                    return value.ToString();
            return null;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
