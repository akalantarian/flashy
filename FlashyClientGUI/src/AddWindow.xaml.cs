﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Flashy.Client.GUI
{
    internal partial class AddWindow : Window
    {
        public event Action<string, CardData> Accepted;

        public IList<string> Libraries { get { return Preview.Libraries; } set { Preview.Libraries = value; } }
        public string Library { get { return Preview.Library; } set { Preview.Library = value; } }
        public CardData Data { get { return Preview.Data; } set { Preview.Data = value; } }
        public TagCollection Tags { get { return Preview.Tags; } set { Preview.Tags = value; } }
        public Field Question { get { return Preview.Question; } set { Preview.Question = value; } }
        public Field Answer { get { return Preview.Answer; } set { Preview.Answer = value; } }
        public byte Bin { get { return Preview.Bin; } set { Preview.Bin = value; } }

        public AddWindow(string library, IList<string> libraries, CardData data = null)
        {
            InitializeComponent();
            Preview.Libraries = libraries;
            Preview.Library = library;
            Preview.Data = data;
            Loaded += (a, b) => Preview.QuestionControl.Focus();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Escape)
                Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Action<string, CardData> del = Accepted;
            if(del != null)
                del(Preview.Library, Preview.Data);
            Close();
        }
    }
}
