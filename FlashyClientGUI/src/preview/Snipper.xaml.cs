﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using System.Drawing.Imaging;

namespace Flashy.Client.GUI.PreviewControls
{
    using Graphics = System.Drawing.Graphics;
    using Int32Rect = System.Drawing.Rectangle;
    using PointInt32 = System.Drawing.Point;
    using Screen = System.Windows.Forms.Screen;
    using Bitmap = System.Drawing.Bitmap;
    using Img = System.Drawing.Image;
    using Cursor = System.Windows.Forms.Cursor;
    using PixelParameters = System.Windows.Forms.SystemInformation;

    internal struct DoubleRect
    {
        private readonly double x, y, width, height;
        public double X { get { return x; } }
        public double Y { get { return y; } }
        public double Width { get { return width; } }
        public double Height { get { return height; } }
        public DoubleRect(double x, double y, double width, double height) { this.x = x; this.y = y; this.width = width; this.height = height; }
        public override string ToString() { return "{x=" + x + ",y=" + y + ",width=" + width + ",height=" + height + "}"; }
    }

    public class Snipper
    {
        public static void Snip(Action<Img, object> callback, object tag)
        {
            Task.Factory.StartNew(snip, new KeyValuePair<Action<Img, object>, object>(callback, tag), TaskCreationOptions.LongRunning);
        }
        public static void Snip(Action<Img> callback)
        {
            Task.Factory.StartNew(snip, callback, TaskCreationOptions.LongRunning);
        }
        private static void snip(object obj)
        {
            if(obj is Action<Img, object>)
            {

                Action<Img> callback = (Action<Img>)obj;
                Dispatcher dispatcher = Application.Current.Dispatcher;
                Snipper snipper = null;
                dispatcher.Invoke((Action)(() => { snipper = new Snipper(); }));
                snipper.wait();
                callback(snipper.result);
            }
            else
            {
                KeyValuePair<Action<Img, object>, object> pair = (KeyValuePair<Action<Img, object>, object>)obj;
                Dispatcher dispatcher = Application.Current.Dispatcher;
                Snipper snipper = null;
                dispatcher.Invoke((Action)(() => { snipper = new Snipper(); }));
                snipper.wait();
                pair.Key(snipper.result, pair.Value);
            }
        }
        private static readonly float dpiX, dpiY;
        static Snipper()
        {
            using(var graphics = Graphics.FromHwnd(IntPtr.Zero))
            {
                dpiX = graphics.DpiX;
                dpiY = graphics.DpiY;
            }
        }
        public static int wpfToPixelsX(double size) { return (int)Math.Round(size * dpiX / 96.0); }
        public static int wpfToPixelsY(double size) { return (int)Math.Round(size * dpiY / 96.0); }
        public static double pixelsToWPFX(int pixels) { return pixels * 96.0 / dpiX; }
        public static double pixelsToWPFY(int pixels) { return pixels * 96.0 / dpiY; }

        private Img result = null;
        private readonly ManualResetEvent evt = new ManualResetEvent(false);
        private readonly List<SnipperWindow> snippers = new List<SnipperWindow>();
        private readonly Int32Rect virtualScreen;
        private readonly Bitmap screenShot;

        private PointInt32? pixelPoint = null;

        private Snipper()
        {
            virtualScreen = PixelParameters.VirtualScreen;
            screenShot = new Bitmap(virtualScreen.Width - virtualScreen.X, virtualScreen.Height - virtualScreen.Y);
            Graphics.FromImage(screenShot as Img).CopyFromScreen(virtualScreen.X, virtualScreen.Y, 0, 0, screenShot.Size);

            foreach(Screen screen in Screen.AllScreens)
            {
                Int32Rect rect = new Int32Rect(screen.Bounds.X - virtualScreen.X, screen.Bounds.Y - virtualScreen.Y, screen.Bounds.Width, screen.Bounds.Height);
                SnipperWindow snipper = new SnipperWindow(screen, screenShot.Clone(rect, screenShot.PixelFormat));
                snipper.Start += start;
                snipper.Stop += stop;
                snipper.Move += move;
                snipper.Quit += quit;
                snippers.Add(snipper);
                snipper.Show();
            }
        }

        private void wait() { evt.WaitOne(); }
        private void start()
        {
            if(pixelPoint != null) return;
            pixelPoint = Cursor.Position;
        }
        private void move()
        {
            if(pixelPoint == null) return;
            PointInt32 curPoint = Cursor.Position;

            Int32Rect pixelRect = new Int32Rect(
                Math.Min(curPoint.X, pixelPoint.Value.X),
                Math.Min(curPoint.Y, pixelPoint.Value.Y),
                Math.Abs(curPoint.X - pixelPoint.Value.X),
                Math.Abs(curPoint.Y - pixelPoint.Value.Y));
            DoubleRect wpfRect = new DoubleRect(
                Math.Min(pixelsToWPFX(curPoint.X), pixelsToWPFX(pixelPoint.Value.X)),
                Math.Min(pixelsToWPFY(curPoint.Y), pixelsToWPFY(pixelPoint.Value.Y)),
                Math.Abs(pixelsToWPFX(curPoint.X) - pixelsToWPFX(pixelPoint.Value.X)),
                Math.Abs(pixelsToWPFY(curPoint.Y) - pixelsToWPFY(pixelPoint.Value.Y)));

            if(pixelRect.Width > 0 && pixelRect.Height > 0)
                foreach(SnipperWindow window in snippers)
                    window.Apply(pixelRect);
            else
                foreach(SnipperWindow window in snippers)
                    window.Clear();
        }
        private void stop()
        {
            if(pixelPoint == null) return;
            PointInt32 curPoint = Cursor.Position;

            Int32Rect pixelRect = new Int32Rect(
                Math.Min(curPoint.X, pixelPoint.Value.X) - virtualScreen.X,
                Math.Min(curPoint.Y, pixelPoint.Value.Y) - virtualScreen.Y,
                Math.Max(Math.Abs(curPoint.X - pixelPoint.Value.X), 1),
                Math.Max(Math.Abs(curPoint.Y - pixelPoint.Value.Y), 1));
            
            result = screenShot.Clone(pixelRect, screenShot.PixelFormat);

            foreach(SnipperWindow window in snippers)
                window.Close();

            evt.Set();
        }
        private void quit()
        {
            if(pixelPoint == null)
            {
                result = null;
                foreach(SnipperWindow window in snippers)
                    window.Close();
                evt.Set();
            }
            else
            {
                pixelPoint = null;
                foreach(SnipperWindow window in snippers)
                    window.Clear();
            }
        }
    }

    internal partial class SnipperWindow : Window
    {
        public event Action Start, Stop, Move, Quit;

        private void Canvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Action del = Start; if(del != null) del();
        }
        private void Canvas_MouseMove(object sender, MouseEventArgs e)
        {
            Action del = Move; if(del != null) del();
        }
        private void Canvas_MouseUp(object sender, MouseButtonEventArgs e)
        {
            Action del = Stop; if(del != null) del();
        }
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            Action del = Quit; if(del != null) del();
        }

        private readonly Screen screen;
        private readonly Bitmap bitmap;

        public Screen Screen { get { return screen; } }

        public SnipperWindow(Screen screen, Bitmap bitmap)
        {
            this.screen = screen;
            this.bitmap = bitmap;
            InitializeComponent();
            Left = Snipper.pixelsToWPFX(screen.Bounds.X);
            Top = Snipper.pixelsToWPFX(screen.Bounds.Y);
            this.Loaded += (s, e) =>
            {
                using(MemoryStream stream = new MemoryStream())
                {
                    bitmap.Save(stream, ImageFormat.Bmp);
                    stream.Position = 0;
                    BitmapImage source = new BitmapImage();
                    source.BeginInit();
                    source.StreamSource = stream;
                    source.CacheOption = BitmapCacheOption.OnLoad;
                    source.EndInit();
                    Image.Source = source;
                }
                WindowState = WindowState.Maximized;
            };
        }

        public void Apply(Int32Rect rect)
        {
            Int32Rect croppedRect = Int32Rect.Intersect(screen.Bounds, rect);
            if(croppedRect.Width == 0 || croppedRect.Height == 0) { Clear(); return; }

            croppedRect = new Int32Rect(croppedRect.X - screen.Bounds.X, croppedRect.Y - screen.Bounds.Y, croppedRect.Width, croppedRect.Height);
            
            DoubleRect wpfRect = new DoubleRect(
                Snipper.pixelsToWPFX(croppedRect.X),
                Snipper.pixelsToWPFY(croppedRect.Y),
                Snipper.pixelsToWPFX(croppedRect.Width),
                Snipper.pixelsToWPFY(croppedRect.Height));

            Canvas.SetLeft(Box, wpfRect.X);
            Canvas.SetTop(Box, wpfRect.Y);
            Box.Width = wpfRect.Width;
            Box.Height = wpfRect.Height;
            Box.BorderThickness = new Thickness(
                rect.Left < screen.Bounds.Left ? 0 : 2,
                rect.Top < screen.Bounds.Top ? 0 : 2,
                rect.Right > screen.Bounds.Right ? 0 : 2,
                rect.Bottom > screen.Bounds.Bottom ? 0 : 2);

            Canvas.SetLeft(ColoredImage, wpfRect.X);
            Canvas.SetTop(ColoredImage, wpfRect.Y);
            ColoredImage.Width = wpfRect.Width;
            ColoredImage.Height = wpfRect.Height;
            using(MemoryStream stream = new MemoryStream())
            {
                Bitmap target = bitmap.Clone(croppedRect, bitmap.PixelFormat);
                target.Save(stream, ImageFormat.Bmp);
                stream.Position = 0;
                BitmapImage source = new BitmapImage();
                source.BeginInit();
                source.StreamSource = stream;
                source.CacheOption = BitmapCacheOption.OnLoad;
                source.EndInit();
                ColoredImage.Source = source;
            }

            Box.Visibility = Visibility.Visible;
            ColoredImage.Visibility = Visibility.Visible;
        }
        public void Clear()
        {
            Box.Visibility = Visibility.Hidden;
            ColoredImage.Visibility = Visibility.Hidden;
        }
    }
}
