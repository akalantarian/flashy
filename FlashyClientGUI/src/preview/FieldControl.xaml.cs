﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Runtime.InteropServices;
using System.Drawing.Imaging;
using Microsoft.Win32;

namespace Flashy.Client.GUI.PreviewControls
{
    internal partial class FieldControl : UserControl
    {
        private static readonly System.Drawing.Image emptyImage;
        static FieldControl()
        {
            System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(1, 1);
            bitmap.SetPixel(0, 0, System.Drawing.Color.Transparent);
            emptyImage = bitmap;
        }

        public event Action ItemChanged;
        private void onItemChanged() { Action del = ItemChanged; if(del != null) del(); }

        public FieldControl()
        {
            InitializeComponent();
        }

        public Field Value
        {
            get
            {
                if(Document.Visibility == Visibility.Visible)
                    return new Field.RTF(getDocument());
                else
                    return new Field.Image(getImage(ImageFormat.Png));
            }
            set
            {
                this.invoke(() =>
                {
                    if(value == null)
                    {
                        setDocumentState();
                        clearDocument();
                        clearImage();
                    }
                    else if(value is Field.RTF)
                    {
                        setDocumentState();
                        setDocument(value.Data);
                        clearImage();
                    }
                    else
                    {
                        setImageState();
                        clearDocument();
                        setImage(value.Data);
                    }
                });
            }
        }
        public new void Focus()
        {
            if(Document.Visibility == Visibility.Visible)
                Document.Focus();
            else
                Image.Focus();
        }
        private byte[] getDocumentText()
        {
            using(System.Windows.Forms.RichTextBox rtb = new System.Windows.Forms.RichTextBox())
            {
                rtb.Rtf = Encoding.ASCII.GetString(getDocument());
                return Encoding.Unicode.GetBytes(rtb.Text);
            }
        }
        private byte[] getDocument()
        {
            MemoryStream stream = new MemoryStream();
            new TextRange(Document.Document.ContentStart, Document.Document.ContentEnd).Save(stream, DataFormats.Rtf);
            stream.Close();
            return stream.ToArray();
        }
        private void setDocument(byte[] data) { using(MemoryStream stream = new MemoryStream(data)) setDocument(stream); }
        private void setDocument(FileInfo file)
        {
            if(file.Extension.ToLower() == ".txt")
            {
                using(System.Windows.Forms.RichTextBox rtb = new System.Windows.Forms.RichTextBox())
                {
                    rtb.Font = new System.Drawing.Font("Calibri", 11);
                    rtb.Text = File.ReadAllText(file.FullName);
                    using(MemoryStream stream = new MemoryStream(Encoding.ASCII.GetBytes(rtb.Rtf)))
                        setDocument(stream);
                }
            }
            else
                using(FileStream stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.Read))
                    setDocument(stream);
        }
        private void setDocument(Stream stream)
        {
            new TextRange(Document.Document.ContentStart, Document.Document.ContentEnd).Load(stream, DataFormats.Rtf);
        }
        private void clearDocument()
        {
            Document.Document.Blocks.Clear();
            Document.FontFamily = new FontFamily("Calibri");
            Document.FontSize = 14.667;
        }

        private byte[] getImage(ImageFormat format)
        {
            using(MemoryStream stream = new MemoryStream((byte[])Image.Tag))
            {
                System.Drawing.Image image = System.Drawing.Image.FromStream(stream);
                MemoryStream stream2 = new MemoryStream();
                image.Save(stream2, format);
                stream2.Close();
                return stream2.ToArray();
            }
        }
        private void setImage(byte[] data)
        {
            System.Drawing.Image image;
            using(MemoryStream stream = new MemoryStream(data))
            {
                image = System.Drawing.Image.FromStream(stream);
                setImage(image);
            }
        }
        private void setImage(FileInfo file)
        {
            setImage(System.Drawing.Image.FromFile(file.FullName));
        }
        private void setImage(System.Drawing.Image image)
        {
            MemoryStream stream = new MemoryStream();
            image.Save(stream, ImageFormat.Bmp);
            stream.Position = 0;
            BitmapImage source = new BitmapImage();
            source.BeginInit();
            source.StreamSource = stream;
            source.CacheOption = BitmapCacheOption.OnLoad;
            source.EndInit();
            Image.Source = source;
            stream.Close();
            Image.Tag = stream.ToArray();
        }
        private void clearImage()
        {
            setImage(emptyImage);
        }

        private bool setDocumentState()
        {
            if(Document.Visibility == Visibility.Visible) return false;
            ImageHolder.Visibility = Visibility.Collapsed;
            Document.Visibility = Visibility.Visible;
            return true;
        }
        private bool setImageState() { return setImageState(Image.Stretch); }
        private bool setImageState(Stretch stretch)
        {
            bool itemChanged = false;
            if(ImageHolder.Visibility == Visibility.Visible)
            {
                if(Image.Stretch == stretch) return false;
            }
            else
            {
                Document.Visibility = Visibility.Collapsed;
                ImageHolder.Visibility = Visibility.Visible;
                itemChanged = true;
            }
            Image.Stretch = stretch;
            ((Image)((MenuItem)((MenuItem)ImageHolder.ContextMenu.Items[1]).Items[0]).Icon).Visibility = Visibility.Hidden;
            ((Image)((MenuItem)((MenuItem)ImageHolder.ContextMenu.Items[1]).Items[1]).Icon).Visibility = Visibility.Hidden;
            ((Image)((MenuItem)((MenuItem)ImageHolder.ContextMenu.Items[1]).Items[2]).Icon).Visibility = Visibility.Hidden;
            ((Image)((MenuItem)((MenuItem)ImageHolder.ContextMenu.Items[1]).Items[3]).Icon).Visibility = Visibility.Hidden;
            switch(stretch)
            {
                case Stretch.Fill:
                    ((Image)((MenuItem)((MenuItem)ImageHolder.ContextMenu.Items[1]).Items[1]).Icon).Visibility = Visibility.Visible;
                    break;
                case Stretch.None:
                    ((Image)((MenuItem)((MenuItem)ImageHolder.ContextMenu.Items[1]).Items[3]).Icon).Visibility = Visibility.Visible;
                    break;
                case Stretch.Uniform:
                    ((Image)((MenuItem)((MenuItem)ImageHolder.ContextMenu.Items[1]).Items[0]).Icon).Visibility = Visibility.Visible;
                    break;
                default:
                    ((Image)((MenuItem)((MenuItem)ImageHolder.ContextMenu.Items[1]).Items[2]).Icon).Visibility = Visibility.Visible;
                    break;
            }
            return itemChanged;
        }

        private void contentDrop(object sender, DragEventArgs e)
        {
            try
            {
                FileInfo file = new FileInfo(((string[])e.Data.GetData(DataFormats.FileDrop))[0]);
                switch(file.Extension.ToLower())
                {
                    case ".txt":
                    case ".rtf":
                        setDocumentState();
                        setDocument(file);
                        Document.Focus();
                        break;
                    default:
                        setImageState();
                        setImage(file);
                        Image.Focus();
                        break;
                }
                onItemChanged();
            }
            catch(Exception) { }
        }

        private void document_TextChanged(object sender, TextChangedEventArgs e)
        {
            onItemChanged();
        }

        private void cancelDragOver(object sender, DragEventArgs e) { e.Handled = true; }

        private void document(object sender, RoutedEventArgs e)
        {
            if(setDocumentState())
                onItemChanged();
            Document.Focus();
        }
        private void imageNone(object sender, RoutedEventArgs e)
        {
            if(setImageState(Stretch.None))
                onItemChanged();
            Image.Focus();
        }
        private void imageUniform(object sender, RoutedEventArgs e)
        {
            if(setImageState(Stretch.Uniform))
                onItemChanged();
            Image.Focus();
        }
        private void imageStretch(object sender, RoutedEventArgs e)
        {
            if(setImageState(Stretch.Fill))
                onItemChanged();
            Image.Focus();
        }
        private void imageCrop(object sender, RoutedEventArgs e)
        {
            if(setImageState(Stretch.UniformToFill))
                onItemChanged();
            Image.Focus();
        }

        private bool isMiddlePressed(MouseButtonEventArgs e)
        {
            return e.MiddleButton == MouseButtonState.Pressed &&
                e.LeftButton == MouseButtonState.Released &&
                e.RightButton == MouseButtonState.Released &&
                e.XButton1 == MouseButtonState.Released &&
                e.XButton2 == MouseButtonState.Released;
        }


        private void document_MouseDown(object sender, MouseButtonEventArgs e) { if(isMiddlePressed(e) || (e.ChangedButton == MouseButton.Left && e.ClickCount == 2)) editDocument(); }
        private void image_MouseDown(object sender, MouseButtonEventArgs e) { if(isMiddlePressed(e) || (e.ChangedButton == MouseButton.Left && e.ClickCount == 2)) editImage(); }

        private void editDocument()
        {
            byte[] newData = edit(new FileInfo(System.IO.Path.GetTempPath() + @"\" + new Random().Next() + @".rtf"), getDocument());
            if(newData != null)
            {
                setDocument(newData);
                onItemChanged();
                Document.Focus();
            }
        }
        private void editImage()
        {
            byte[] newData = edit(new FileInfo(System.IO.Path.GetTempPath() + @"\" + new Random().Next() + @".bmp"), getImage(ImageFormat.Bmp));
            if(newData != null)
            {
                setImage(newData);
                onItemChanged();
                Image.Focus();
            }
        }

        [DllImport("msvcrt.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern int memcmp(byte[] b1, byte[] b2, long count);

        private byte[] edit(FileInfo file, byte[] data)
        {
            try
            {
                File.WriteAllBytes(file.FullName, data);
                //tempfile.Attributes = tempfile.Attributes | FileAttributes.Hidden;
                file.Refresh();
                DateTime time = file.LastWriteTime;
                new EmptyWindow(file).ShowDialog();
                file.Refresh();
                if(file.LastWriteTime != time)
                {
                    byte[] newData = File.ReadAllBytes(file.FullName);

                    if(data.Length != newData.Length || memcmp(data, newData, data.Length) != 0)
                        return newData;
                }
            }
            finally
            {
                try
                {
                    file.Delete();
                }
                catch(Exception) { }
            }
            return null;
        }
        private void openFromFile(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();

            dlg.FilterIndex = Document.Visibility == Visibility.Visible ? 0 : 3;

            dlg.Filter = "Document files (*.rtf)|*.rtf|Text files (*.txt)|*.txt|Image files (*.png, *.tif, *.bmp, *.jpg, *.gif)|*.png;*.tif;*.bmp;*.jpg;*.gif|All files (*.*)|*.*";
            bool? result = dlg.ShowDialog();

            if(result == true)
            {
                FileInfo file = new FileInfo(dlg.FileName);
                try
                {
                    switch(file.Extension.ToLower())
                    {
                        case ".txt":
                        case ".rtf":
                            setDocumentState();
                            setDocument(file);
                            Document.Focus();
                            break;
                        default:
                            setImageState();
                            setImage(file);
                            Image.Focus();
                            break;
                    }
                    onItemChanged();
                }
                catch(Exception) { }
            }
        }
        private void saveToFile(object sender, RoutedEventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            if(Document.Visibility == Visibility.Visible)
            {
                dlg.FileName = "Document";
                dlg.DefaultExt = ".rtf";
                dlg.Filter = "Document files (*.rtf)|*.rtf|Text files (*.txt)|*.txt|All files (*.*)|*.*";
            }
            else
            {
                dlg.FileName = "Image";
                dlg.DefaultExt = ".png";
                dlg.Filter = "Image files (*.png, *.tif, *.bmp, *.jpg, *.gif)|*.png;*.tif;*.bmp;*.jpg;*.gif|All files (*.*)|*.*";
            }

            bool? result = dlg.ShowDialog();
            if(result == true)
            {
                FileInfo file = new FileInfo(dlg.FileName);
                if(Document.Visibility == Visibility.Visible)
                    switch(file.Extension.ToLower())
                    {
                        case ".txt":
                            File.WriteAllBytes(file.FullName, getDocumentText());
                            break;
                        default:
                            File.WriteAllBytes(file.FullName, getDocument());
                            break;
                    }
                else
                    switch(file.Extension.ToLower())
                    {
                        case ".bmp":
                            File.WriteAllBytes(file.FullName, getImage(ImageFormat.Bmp));
                            break;
                        case ".jpg":
                            File.WriteAllBytes(file.FullName, getImage(ImageFormat.Jpeg));
                            break;
                        case ".gif":
                            File.WriteAllBytes(file.FullName, getImage(ImageFormat.Gif));
                            break;
                        case ".tif":
                            File.WriteAllBytes(file.FullName, getImage(ImageFormat.Tiff));
                            break;
                        default:
                            File.WriteAllBytes(file.FullName, getImage(ImageFormat.Png));
                            break;
                    }
            }
        }

        private void snip(object sender, RoutedEventArgs e)
        {
            List<Window> windows = new List<Window>();
            foreach(Window window in Application.Current.Windows)
                if(window.IsVisible)
                {
                    windows.Add(window);
                    window.Hide();
                }

            Task.Factory.StartNew(() =>
            {
                Thread.Sleep(200);
                Snipper.Snip(snipComplete, windows);
            }, TaskCreationOptions.LongRunning);
        }
        private void snipComplete(System.Drawing.Image image, object tag)
        {
            if(image == null)
                this.invoke(() =>
                {
                    foreach(Window window in (IEnumerable<Window>)tag)
                        window.Show();
                });
            else
                this.invoke(() =>
                {
                    foreach(Window window in (IEnumerable<Window>)tag)
                        window.Show();
                    setImageState();
                    setImage(image);
                    onItemChanged();
                });
        }

        private void clear(object sender, RoutedEventArgs e)
        {
            Value = null;
            onItemChanged();
        }
    }
}
