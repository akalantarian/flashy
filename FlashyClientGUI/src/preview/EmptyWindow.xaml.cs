﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;

namespace Flashy.Client.GUI.PreviewControls
{
    internal partial class EmptyWindow : Window
    {
        public EmptyWindow(FileInfo file)
        {
            if(file == null) throw new ArgumentNullException("Null file");
            InitializeComponent();
            this.Loaded += (sender, e) =>
            {
                Task.Factory.StartNew(() =>
                {
                    try
                    {
                        if(file.Extension == ".rtf")
                                Process.Start("wordpad", file.FullName).WaitForExit();
                        else if(file.Extension == ".bmp")
                                Process.Start("mspaint", file.FullName).WaitForExit();
                        else
                            throw new Exception("Unknown field format");

                        this.invoke(() => this.Close());
                    }
                    catch(Exception exc)
                    {
                        MessageBox.Show("failed to edit: " + exc.Message);
                        try
                        {
                            this.invoke(() => this.Close());
                        }
                        catch(Exception) { }
                    }
                }, TaskCreationOptions.LongRunning);
            };
        }
    }
}
