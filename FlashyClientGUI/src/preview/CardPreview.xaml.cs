﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Runtime.InteropServices;
using System.Drawing.Imaging;

namespace Flashy.Client.GUI.PreviewControls
{
    internal partial class CardPreview : UserControl
    {
        #region Events
        public event Action ItemChanged;
        private void onItemChanged() { Action del = ItemChanged; if(del != null) del(); }
        #endregion

        #region Variables
        private byte bin;
        private List<string> libraries;
        #endregion

        #region Accessors
        public IList<string> Libraries
        {
            get { return Libraries; }
            set
            {
                string selectedLib = Library;
                libraries.Clear();
                if(value != null)
                    foreach(string library in value)
                        libraries.Add(library);
                if(selectedLib != null && libraries.ContainsIgnoreCase(selectedLib))
                    Library = selectedLib;
                else
                    Library = null;
            }
        }
        public string Library
        {
            get { return LibraryBox.SelectedIndex != -1 ? libraries[LibraryBox.SelectedIndex] : null; }
            set
            {
                if(value == null) LibraryBox.SelectedIndex = -1;
                else
                {
                    foreach(string library in libraries)
                        if(library.ToLower() == value.ToLower())
                        {
                            LibraryBox.SelectedItem = library;
                            return;
                        }
                }
            }
        }
        public CardData Data
        {
            get { return new CardData(Tags, Question, Answer, bin); }
            set
            {
                if(value == null) { Tags = null; Question = null; Answer = null; }
                else { Tags = value.Tags; Question = value.Question; Answer = value.Answer; bin = value.Bin; }
            }
        }
        public TagCollection Tags
        {
            get
            {
                string text = TagBox.Text;
                if(!TagCollection.IsValidTagString(text))
                    return new TagCollection();
                return new TagCollection(text);
            }
            set
            {
                if(value == null)
                    TagBox.Text = "";
                else
                    TagBox.Text = value.ToString();
                checkTagFilterText();
            }
        }
        public Field Question { get { return QuestionControl.Value; } set { QuestionControl.Value = value; } }
        public Field Answer { get { return AnswerControl.Value; } set { AnswerControl.Value = value; } }
        public byte Bin { get { return bin; } set { bin = value; } }
        #endregion

        #region Constructor
        public CardPreview()
        {
            InitializeComponent();
            LibraryBox.ItemsSource = libraries = new List<string>();
            QuestionControl.ItemChanged += onItemChanged;
            AnswerControl.ItemChanged += onItemChanged;
        }
        #endregion

        #region Functions
        private void checkTagFilterText()
        {
            string tagString = TagBox.Text;
            if(string.IsNullOrEmpty(tagString)) return;
            if(TagCollection.IsValidTagString(tagString))
                TagBox.Foreground = new SolidColorBrush(Colors.Green);
            else
                TagBox.Foreground = new SolidColorBrush(Colors.Red);
        }
        private bool isMiddlePressed(MouseButtonEventArgs e)
        {
            return e.MiddleButton == MouseButtonState.Pressed &&
                e.LeftButton == MouseButtonState.Released &&
                e.RightButton == MouseButtonState.Released &&
                e.XButton1 == MouseButtonState.Released &&
                e.XButton2 == MouseButtonState.Released;
        }
        
        private void GridSplitter_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            EditorGrid.RowDefinitions[2].Height = new GridLength(0.5, GridUnitType.Star);
            EditorGrid.RowDefinitions[4].Height = new GridLength(0.5, GridUnitType.Star);
        }

        private void TagBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            checkTagFilterText();
            onItemChanged();
        }

        private void LibraryBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            onItemChanged();
        }
        #endregion
    }
}
