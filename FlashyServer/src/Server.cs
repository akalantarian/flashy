﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;

namespace Flashy.Server
{
    public static class Program
    {
        [DllImport("user32.dll")]
        public static extern IntPtr SetWindowPos(IntPtr hWnd, int hWndInsertAfter, int x, int Y, int cx, int cy, int wFlags);

        [DllImport("kernel32.dll")]
        public static extern IntPtr GetConsoleWindow();

        [STAThread]
        public static void Main(string[] args)
        {
#if DEBUG
            Logger.Init(new FileInfo(@"C:\Users\Aik\Documents\FlashyDir\Server.log"), new FileInfo(@"C:\Users\Aik\Documents\ServerException.log"), true);
#else
            Logger.Init(new FileInfo(@"C:\Users\Aik\Documents\FlashyNetworkTest\Log.log"), new FileInfo(@"C:\Users\Aik\Documents\Exception.log"), true);
#endif
            #region Console
            Console.Title = "Server";
            Console.BackgroundColor = ConsoleColor.Gray;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.CursorVisible = false;
            Console.BufferWidth = Console.WindowWidth = Console.LargestWindowWidth;
            Console.BufferHeight = Console.WindowHeight = Console.LargestWindowHeight / 2;
            SetWindowPos(GetConsoleWindow(), 0,
                0,
                System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Height / 2,
                System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Width,
                System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Height / 2, 0);
            Console.Clear();
            #endregion

#if DEBUG
            EventWaitHandle evt = new EventWaitHandle(false, EventResetMode.ManualReset, @"FLASHY");
            Database x = new Database(new DirectoryInfo(@"C:\Users\Aik\Documents\FlashyDir\Server"));
            evt.Set();
#else
            Database x = new Database(new DirectoryInfo(@"C:\Users\Aik\Documents\FlashyNetworkTest"));
#endif
            Thread.Sleep(int.MaxValue);
        }
    }
}
