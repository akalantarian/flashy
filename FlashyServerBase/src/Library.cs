﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Sockets;

using Flashy.Network;

namespace Flashy.Server
{
    public sealed class Library
    {
        #region Static
        private static readonly Regex nameChecker = new Regex(@"^[\w\d_-]+$", RegexOptions.Compiled);
        private static readonly Regex fileNameParser = new Regex(@"^(\d+)-(\d+)\.card$", RegexOptions.Compiled);
        #endregion

        #region Variables
        private readonly Database database;
        private readonly DirectoryInfo directory;
        private readonly string name;
        private readonly MessageQueue<FMIMessage> messageQueue;

        private readonly List<Card> cards;
        private readonly LinkedList<Card> history;

        private TCPConnection connection;
        #endregion

        #region Accessors
        public Database Database { get { return database; } }
        public DirectoryInfo Directory { get { return directory; } }
        public string Name { get { return name; } }

        public Card this[uint id] { get { lock (messageQueue.LockObject) if((int)id >= cards.Count) return null; return cards[(int)id]; } }
        public uint NextID { get { lock (messageQueue.LockObject) return (uint)cards.Count; } }
        public DateTime LastEdit { get { lock (messageQueue.LockObject) return history.First != null ? history.First.Value.LastEdit : DateTime.MinValue; } }

        public bool Connected { get { lock (messageQueue.LockObject) return connection != null && connection.Connected; } }
        #endregion

        #region Constructor
        internal Library(Database database, string name)
        {
            this.database = database;
            this.directory = new DirectoryInfo(database.Directory + @"\" + name);
            this.name = name;
            this.messageQueue = new MessageQueue<FMIMessage>();
            this.messageQueue.Process += process;

            this.cards = new List<Card>();
            this.history = new LinkedList<Card>();

            this.connection = null;

            if (this.directory.Exists)
            {
                foreach (FileInfo file in this.directory.GetFiles())
                {
                    Match match = fileNameParser.Match(file.Name);
                    if (!match.Success) throw new Exception("Invalid card file: " + file.FullName);
                    uint cardID = uint.Parse(match.Groups[1].Value);
                    DateTime cardLastEdit = new DateTime(long.Parse(match.Groups[2].Value));

                    Card card = new Card(this, cardID, cardLastEdit);
                    while (this.cards.Count < (int)cardID)
                        this.cards.Add(null);

                    if (this.cards.Count == (int)cardID)
                        this.cards.Add(card);
                    else if (this.cards[(int)cardID] == null)
                        this.cards[(int)cardID] = card;
                    else
                        throw new InvalidDataException("Corrupted library - duplicate cards: " + this.name);

                    LinkedListNode<Card> curr = this.history.First;
                    while (curr != null && cardLastEdit < curr.Value.LastEdit)
                        curr = curr.Next;

                    if (curr == null) this.history.AddLast(card.HistoryNode);
                    else if (cardLastEdit == curr.Value.LastEdit) throw new InvalidDataException("Corrupted library - duplicate sync times: " + this.name);
                    else this.history.AddBefore(curr, card.HistoryNode);
                }
                foreach (Card card in this.cards)
                    if (card == null) throw new InvalidDataException("Corrupted library - missing cards: " + this.name);
            }
            else
                this.directory.Create();

            this.messageQueue.Start();
        }
        #endregion

        #region Functions
        internal void enqueue(FMIMessage message)
        {
            messageQueue.Enqueue(message);
        }

        private void networkDisconnect(TCPConnection receiveConnection, Exception e)
        {
            messageQueue.Enqueue(new FMIMessage.Disconnect(receiveConnection));
        }
        private void networkMessage(TCPConnection receiveConnection, byte[] data)
        {
            if (connection != receiveConnection) return;

            NetworkMessage message = NetworkMessage.Decode(data);
            if (message is NetworkMessage.PingRequest)
                receiveConnection.Send(NetworkMessage.Encode(NetworkMessage.PingResponse.Instance));
            else if (message is NetworkMessage.SyncCardsRequest)
                messageQueue.Enqueue(new FMIMessage.SyncCardsRequest(receiveConnection, (NetworkMessage.SyncCardsRequest)message));
            else
                throw new Exception("Unknown network message received " + message.GetType());
        }

        private void process(FMIMessage message)
        {
            Logger.Log(string.Format("{0,-18}", name) + "Process " + message);
            if (message is FMIMessage.SyncLibraryRequest)
                syncProc((FMIMessage.SyncLibraryRequest)message);
            if (message is FMIMessage.SyncCardsRequest)
                saveProc((FMIMessage.SyncCardsRequest)message);
        }

        private void syncProc(FMIMessage.SyncLibraryRequest message)
        {
            if (connection != null)
                disconnectProc();

            message.Connection.OnDisconnect += networkDisconnect;
            if (!message.Connection.Connected) return;

            connection = message.Connection;
            connection.OnReceive += networkMessage;

            List<CardIDLastEditData> cardListResponse = new List<CardIDLastEditData>();
            LinkedListNode<Card> curr = history.First;
            while (curr != null && curr.Value.LastEdit > message.LastEdit)
            {
                cardListResponse.Add(new CardIDLastEditData(curr.Value.ID, curr.Value.LastEdit, curr.Value.Data));
                curr = curr.Next;
            }
            cardListResponse.Reverse();
            connection.Send(NetworkMessage.Encode(new NetworkMessage.SyncLibraryResponse(cardListResponse.AsReadOnly())));
        }

        private void saveProc(FMIMessage.SyncCardsRequest message)
        {
            if (connection != message.Connection) return;

            DateTime editTime = DateTime.UtcNow;
            if (LastEdit >= editTime) editTime = LastEdit.AddTicks(1);

            DateTime currEditTime = editTime;
            foreach (CardIDData clientData in message.Cards)
            {
                while (clientData.ID > cards.Count)
                    cards.Add(null);

                Card card;
                if (clientData.ID == cards.Count)
                    cards.Add(card = new Card(this, clientData.ID, currEditTime, clientData.Data));
                else
                    if ((card = cards[(int)clientData.ID]) != null)
                    {
                        card.set(currEditTime, clientData.Data);
                        history.Remove(card.HistoryNode);
                    }
                    else
                        cards[(int)clientData.ID] = card = new Card(this, clientData.ID, currEditTime, clientData.Data);

                history.AddFirst(card.HistoryNode);

                currEditTime = currEditTime.AddTicks(1);
            }

            connection.Send(NetworkMessage.Encode(new NetworkMessage.SyncCardsResponse(message.Tag, editTime)));
        }

        private void disconnectProc()
        {
            connection.Dispose();
            connection = null;
        }
        #endregion
    }
}