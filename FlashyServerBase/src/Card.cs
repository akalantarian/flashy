﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Sockets;

namespace Flashy.Server
{
    public sealed class Card
    {
        #region Variables
        private readonly Library library;
        private readonly uint id;
        private readonly LinkedListNode<Card> historyNode;
        private readonly object lockObject = new object();
        private DateTime lastEdit;
        private CachedCardData __data__;
        #endregion

        #region Accessors
        public Database Database { get { return library.Database; } }
        public Library Library { get { return library; } }
        public DirectoryInfo Directory { get { return library.Directory; } }
        public uint ID { get { return id; } }
        internal LinkedListNode<Card> HistoryNode { get { return historyNode; } }
        public DateTime LastEdit { get { return lastEdit; } }
        public bool Deleted { get { return getData(true).Data == null; } }
        public CardData Data { get { return getData(true).Data; } }

        private FileInfo file { get { return new FileInfo(fileName); } }
        private FileInfo backupFile { get { return new FileInfo(backupFileName); } }
        private static FileInfo getFile(DirectoryInfo directory, uint id, DateTime lastEdit) { return new FileInfo(getFileName(directory, id, lastEdit)); }
        private static FileInfo getBackupFile(DirectoryInfo directory, uint id, DateTime lastEdit) { return new FileInfo(getBackupFileName(directory, id, lastEdit)); }

        private string fileName { get { return getFileName(library.Directory, id, lastEdit); } }
        private string backupFileName { get { return getBackupFileName(library.Directory, id, lastEdit); } }
        private static string getFileName(DirectoryInfo directory, uint id, DateTime lastEdit) { return directory.FullName + @"\" + id + "-" + lastEdit.Ticks + @".card"; }
        private static string getBackupFileName(DirectoryInfo directory, uint id, DateTime lastEdit) { return getFileName(directory, id, lastEdit) + ".bak"; }
        #endregion

        #region Constructor
        internal Card(Library library, uint id, DateTime lastEdit)
        {
            this.library = library;
            this.id = id;
            this.historyNode = new LinkedListNode<Card>(this);
            this.lastEdit = lastEdit;
            this.__data__ = null;
        }
        internal Card(Library library, uint id, DateTime lastEdit, CardData data)
        {
            this.library = library;
            this.id = id;
            this.historyNode = new LinkedListNode<Card>(this);
            this.lastEdit = lastEdit;
            this.__data__ = new CachedCardData(this, data);
            if(data == null)
                file.Create().Dispose();
            else
            {
                FileInfo tempFile = new FileInfo(fileName + ".temp");
                using(BinaryWriter writer = new BinaryWriter(tempFile.Open(FileMode.CreateNew, FileAccess.Write, FileShare.None)))
                    data.Write(writer);
                tempFile.MoveTo(fileName);
                lock (library.Database.Cache)
                    library.Database.Cache.Add(this.__data__.Node);
            }
        }
        #endregion

        #region Functions

        internal void unload() { __data__ = null; }
        internal void deleteFile() { file.Delete(); }

        private CachedCardData getData(bool cache)
        {
            lock (lockObject)
            {
                CachedCardData thisData = __data__;
                if (thisData == null)
                {
                    FileInfo curFile = file;
                    if (curFile.Length == 0)
                        thisData = new CachedCardData(this, null);
                    else
                        using (BinaryReader reader = new BinaryReader(curFile.Open(FileMode.Open, FileAccess.Read, FileShare.Read)))
                            thisData = new CachedCardData(this, new CardData(reader));
                }

                if (cache && thisData.Data != null)
                    lock (library.Database.Cache)
                        library.Database.Cache.Add(thisData.Node);
                return thisData;
            }
        }

        internal bool set(DateTime lastEdit)
        {
            lock (lockObject)
            {
                if (lastEdit == this.lastEdit) return false;

                FileInfo old = file;
                this.lastEdit = lastEdit;
                old.MoveTo(fileName);
                return true;
            }
        }
        internal bool set(DateTime lastEdit, CardData newData)
        {
            lock (lockObject)
            {
                CachedCardData oldData = getData(false);

                if (CardData.Equivalent(oldData.Data, newData))
                    return set(lastEdit);

                FileInfo backup = file;
                backup.MoveTo(backupFileName);

                this.lastEdit = lastEdit;
                FileInfo newFile = file;

                if(newData == null)
                    newFile.Create().Dispose();
                else
                    using(BinaryWriter writer = new BinaryWriter(newFile.Open(FileMode.CreateNew, FileAccess.Write, FileShare.None)))
                        newData.Write(writer);

                backup.Delete();

                lock (library.Database.Cache)
                {
                    library.Database.Cache.Remove(oldData.Node);
                    this.__data__ = new CachedCardData(this, newData);
                    if (newData != null)
                        library.Database.Cache.Add(this.__data__.Node);
                }

                return true;
            }
        }

        #endregion
    }
}