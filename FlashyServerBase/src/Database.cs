﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Sockets;
using Flashy.Network;

namespace Flashy.Server
{
    public sealed class Database
    {
        #region Variables
        private readonly DirectoryInfo directory;
        private readonly Mutex mutex;
        private readonly MessageQueue<FMIMessage> messageQueue;
        private readonly TCPServer server;
        private readonly Dictionary<string, Library> activeLibraries;
        private readonly HashSet<TCPConnection> temporaryConnections;
        private readonly HashSet<TCPConnection> infoConnections;
        private readonly SortedSet<string> libraries;
        private readonly SortedSet<string> quizzes;
        private readonly CardCache cache;
        #endregion

        #region Accessors
        public DirectoryInfo Directory { get { return directory; } }
        internal CardCache Cache { get { return cache; } }
        public bool Connected
        {
            get
            {
                foreach(Library library in activeLibraries.Values)
                    if(library.Connected) return true;
                return false;
            }
        }
        public Library this[string name]
        {
            get
            {
                Library library;
                if(activeLibraries.TryGetValue(name, out library))
                    return library;
                return null;
            }
        }
        #endregion

        #region Constructor
        public Database(DirectoryInfo directory = null)
        {
            this.directory = directory ?? new DirectoryInfo(Environment.CurrentDirectory);
            this.directory.Create();

            this.mutex = new Mutex(false, ("FLASHY_" + this.directory.FullName).Replace(@"\", @":"));
            if (!this.mutex.WaitOne(0)) throw new Exception("Flashy editor is already opened at that directory");

            this.messageQueue = new MessageQueue<FMIMessage>();
            this.server = new TCPServer(Settings.LocalEndPoint, Settings.BackLog, Settings.ReconnectTime, Settings.TimeoutLength, Settings.MaxTimeouts);
            this.activeLibraries = new Dictionary<string, Library>();
            this.temporaryConnections = new HashSet<TCPConnection>();
            this.infoConnections = new HashSet<TCPConnection>();
            this.libraries = new SortedSet<string>();
            this.quizzes = new SortedSet<string>();

            foreach(DirectoryInfo library in this.directory.EnumerateDirectories())
            {
                this.libraries.Add(library.Name);
            }
            foreach(FileInfo library in this.directory.EnumerateFiles("*.quiz"))
            {
                this.quizzes.Add(library.Name.Substring(library.Name.Length - 5, 5));
            }
            
            this.cache = new CardCache(Settings.CacheCapacity);

            this.messageQueue.Process += process;
            this.server.OnConnect += (newConnection) => messageQueue.Enqueue(new FMIMessage.NewConnection(newConnection));

            this.messageQueue.Start();
            this.server.Start();
        }
        #endregion

        #region Functions
        internal void enqueue(FMIMessage message)
        {
            messageQueue.Enqueue(message);
        }

        private void networkConnect(TCPConnection receiveConnection)
        {
            messageQueue.Enqueue(new FMIMessage.NewConnection(receiveConnection));
        }
        private void networkDisconnect(TCPConnection receiveConnection, Exception e)
        {
            messageQueue.Enqueue(new FMIMessage.Disconnect(receiveConnection));
        }
        private void networkTimeout(TCPConnection receiveConnection)
        {
            receiveConnection.Send(NetworkMessage.Encode(NetworkMessage.PingRequest.Instance));
        }
        private void networkMessage(TCPConnection receiveConnection, byte[] data)
        {
            if (!temporaryConnections.Contains(receiveConnection)) return;

            NetworkMessage message = NetworkMessage.Decode(data);
            if(message is NetworkMessage.PingRequest)
                receiveConnection.Send(NetworkMessage.Encode(NetworkMessage.PingResponse.Instance));
            else if(message is NetworkMessage.SyncLibraryRequest)
                messageQueue.Enqueue(new FMIMessage.SyncLibraryRequest(receiveConnection, (NetworkMessage.SyncLibraryRequest)message));
            else if(message is NetworkMessage.GetInfoRequest)
                messageQueue.Enqueue(new FMIMessage.GetInfoRequest(receiveConnection));
            else
                throw new Exception("Unknown network message received " + message.GetType());
        }

        private void process(FMIMessage message)
        {
            Logger.Log(string.Format("{0,-18}", "Database") + "Process " + message);
            if(message is FMIMessage.NewConnection)
                connectProc((FMIMessage.NewConnection)message);
            else if(message is FMIMessage.Disconnect)
                disconnectProc((FMIMessage.Disconnect)message);
            else if(message is FMIMessage.SyncLibraryRequest)
                syncProc((FMIMessage.SyncLibraryRequest)message);
            else if(message is FMIMessage.GetInfoRequest)
                getInfoProc((FMIMessage.GetInfoRequest)message);
            else
                throw new Exception();
        }

        private void syncProc(FMIMessage.SyncLibraryRequest message)
        {
            if (!temporaryConnections.Remove(message.Connection)) return;
            Library library;
            if(!activeLibraries.TryGetValue(message.Library, out library))
            {
                activeLibraries.Add(message.Library, library = new Library(this, message.Library));
                if(!libraries.Contains(message.Library))
                {
                    libraries.Add(message.Library);
                    foreach(TCPConnection connection in infoConnections)
                        connection.Send(NetworkMessage.Encode(new NetworkMessage.GetInfoResponse(libraries.ToList(), quizzes.ToList())));
                }
            }
            message.Connection.OnReceive -= networkMessage;
            message.Connection.OnDisconnect -= networkDisconnect;
            library.enqueue(message);
        }

        private void getInfoProc(FMIMessage.GetInfoRequest message)
        {
            if(!temporaryConnections.Remove(message.Connection)) return;
            infoConnections.Add(message.Connection);
            message.Connection.Send(NetworkMessage.Encode(new NetworkMessage.GetInfoResponse(libraries.ToList(), quizzes.ToList())));
        }

        private void connectProc(FMIMessage.NewConnection message)
        {
            message.Connection.OnTimeout += networkTimeout;
            message.Connection.OnDisconnect += networkDisconnect;
            message.Connection.OnReceive += networkMessage;
            temporaryConnections.Add(message.Connection);
            message.Connection.Start();
        }
        private void disconnectProc(FMIMessage.Disconnect message)
        {
            message.Connection.Dispose();
            temporaryConnections.Remove(message.Connection);
            infoConnections.Remove(message.Connection);
        }
        #endregion
    }
}