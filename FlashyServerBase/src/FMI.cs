﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Sockets;

using Flashy.Network;

namespace Flashy.Server
{
    internal abstract class FMIMessage
    {
        internal sealed class NewConnection : FMIMessage
        {
            private readonly TCPConnection connection;
            internal TCPConnection Connection { get { return connection; } }
            internal NewConnection(TCPConnection connection) { this.connection = connection; }
            public override string ToString() { return "New Connection: Local=[" + connection.LocalEndPoint + "] Remote=[" + connection.RemoteEndPoint + "]"; }
        }

        internal sealed class GetInfoRequest : FMIMessage
        {
            private readonly TCPConnection connection;
            internal TCPConnection Connection { get { return connection; } }
            internal GetInfoRequest(TCPConnection connection) { this.connection = connection; }
            public override string ToString() { return "Get Info Request: Local=[" + connection.LocalEndPoint + "] Remote=[" + connection.RemoteEndPoint + "]"; }
        }

        internal sealed class SyncLibraryRequest : FMIMessage
        {
            private readonly TCPConnection connection;
            private readonly NetworkMessage.SyncLibraryRequest message;
            internal TCPConnection Connection { get { return connection; } }
            internal string Library { get { return message.Library; } }
            internal DateTime LastEdit { get { return message.LastEdit; } }
            internal bool Kick { get { return message.Kick; } }
            internal SyncLibraryRequest(TCPConnection connection, NetworkMessage.SyncLibraryRequest message) { this.connection = connection; this.message = message; }
            public override string ToString()
            {
                return "Sync Library Request: Library=" + message.Library + " LastEdit=" + message.LastEdit.Ticks + " Kick=" + message.Kick;
            }
        }

        internal sealed class SyncCardsRequest : FMIMessage
        {
            private readonly TCPConnection connection;
            private readonly NetworkMessage.SyncCardsRequest message;
            internal TCPConnection Connection { get { return connection; } }
            internal DateTime Tag { get { return message.Tag; } }
            internal ReadOnlyCollection<CardIDData> Cards { get { return message.Cards; } }
            internal SyncCardsRequest(TCPConnection connection, NetworkMessage.SyncCardsRequest message) { this.connection = connection; this.message = message; }
            public override string ToString()
            {
                StringBuilder returnString = new StringBuilder("Sync Cards Request: ");
                for (int i = 0; i < message.Cards.Count - 1; i++)
                    returnString.Append("[" + message.Cards[i] + "] ");
                if (message.Cards.Count > 0)
                    returnString.Append("[" + message.Cards.Last() + "]");
                return returnString.ToString();
            }
        }

        internal sealed class Disconnect : FMIMessage
        {
            private readonly TCPConnection connection;
            internal TCPConnection Connection { get { return connection; } }
            internal Disconnect(TCPConnection connection) { this.connection = connection; }
            public override string ToString() { return "Disconnect"; }
        }
    }
}