﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Windows;

namespace Flashy.Client.Editor
{
    public static class Program
    {
        [DllImport("kernel32")]
        static extern bool AllocConsole();

        [DllImport("user32.dll")]
        public static extern IntPtr SetWindowPos(IntPtr hWnd, int hWndInsertAfter, int x, int Y, int cx, int cy, int wFlags);

        [DllImport("kernel32.dll")]
        public static extern IntPtr GetConsoleWindow();

        private static Database database;

        [STAThread]
        public static void Main(string[] args)
        {
            StartupParameters parameters = ArgParser.Parse(args);
#if DEBUG
            Logger.Init(
                new FileInfo(@"C:\Users\Aik\Documents\FlashyDir\Client.log"), 
                new FileInfo(@"C:\Users\Aik\Documents\FlashyDir\ClientException.log"), 
                true);

            #region Console
            AllocConsole();
            Console.Title = "Client";
            Console.BackgroundColor = ConsoleColor.DarkGreen;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.CursorVisible = false;
            Console.BufferWidth = Console.WindowWidth = Console.LargestWindowWidth;
            Console.BufferHeight = Console.WindowHeight = Console.LargestWindowHeight / 2;
            SetWindowPos(GetConsoleWindow(), 0,
                0,
                0,
                System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Width,
                System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Height / 2, 0);
            Console.Clear();
            #endregion
            
            EventWaitHandle evt;
            while(!EventWaitHandle.TryOpenExisting(@"FLASHY", out evt)) ;
            evt.WaitOne();
#else
            Logger.Init(
                new FileInfo(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Flashy\Log.log"), 
                new FileInfo(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Flashy\Exception.log"), 
                false);
#endif

            Application app = new Application();
            app.ShutdownMode = ShutdownMode.OnMainWindowClose;

            database = new Database(parameters.Directory, null, parameters.Libraries.ToArray());
            database.Dispatcher = app.Dispatcher;
            //Task.Factory.StartNew(hook, TaskCreationOptions.LongRunning);
            app.Run(new Flashy.Client.GUI.FlashyWindow(database));
        }
        private static void hook() { }
    }
}
