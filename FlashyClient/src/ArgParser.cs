﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Windows;

namespace Flashy.Client.Editor
{
    internal class StartupParameters
    {
        private List<string> libraries = null;
        private DirectoryInfo directory = null;

        public List<string> Libraries { get { return libraries; } set { libraries = value; } }
        public DirectoryInfo Directory { get { return directory; } set { directory = value; } }
    }
    internal sealed class ArgParser
    {
        private enum Mode
        {
            None = 0x0,
            LibraryLiteral = 0x1,
            LibraryFile = 0x2,
            Directory = 0x4,
        }
        public static StartupParameters Parse(string[] args)
        {
            ArgParser parser = new ArgParser(args);
            return parser.parameters;
        }
        
        private StartupParameters parameters = new StartupParameters();
        private DirectoryInfo directory = new DirectoryInfo(Environment.CurrentDirectory);
        private List<string> orphanArgs = new List<string>();
        private List<string> libraries = new List<string>();
        private Mode currMode = Mode.None;
        private bool switchReady = true;
        private Mode processedModes = Mode.None;
        
        private ArgParser(string[] args)
        {
            parseArgs(args, false);
            parseArgs(orphanArgs.ToArray(), true);

            parameters.Libraries = libraries;
            parameters.Directory = directory;
        }

        private void parseArgs(string[] args, bool parseOrphans)
        {
            foreach(string argument in args)
            {
                switch(argument)
                {
                    case "-l":
                        if(currMode != Mode.None && !switchReady) throw new ArgumentException();
                        setLibraryLiteralMode();
                        break;
                    case "-lf":
                        if(currMode != Mode.None && !switchReady) throw new ArgumentException();
                        setLibraryFileMode();
                        break;
                    case "-d":
                        if(processedModes.HasFlag(Mode.Directory)) throw new ArgumentException();
                        if(currMode != Mode.None && !switchReady) throw new ArgumentException();
                        setDirectoryMode();
                        break;
                    default:
                        parseArgument(argument, parseOrphans);
                        break;
                }
            }
        }

        private void setLibraryLiteralMode()
        {
            currMode = Mode.LibraryLiteral;
            switchReady = false;
        }
        private void setLibraryFileMode()
        {
            currMode = Mode.LibraryFile;
            switchReady = false;
        }
        private void setDirectoryMode()
        {
            currMode = Mode.Directory;
            switchReady = false;
        }
        private void parseArgument(string argument, bool parseOrphans)
        {
            switch(currMode)
            {
                case Mode.LibraryLiteral:
                    parseLibraryLiteral(argument);
                    break;
                case Mode.LibraryFile:
                    parseLibraryFile(argument);
                    break;
                case Mode.Directory:
                    parseDirectory(argument);
                    break;
                case Mode.None:
                    if(parseOrphans)
                        parseOrphan(argument);
                    else
                        orphanArgs.Add(argument);
                    break;
            }
        }

        private void parseLibraryLiteral(string argument)
        {
            if(!Settings.CheckLibraryName(argument)) throw new ArgumentException();
            if(!libraries.Contains(argument))
                libraries.Add(argument);
            switchReady = true;
            processedModes |= Mode.LibraryFile;
        }
        private void parseLibraryFile(string argument)
        {
            foreach(string library in librariesFromFile(new FileInfo(argument)))
                if(!libraries.Contains(library))
                    libraries.Add(library);
            currMode = Mode.None;
            switchReady = true;
            processedModes |= Mode.LibraryFile;
        }
        private void parseDirectory(string argument)
        {
            directory = new DirectoryInfo(argument);
            currMode = Mode.None;
            switchReady = true;
            processedModes |= Mode.Directory;
        }
        private void parseOrphan(string argument)
        {
            if(File.Exists(argument))
            {
                FileInfo file = new FileInfo(argument);
                switch(file.Extension)
                {
                    case ".lib":
                    default:
                        parseLibraryFile(argument);
                        break;
                }
            }
            else if(!processedModes.HasFlag(Mode.Directory) && isValidDirectory(argument))
            {
                parseDirectory(argument);
            }
            else
            {
                parseLibraryLiteral(argument);
            }
        }





        private bool isValidDirectory(string name)
        {
            try { new DirectoryInfo(name); return true; } catch(Exception) { return false; }
        }
        private List<string> librariesFromFile(FileInfo file)
        {
            List<string> libraries = new List<string>();
            using(StreamReader reader = new StreamReader(file.Open(FileMode.Open, FileAccess.Read, FileShare.Read), Encoding.ASCII))
            {
                string line;
                while((line = reader.ReadLine()) != null)
                {
                    string[] libraryNames = line.Split(new char[] { ',', ' ', }, StringSplitOptions.RemoveEmptyEntries);
                    foreach(string libraryName in libraryNames)
                        if(!Settings.CheckLibraryName(libraryName)) throw new ArgumentException();
                        else libraries.Add(libraryName);
                }
            }
            return libraries;
        }
    }
}
