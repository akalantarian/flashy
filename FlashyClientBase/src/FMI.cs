﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Windows.Threading;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Sockets;

using Flashy.Network;

namespace Flashy.Client.Editor
{
    internal abstract class FMIMessage
    {
        internal sealed class NewConnection : FMIMessage
        {
            private readonly TCPConnection connection;
            internal TCPConnection Connection { get { return connection; } }
            internal NewConnection(TCPConnection connection) { this.connection = connection; }
            public override string ToString() { return "New Connection: Local=[" + connection.LocalEndPoint + "] Remote=[" + connection.RemoteEndPoint + "]"; }
        }

        internal sealed class GetInfoResponse : FMIMessage
        {
            private readonly TCPConnection connection;
            private readonly NetworkMessage.GetInfoResponse message;
            internal TCPConnection Connection { get { return connection; } }
            internal List<string> Libraries { get { return message.Libraries; } }
            internal List<string> Quizzes { get { return message.Quizzes; } }
            internal GetInfoResponse(TCPConnection connection, NetworkMessage.GetInfoResponse message) { this.connection = connection; this.message = message; }
            public override string ToString() { return "Get Info Response: Local=[" + connection.LocalEndPoint + "] Remote=[" + connection.RemoteEndPoint + "] Libraries=" + message.Libraries + " Quizzes=" + message.Quizzes; }
        }

        internal sealed class SyncLibraryResponse : FMIMessage
        {
            private readonly TCPConnection connection;
            private readonly NetworkMessage.SyncLibraryResponse message;
            internal TCPConnection Connection { get { return connection; } }
            internal ReadOnlyCollection<CardIDLastEditData> Cards { get { return message.Cards; } }
            internal SyncLibraryResponse(TCPConnection connection, NetworkMessage.SyncLibraryResponse message) { this.connection = connection; this.message = message; }
            public override string ToString()
            {
                StringBuilder returnString = new StringBuilder("Sync Library Response: ");
                for (int i = 0; i < message.Cards.Count - 1; i++)
                    returnString.Append("[" + message.Cards[i] + "] ");
                if (message.Cards.Count > 0)
                    returnString.Append("[" + message.Cards.Last() + "]");
                return returnString.ToString();
            }
        }

        internal sealed class SyncCardsResponse : FMIMessage
        {
            private readonly TCPConnection connection;
            private readonly NetworkMessage.SyncCardsResponse message;
            internal TCPConnection Connection { get { return connection; } }
            internal DateTime Tag { get { return message.Tag; } }
            internal DateTime EditTime { get { return message.LastEditTime; } }
            internal SyncCardsResponse(TCPConnection connection, NetworkMessage.SyncCardsResponse message) { this.connection = connection; this.message = message; }
            public override string ToString()
            {
                return "Sync Cards Response: Tag=" + message.Tag.Ticks + " EditTime=" + message.LastEditTime.Ticks;
            }
        }

        internal sealed class Disconnect : FMIMessage
        {
            private readonly TCPConnection connection;
            internal TCPConnection Connection { get { return connection; } }
            internal Disconnect(TCPConnection connection) { this.connection = connection; }
            public override string ToString() { return "Disconnect: Local=[" + connection.LocalEndPoint + "] Remote=[" + connection.RemoteEndPoint + "]"; }
        }

        internal sealed class AddCard : FMIMessage
        {
            private Card card;
            private readonly CardData data;
            private readonly ManualResetEvent evt;
            internal Card Card { get { return card; } }
            internal CardData Data { get { return data; } }
            internal AddCard(CardData data) { this.data = data; this.evt = new ManualResetEvent(false); }
            internal void Wait() { evt.WaitOne(); }
            internal void Set(Card card) { this.card = card; evt.Set(); }
            public override string ToString()
            {
                return "Add Card: Data=[" + (data == null ? "void" : data.ToString()) + "]";
            }
        }
        internal sealed class EditCard : FMIMessage
        {
            private readonly Card card;
            private readonly CardData data;
            private readonly ManualResetEvent evt;
            internal Card Card { get { return card; } }
            internal CardData Data { get { return data; } }
            internal EditCard(Card card, CardData data) { this.card = card; this.data = data; this.evt = new ManualResetEvent(false); }
            internal void Wait() { evt.WaitOne(); }
            internal void Set() { evt.Set(); }
            public override string ToString()
            {
                return "Edit Card: " + "ID=" + card.ID + " Data=[" + (data == null ? "void" : data.ToString()) + "]";
            }
        }

        //internal sealed class SetLibrariesAndFilter : FMIMessage
        //{
        //    private bool filterChange, libraryChange;
        //    private readonly Filter filter;
        //    private readonly List<string> libraries;
        //    private readonly ManualResetEvent evt;
        //    internal bool FilterChange { get { return filterChange; } }
        //    internal bool LibraryChange { get { return libraryChange; } }
        //    internal Filter Filter { get { return filter; } }
        //    internal List<string> Libraries { get { return libraries; } }
        //    internal SetLibrariesAndFilter(Filter filter) : this(filter, null) { libraryChange = false; }
        //    internal SetLibrariesAndFilter(List<string> libraries) : this(null, libraries) { filterChange = false; }
        //    internal SetLibrariesAndFilter(Filter filter, List<string> libraries) { filterChange = libraryChange = true; this.filter = filter; this.libraries = libraries; this.evt = new ManualResetEvent(false); }
        //    internal void Wait() { evt.WaitOne(); }
        //    internal void Set() { evt.Set(); }
        //    public override string ToString()
        //    {
        //        return "Set Libraries and Apply Filter: " + libraries + " Filter=" + (filter == null ? "void" : filter.ToString());
        //    }
        //}

        internal sealed class SetLibraries : FMIMessage
        {
            private readonly List<string> libraries;
            private readonly ManualResetEvent evt;
            internal List<string> Libraries { get { return libraries; } }
            internal SetLibraries(List<string> libraries) { this.libraries = libraries; this.evt = new ManualResetEvent(false); }
            internal void Wait() { evt.WaitOne(); }
            internal void Set() { evt.Set(); }
            public override string ToString()
            {
                return "Set Libraries: " + libraries;
            }
        }

        internal sealed class ApplyFilter : FMIMessage
        {
            private readonly Filter filter;
            private readonly ManualResetEvent evt;
            internal Filter Filter { get { return filter; } }
            internal ApplyFilter(Filter filter) { this.filter = filter; this.evt = new ManualResetEvent(false); }
            internal void Wait() { evt.WaitOne(); }
            internal void Set() { evt.Set(); }
            public override string ToString()
            {
                return "Apply Filter: Filter=" + (filter == null ? "void" : filter.ToString());
            }
        }

        internal sealed class CardChange : FMIMessage
        {
            private readonly Card card;
            private readonly FMIMessage.ApplyFilter filterMessage;
            private readonly CardData oldData;
            private readonly CardData newData;
            internal Card Card { get { return card; } }
            internal FMIMessage.ApplyFilter FilterMessage { get { return filterMessage; } }
            internal CardData OldData { get { return oldData; } }
            internal CardData NewData { get { return newData; } }
            internal CardChange(Card card, FMIMessage.ApplyFilter filterMessage, CardData oldData, CardData newData) { this.card = card; this.filterMessage = filterMessage; this.oldData = oldData; this.newData = newData; }
            public override string ToString()
            {
                return "Card Change: " + "ID=" + card.ID + " Filter=" + filterMessage.Filter + " Old=[" + (oldData == null ? "void" : oldData.ToString()) + "] New=[" + (newData == null ? "void" : newData.ToString()) + "]";
            }
        }
    }
}