﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Windows.Threading;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Sockets;

using Flashy.Network;

namespace Flashy.Client.Editor
{
    public sealed class Library : INotifyPropertyChanged
    {
        private sealed class Update : IComparable<Update>
        {
            private readonly Card card;
            private readonly bool syncStatus;
            private readonly DateTime time;
            private readonly bool hasData;
            private readonly CardData data;
            public Card Card { get { return card; } }
            public bool SyncStatus { get { return syncStatus; } }
            public DateTime Time { get { return time; } }
            public bool HasData { get { return hasData; } }
            public CardData Data { get { if (!hasData) throw new Exception(); return data; } }
            public Update(Card card, bool syncStatus, DateTime time, CardData data) { this.card = card; this.syncStatus = syncStatus; this.time = time; this.hasData = true; this.data = data; }
            public Update(Card card, bool syncStatus, DateTime time) { this.card = card; this.syncStatus = syncStatus; this.time = time; this.hasData = false; }

            public int CompareTo(object obj)
            {
                if (obj == null) return 1;
                Update other = obj as Update;
                if (other == null) throw new ArgumentException("Object is not a Update");
                return time.Ticks.CompareTo(other.time.Ticks);
            }
            public int CompareTo(Update other)
            {
                if (other == null) return 1;
                return time.Ticks.CompareTo(other.time.Ticks);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void propertyChangedAsync(params string[] properties) { this.propertyChangedAsync(database.Dispatcher, PropertyChanged, properties); }
        private void propertyChanged(params string[] properties) { this.propertyChanged(database.Dispatcher, PropertyChanged, properties); }
        
        private static readonly Regex fileNameParser = new Regex(@"^(\d+)-(\d+)(\#?)\.card((?:\.bak)?)$", RegexOptions.Compiled);

        #region Variables
        private readonly Database database;
        private readonly DirectoryInfo directory;
        private readonly string name;
        private readonly MessageQueue<FMIMessage> messageQueue;
#if !NO_NETWORK
        private readonly TCPClient client;
#endif

        private readonly List<Card> cards;
        private readonly HashSet<Card> unsyncedCards;
        private readonly Dictionary<DateTime, List<Card>> saveRequests;
        private readonly Dictionary<Card, DateTime> saveTags;

        private TCPConnection connection;
        private bool registered;
        private DateTime lastEdit;
        private FMIMessage.ApplyFilter filterMessage;
#endregion

#region Accessors
        public Database Database { get { return database; } }
        public DirectoryInfo Directory { get { return directory; } }
        public string Name { get { return name; } }

        public IReadOnlyCollection<Card> Cards { get { return cards; } }
        public Card this[uint id] { get { lock (messageQueue.LockObject) if ((int)id >= cards.Count) return null; return cards[(int)id]; } }
        public uint NextID { get { lock (messageQueue.LockObject) return (uint)cards.Count; } }
        public DateTime LastEdit { get { lock (messageQueue.LockObject) return lastEdit; } }

        public bool Synced { get { lock (messageQueue.LockObject) return connection != null && connection.Connected && saveRequests.Count == 0 && unsyncedCards.Count == 0; } }
        public bool Connected { get { lock (messageQueue.LockObject) return connection != null && connection.Connected; } }
        public Status Status
        {
            get
            {
                lock (messageQueue.LockObject)
                {
                    if (connection != null && connection.Connected)
                        return saveRequests.Count == 0 && unsyncedCards.Count == 0 ? Status.Synced : Status.UnSynced;
                    return Status.Disconnected;
                }
            }
        }
#endregion

#region Constructor
        internal Library(Database database, string name, FMIMessage.ApplyFilter filterMessage, out IEnumerable<Card> filteredCards)
        {
            this.database = database;
            this.directory = new DirectoryInfo(database.Directory + @"\" + name);
            this.name = name;
            this.messageQueue = new MessageQueue<FMIMessage>();
            this.messageQueue.Process += process;
#if !NO_NETWORK
            this.client = new TCPClient(Settings.RemoteEndPoint, Settings.ReconnectTime, Settings.TimeoutLength, Settings.MaxTimeouts);
            this.client.OnConnect += networkConnect;
#endif

            this.cards = new List<Card>();
            this.unsyncedCards = new HashSet<Card>();
            this.saveRequests = new Dictionary<DateTime, List<Card>>();
            this.saveTags = new Dictionary<Card, DateTime>();

            this.connection = null;
            this.registered = false;

            this.lastEdit = DateTime.MinValue;
            this.filterMessage = filterMessage;

            filteredCards = new List<Card>();
            if(this.directory.Exists)
            {
                foreach(FileInfo file in this.directory.GetFiles())
                {
                    if(file.Extension == ".temp" || File.Exists(file.FullName + ".bak"))
                    {
                        file.Delete();
                        continue;
                    }

                    Match match = fileNameParser.Match(file.Name);
                    if(!match.Success) throw new Exception("Invalid card file: " + file.FullName);
                    uint cardID = uint.Parse(match.Groups[1].Value);
                    DateTime cardLastEdit = new DateTime(long.Parse(match.Groups[2].Value));
                    bool cardSynced = match.Groups[3].Value == @"";
                    bool isBackup = match.Groups[4].Value == @".bak";

                    Card card = new Card(this, cardID, cardLastEdit, cardSynced);
                    if(!cardSynced)
                        this.unsyncedCards.Add(card);
                    if(this.lastEdit < cardLastEdit)
                        this.lastEdit = cardLastEdit;
                    while(this.cards.Count < (int)cardID)
                        this.cards.Add(null);

                    if(this.cards.Count == (int)cardID) //does not exist
                        this.cards.Add(card);
                    else if(cards[(int)cardID] == null) //does not exist
                        this.cards[(int)cardID] = card;
                    else if(isBackup) //file exists - replace with current
                    {
                        this.cards[(int)cardID].deleteFile();
                        this.cards[(int)cardID] = card;
                    }
                    if(isBackup)
                        file.MoveTo(file.FullName.Substring(0, file.FullName.Length - 4));
                }
                foreach(Card card in this.cards)
                    if(card == null) throw new InvalidDataException("Corrupted library - missing cards: " + this.name);
                    else if(!card.Deleted && Filter.Evaluate(this.filterMessage.Filter, name, card.Data))
                        ((List<Card>)filteredCards).Add(card);
            }
            else
                this.directory.Create();

            this.messageQueue.Start();
#if !NO_NETWORK
            this.client.Start();
#endif
        }
#endregion

        internal CardCache Cache { get { return database.Cache; } }

        internal void Enqueue(FMIMessage message)
        {
            messageQueue.Enqueue(message);
        }

        internal void Close()
        {
#if !NO_NETWORK
            this.client.Dispose();
            TCPConnection conn = connection;
            if(conn != null) conn.Dispose();
#endif
        }

        internal IEnumerable<Card> FilterCards(FMIMessage.ApplyFilter message)
        {
            lock (messageQueue.LockObject)
            {
                this.filterMessage = message;
                List<Card> filteredCards = new List<Card>();
                foreach(Card card in cards)
                    if(!card.Deleted && Filter.Evaluate(message.Filter, name, card.Data))
                        filteredCards.Add(card);
                return filteredCards;
            }
        }

        private void networkConnect(TCPConnection receiveConnection)
        {
            messageQueue.Enqueue(new FMIMessage.NewConnection(receiveConnection));
        }
        private void networkDisconnect(TCPConnection receiveConnection, Exception e)
        {
            messageQueue.Enqueue(new FMIMessage.Disconnect(receiveConnection));
        }
        private void networkTimeout(TCPConnection receiveConnection)
        {
            receiveConnection.Send(NetworkMessage.Encode(NetworkMessage.PingRequest.Instance));
        }
        private void networkMessage(TCPConnection receiveConnection, byte[] data)
        {
            if (connection != receiveConnection) return;

            NetworkMessage message = NetworkMessage.Decode(data);
            if (message is NetworkMessage.PingRequest)
                receiveConnection.Send(NetworkMessage.Encode(NetworkMessage.PingResponse.Instance));
            else if (message is NetworkMessage.SyncLibraryResponse)
                messageQueue.Enqueue(new FMIMessage.SyncLibraryResponse(receiveConnection, (NetworkMessage.SyncLibraryResponse)message));
            else if (message is NetworkMessage.SyncCardsResponse)
                messageQueue.Enqueue(new FMIMessage.SyncCardsResponse(receiveConnection, (NetworkMessage.SyncCardsResponse)message));
            else
                throw new Exception("Unknown network message received " + message.GetType());
        }

        private void process(FMIMessage message)
        {
            Logger.Log(string.Format("{0,-18}", name) + "Process " + message);
            if (message is FMIMessage.NewConnection)
                connectProc((FMIMessage.NewConnection)message);
            else if (message is FMIMessage.SyncLibraryResponse)
            {
                if (connection == ((FMIMessage.SyncLibraryResponse)message).Connection)
                    syncProc((FMIMessage.SyncLibraryResponse)message);
            }
            else if (message is FMIMessage.SyncCardsResponse)
            {
                if (connection == ((FMIMessage.SyncCardsResponse)message).Connection)
                    saveProc((FMIMessage.SyncCardsResponse)message);
            }
            else if (message is FMIMessage.Disconnect)
            {
                if (connection == ((FMIMessage.Disconnect)message).Connection)
                    disconnectProc();
            }
            else if (message is FMIMessage.EditCard)
                editCardProc((FMIMessage.EditCard)message);
            else if (message is FMIMessage.AddCard)
                addCardProc((FMIMessage.AddCard)message);
            else
                throw new Exception("Unknown fmi message received " + message.GetType());
        }

        private void connectProc(FMIMessage.NewConnection message)
        {
            if (connection != null) disconnectProc();
            connection = message.Connection;
            connection.OnReceive += networkMessage;
            connection.OnTimeout += networkTimeout;
            connection.OnDisconnect += networkDisconnect;
            connection.Start();

            connection.Send(NetworkMessage.Encode(new NetworkMessage.SyncLibraryRequest(name, lastEdit, true)));

            propertyChangedAsync("Connected", "Synced", "Status");
            database.SyncCheck();
        }
        private void disconnectProc()
        {
            connection.Dispose();
            connection = null;
            registered = false;

            unsyncedCards.Clear();
            foreach (Card card in saveTags.Keys)
                unsyncedCards.Add(card);
            saveRequests.Clear();
            saveTags.Clear();

            propertyChangedAsync("Connected", "Synced", "Status");
            database.SyncCheck();
        }








        internal void syncProc(FMIMessage.SyncLibraryResponse message)
        {
            List<Update> updateList = new List<Update>();
            List<ResolveRequest> resolveList = new List<ResolveRequest>();
            Dictionary<Card, DateTime> resolveServerTimes = new Dictionary<Card, DateTime>();

            foreach (CardIDLastEditData serverData in message.Cards)
            {
                if (cards.Count <= serverData.ID)
                {
                    while (cards.Count <= serverData.ID)
                        cards.Add(new Card(this, (uint)cards.Count, DateTime.MinValue, true, null));
                    updateList.Add(new Update(cards.Last(), true, serverData.LastEditTime, serverData.Data));
                    continue;
                }
                Card card = cards[(int)serverData.ID];
                CardData localData = card.Data;
                if (unsyncedCards.Contains(card))
                {
                    if (CardData.Equivalent(localData, serverData.Data))
                    {
                        unsyncedCards.Remove(card);
                        updateList.Add(new Update(card, true, serverData.LastEditTime));
                    }
                    else
                    {
                        resolveList.Add(new ResolveRequest(card, localData, serverData.Data));
                        resolveServerTimes.Add(card, serverData.LastEditTime);
                    }
                }
                else
                    updateList.Add(new Update(card, true, serverData.LastEditTime, serverData.Data));
            }

            if (resolveList.Count > 0)
            {
                database.Resolver(resolveList.AsReadOnly());

                foreach (ResolveRequest resolveRequest in resolveList)
                {
                    Card card = resolveRequest.Card;
                    CardData localData = resolveRequest.LocalData;
                    CardData serverData = resolveRequest.ServerData;
                    CardData resolvedData = resolveRequest.ResolvedData;

                    if (CardData.Equivalent(resolvedData, serverData))
                    {
                        unsyncedCards.Remove(card);
                        updateList.Add(new Update(card, true, resolveServerTimes[card], serverData));
                    }
                    else if (CardData.Equivalent(resolvedData, serverData))
                        updateList.Add(new Update(card, false, resolveServerTimes[card]));
                    else
                        updateList.Add(new Update(card, false, resolveServerTimes[card], resolvedData));
                }
            }

            updateList.Sort();

            foreach (Update update in updateList)
            {
                if (update.HasData)
                    update.Card.set(update.Time, update.SyncStatus, filterMessage, update.Data);
                else
                    update.Card.set(update.Time, update.SyncStatus);
                if (lastEdit < update.Time)
                    lastEdit = update.Time;
            }

            if (unsyncedCards.Count > 0)
            {
                DateTime tag = DateTime.UtcNow;
                while(saveRequests.ContainsKey(tag))
                    tag = tag.AddTicks(1);

                List<Card> localRequestCards = new List<Card>(unsyncedCards.Count);
                List<CardIDData> serverRequestCards = new List<CardIDData>(unsyncedCards.Count);
                foreach (Card card in unsyncedCards)
                {
                    localRequestCards.Add(card);
                    serverRequestCards.Add(new CardIDData(card.ID, card.Data));
                    saveTags.Add(card, tag);
                }
                saveRequests.Add(tag, localRequestCards);

                connection.Send(NetworkMessage.Encode(new NetworkMessage.SyncCardsRequest(tag, serverRequestCards.AsReadOnly())));

                unsyncedCards.Clear();
            }

            propertyChangedAsync("Synced", "Status", "LastEdit", "NextID");
            database.SyncCheck();
            registered = true;
        }

        private void saveProc(FMIMessage.SyncCardsResponse message)
        {
            List<Card> cardList;
            if (saveRequests.TryGetValue(message.Tag, out cardList))
            {
                for (int i = 0; i < cardList.Count; i++)
                {
                    DateTime editTime = message.EditTime.AddTicks(i);
                    if (saveTags[cardList[i]] == message.Tag)
                    {
                        saveTags.Remove(cardList[i]);
                        cardList[i].set(editTime, true);
                    }
                    else
                        cardList[i].set(editTime, false);
                    if (lastEdit < editTime) lastEdit = editTime;
                }
                saveRequests.Remove(message.Tag);
                propertyChangedAsync("Synced", "Status", "LastEdit");
                database.SyncCheck();
            }
        }

        public Card AddCard(CardData data)
        {
            FMIMessage.AddCard message = new FMIMessage.AddCard(data);
            messageQueue.Enqueue(message);
            message.Wait();
            return message.Card;
        }
        public void EditCard(Card card, CardData data)
        {
            if (card.Library != this) throw new Exception("Card is not part of this library");
            FMIMessage.EditCard message = new FMIMessage.EditCard(card, data);
            messageQueue.Enqueue(message);
            message.Wait();
        }


        private void addCardProc(FMIMessage.AddCard message)
        {
            Card card = new Card(this, (uint)cards.Count, DateTime.MinValue, false, message.Data);
            cards.Add(card);
            if(message.Data != null)
                database.Enqueue(new FMIMessage.CardChange(card, filterMessage, null, message.Data));
            if (registered)
            {
                DateTime tag = DateTime.UtcNow;
                while (saveRequests.ContainsKey(tag))
                    tag = tag.AddTicks(1);

                saveRequests.Add(tag, new List<Card>(1) { card });
                saveTags.Add(card, tag);

                connection.Send(NetworkMessage.Encode(new NetworkMessage.SyncCardsRequest(tag, new CardIDData(card.ID, message.Data))));
            }
            else
                unsyncedCards.Add(card);
            message.Set(card);

            propertyChangedAsync("Synced", "Status", "NextID");
            database.SyncCheck();
        }

        private void editCardProc(FMIMessage.EditCard message)
        {
            message.Card.set(message.Card.LastEdit, false, filterMessage, message.Data);
            if (registered)
            {
                DateTime tag = DateTime.UtcNow;
                while (saveRequests.ContainsKey(tag))
                    tag = tag.AddTicks(1);

                saveRequests.Add(tag, new List<Card>(1) { message.Card });
                if (saveTags.ContainsKey(message.Card))
                    saveTags[message.Card] = tag;
                else
                    saveTags.Add(message.Card, tag);

                connection.Send(NetworkMessage.Encode(new NetworkMessage.SyncCardsRequest(tag, new CardIDData(message.Card.ID, message.Data))));
            }
            else
                unsyncedCards.Add(message.Card);
            message.Set();

            propertyChangedAsync("Synced", "Status");
            database.SyncCheck();
        }
    }
}