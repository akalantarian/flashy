﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Windows.Threading;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Sockets;

namespace Flashy.Client.Editor
{
    public sealed class Card : INotifyPropertyChanged, IComparable<Card>, IComparable
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void propertyChangedAsync(params string[] properties) { this.propertyChangedAsync(library.Database.Dispatcher, PropertyChanged, properties); }
        private void propertyChanged(params string[] properties) { this.propertyChanged(library.Database.Dispatcher, PropertyChanged, properties); }

        private readonly Library library;
        private readonly uint id;
        private readonly object lockObject = new object();
        private DateTime lastEdit;
        private bool synced;
        private CachedCardData __data__;

        public Database Database { get { return library.Database; } }
        public Library Library { get { return library; } }
        public DirectoryInfo Directory { get { return library.Directory; } }
        public uint ID { get { return id; } }
        public DateTime LastEdit { get { return lastEdit; } }
        public bool Synced { get { return synced; } }
        public bool Deleted { get { return getData(true).Data == null; } }
        public CardData Data { get { return getData(true).Data; } }

        private FileInfo file { get { return new FileInfo(fileName); } }
        private FileInfo backupFile { get { return new FileInfo(backupFileName); } }
        private static FileInfo getFile(DirectoryInfo directory, uint id, DateTime lastEdit, bool synced) { return new FileInfo(getFileName(directory, id, lastEdit, synced)); }
        private static FileInfo getBackupFile(DirectoryInfo directory, uint id, DateTime lastEdit, bool synced) { return new FileInfo(getBackupFileName(directory, id, lastEdit, synced)); }

        private string fileName { get { return getFileName(library.Directory, id, lastEdit, synced); } }
        private string backupFileName { get { return getBackupFileName(library.Directory, id, lastEdit, synced); } }
        private static string getFileName(DirectoryInfo directory, uint id, DateTime lastEdit, bool synced) { return directory.FullName + @"\" + id + "-" + lastEdit.Ticks + (synced ? @".card" : @"#.card"); }
        private static string getBackupFileName(DirectoryInfo directory, uint id, DateTime lastEdit, bool synced) { return getFileName(directory, id, lastEdit, synced) + ".bak"; }

        internal Card(Library library, uint id, DateTime lastEdit, bool synced)
        {
            this.library = library;
            this.id = id;
            this.lastEdit = lastEdit;
            this.synced = synced;
            this.__data__ = null;
        }
        internal Card(Library library, uint id, DateTime lastEdit, bool synced, CardData data)
        {
            this.library = library;
            this.id = id;
            this.lastEdit = lastEdit;
            this.synced = synced;
            this.__data__ = new CachedCardData(this, data);
            if(data == null)
                file.Create().Dispose();
            else
            {
                FileInfo tempFile = new FileInfo(fileName + ".temp");
                using(BinaryWriter writer = new BinaryWriter(tempFile.Open(FileMode.CreateNew, FileAccess.Write, FileShare.None)))
                    data.Write(writer);
                tempFile.MoveTo(fileName);
                lock (library.Database.Cache)
                    library.Database.Cache.Add(this.__data__.Node);
            }
        }

        internal void unload() { __data__ = null; }
        internal void deleteFile() { file.Delete(); }

        private CachedCardData getData(bool cache)
        {
            lock (lockObject)
            {
                CachedCardData thisData = __data__;
                if (thisData == null)
                {
                    FileInfo curFile = file;
                    if (curFile.Length == 0)
                        thisData = new CachedCardData(this, null);
                    else
                        using (BinaryReader reader = new BinaryReader(curFile.Open(FileMode.Open, FileAccess.Read, FileShare.Read)))
                            thisData = new CachedCardData(this, new CardData(reader));
                }

                if (cache && thisData.Data != null)
                    lock (library.Database.Cache)
                        library.Database.Cache.Add(thisData.Node);
                return thisData;
            }
        }

        internal bool set(DateTime lastEdit, bool synced)
        {
            lock (lockObject)
            {
                if(lastEdit == this.lastEdit && synced == this.synced) return false;

                FileInfo old = file;
                this.lastEdit = lastEdit;
                this.synced = synced;
                old.MoveTo(fileName);
                propertyChangedAsync("LastEdit", "Synced");
                return true;
            }
        }
        internal bool set(DateTime lastEdit, bool synced, FMIMessage.ApplyFilter filterMessage, CardData newData)
        {
            lock (lockObject)
            {
                CachedCardData oldData = getData(false);

                if (CardData.Equivalent(oldData.Data, newData)) 
                    return set(lastEdit, synced);

                FileInfo backup = file;
                backup.MoveTo(backupFileName);

                this.lastEdit = lastEdit;
                this.synced = synced;
                FileInfo newFile = file;

                if(newData == null)
                    newFile.Create().Dispose();
                else
                    using(BinaryWriter writer = new BinaryWriter(newFile.Open(FileMode.CreateNew, FileAccess.Write, FileShare.None)))
                        newData.Write(writer);

                backup.Delete();

                lock (library.Database.Cache)
                {
                    library.Database.Cache.Remove(oldData.Node);
                    this.__data__ = new CachedCardData(this, newData);
                    if (newData != null)
                        library.Database.Cache.Add(this.__data__.Node);
                }

                library.Database.Enqueue(new FMIMessage.CardChange(this, filterMessage, oldData.Data, newData));

                propertyChangedAsync("LastEdit", "Synced", "Data", "Deleted");

                return true;
            }
        }

        public int CompareTo(object obj)
        {
            if(obj == null) return 1;

            Card other = obj as Card;
            if(other != null)
                return CompareTo(other);
            else
                throw new ArgumentException("Object is not a Card");
        }
        public int CompareTo(Card other)
        {
            if(other == null) return 1;
            if(library.Name != other.library.Name)
                return library.Name.CompareTo(other.library.Name);
            return id.CompareTo(other.id);
        }

        public void Delete() { Save(null); }
        public void Save(CardData newData)
        {
            library.EditCard(this, newData);
        }
    }
}