﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Windows.Threading;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Sockets;

namespace Flashy.Client.Editor
{
    public delegate void Resolver(ReadOnlyCollection<ResolveRequest> resolveRequests);
    public sealed class ResolveRequest
    {
        private readonly Card card;
        private readonly CardData localData, serverData;
        private CardData resolvedData;
        private readonly object lockObject = new object();

        public Card Card { get { return card; } }
        public CardData LocalData { get { return localData; } }
        public CardData ServerData { get { return serverData; } }
        public CardData ResolvedData { get { lock (lockObject) if (resolvedData == null) throw new Exception("Not resolved yet"); else return resolvedData; } }
        public bool Resolved { get { return resolvedData == null; } }

        internal ResolveRequest(Card card, CardData localData, CardData serverData)
        {
            this.card = card;
            this.localData = localData;
            this.serverData = serverData;
            this.resolvedData = null;
        }

        public void Resolve(CardData data)
        {
            if (data == null) throw new ArgumentNullException("Null data");
            lock (lockObject)
                if (resolvedData != null)
                    throw new Exception("Already resolved");
                else
                    this.resolvedData = data;
        }
    }
}