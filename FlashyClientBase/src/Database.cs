﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Windows.Threading;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Sockets;

using Flashy.Network;

namespace Flashy.Client.Editor
{
    public enum Status : byte
    {
        Disconnected,
        UnSynced,
        Synced
    }
    public sealed class Database : INotifyPropertyChanged
    {
        private static void serverResolver(ReadOnlyCollection<ResolveRequest> resolveRequests)
        {
            foreach (ResolveRequest resolveRequest in resolveRequests)
                resolveRequest.Resolve(resolveRequest.ServerData);
        }
        public event PropertyChangedEventHandler PropertyChanged;
        private void propertyChangedAsync(params string[] properties) { this.propertyChangedAsync(dispatcher, PropertyChanged, properties); }
        private void propertyChanged(params string[] properties) { this.propertyChanged(dispatcher, PropertyChanged, properties); }
        private void invoke(Action action) { this.invoke(dispatcher, action); }

        private readonly DirectoryInfo directory;
        private readonly Mutex mutex;
        private readonly MessageQueue<FMIMessage> messageQueue;
        private readonly MessageQueue<Action> invokerQueue;
        private readonly TCPClient client;
        private readonly Resolver resolver;
        private readonly SortedDictionary<string, Library> activeLibraries;
        private readonly SortedSet<string> libraries;
        private readonly SortedSet<string> quizzes;
        private readonly CardCache cache;
        private readonly ObservableAVLTree<Card> filteredCards;
        private Dispatcher dispatcher;
        private FMIMessage.ApplyFilter filterMessage;
        private TCPConnection connection;

        public DirectoryInfo Directory { get { return directory; } }
        public Dispatcher Dispatcher { get { return dispatcher; } set { dispatcher = value; } }
        internal Resolver Resolver { get { return resolver; } }
        internal CardCache Cache { get { return cache; } }
        public ObservableAVLTree<Card> FilteredCards { get { return filteredCards; } }
        public Filter Filter { get { return filterMessage.Filter; } }

        public bool Connected { get { lock (messageQueue.LockObject) { if(connection == null || !connection.Connected) return false; foreach(Library library in activeLibraries.Values) if(!library.Connected) return false; return true; } } }
        public bool Synced { get { lock (messageQueue.LockObject) { foreach(Library library in activeLibraries.Values) if(!library.Synced) return false; return true; } } }
        public Status Status
        {
            get
            {
                lock (messageQueue.LockObject)
                {
                    bool synced = connection != null && connection.Connected;
                    foreach(Library library in activeLibraries.Values)
                    {
                        if(!library.Connected) return Status.Disconnected;
                        if(synced && !library.Synced) synced = false;
                    }
                    return synced ? Status.Synced : Status.UnSynced;
                }
            }
        }

        public IEnumerable<string> ActiveLibraries { get { return activeLibraries.Keys; } }
        public IEnumerable<string> Libraries { get { return libraries; } }
        //public IEnumerable<string> Quizzes { get { return quizzes; } }
        public Library this[string name]
        {
            get
            {
                lock(messageQueue.LockObject)
                {
                    Library library;
                    if(activeLibraries.TryGetValue(name, out library))
                        return library;
                    return null;
                }
            }
        }
        
        public Database(DirectoryInfo directory, Filter filter, params string[] libraries)
        {
            this.directory = directory ?? new DirectoryInfo(Environment.CurrentDirectory);
            this.directory.Create();

            this.mutex = new Mutex(false, ("FLASHY_" + this.directory.FullName).Replace(@"\", @":"));
            if (!this.mutex.WaitOne(0)) throw new Exception("Flashy editor is already opened at that directory");

            this.messageQueue = new MessageQueue<FMIMessage>();
            this.invokerQueue = new MessageQueue<Action>();
#if !NO_NETWORK
            this.client = new TCPClient(Settings.RemoteEndPoint, Settings.ReconnectTime, Settings.TimeoutLength, Settings.MaxTimeouts);
            this.client.OnConnect += networkConnect;
#endif
            this.resolver = serverResolver;
            this.activeLibraries = new SortedDictionary<string, Library>();
            this.libraries = new SortedSet<string>();
            this.quizzes = new SortedSet<string>();
            this.cache = new CardCache(Settings.CacheCapacity);
            this.cache.OnRemove += (node) => node.Value.unload();
            this.dispatcher = null;
            this.filteredCards = new ObservableAVLTree<Card>();
            this.filterMessage = new FMIMessage.ApplyFilter(filter);

            this.messageQueue.Process += process;
            this.invokerQueue.Process += invoke;

            foreach(DirectoryInfo library in this.directory.EnumerateDirectories())
            {
                this.libraries.Add(library.Name);
            }
            foreach(FileInfo library in this.directory.EnumerateFiles("*.quiz"))
            {
                this.quizzes.Add(library.Name.Substring(library.Name.Length - 5, 5));
            }

            if(libraries != null)
            {
                for(int i = 0; i < libraries.Length; i++)
                {
                    if(!Settings.CheckLibraryName(libraries[i])) throw new ArgumentException("Invalid library name");
                    for(int j = 0; j < libraries.Length; j++)
                        if(j != i && libraries[i].ToLower() == libraries[j].ToLower()) throw new ArgumentException("Duplicate library found");
                }

                Parallel.ForEach(libraries, (name) =>
                    {
                        IEnumerable<Card> libCards;
                        Library library = new Library(this, name, this.filterMessage, out libCards);
                        lock (this.filteredCards) foreach(Card card in libCards) this.filteredCards.Add(card);
                        lock (this.activeLibraries) this.activeLibraries.Add(name, library);
                        lock (this.libraries) this.libraries.Add(name);
                        //Logger.Log("Library \"" + name + "\" Initialized");
                    });
            }

            this.messageQueue.Start();
            this.invokerQueue.Start();
#if !NO_NETWORK
            this.client.Start();
#endif
            //Logger.Log("Database Initialized at " + this.directory.FullName);
        }

        internal void Enqueue(FMIMessage message)
        {
            messageQueue.Enqueue(message);
        }

        internal void SyncCheck()
        {
            propertyChangedAsync("Connected", "Synced", "Status");
        }
#if !NO_NETWORK
        private void networkConnect(TCPConnection receiveConnection)
        {
            messageQueue.Enqueue(new FMIMessage.NewConnection(receiveConnection));
        }
        private void networkDisconnect(TCPConnection receiveConnection, Exception e)
        {
            messageQueue.Enqueue(new FMIMessage.Disconnect(receiveConnection));
        }
        private void networkTimeout(TCPConnection receiveConnection)
        {
            receiveConnection.Send(NetworkMessage.Encode(NetworkMessage.PingRequest.Instance));
        }
        private void networkMessage(TCPConnection receiveConnection, byte[] data)
        {
            if(connection != receiveConnection) return;

            NetworkMessage message = NetworkMessage.Decode(data);
            if(message is NetworkMessage.PingRequest)
                receiveConnection.Send(NetworkMessage.Encode(NetworkMessage.PingResponse.Instance));
            else if(message is NetworkMessage.GetInfoResponse)
                messageQueue.Enqueue(new FMIMessage.GetInfoResponse(receiveConnection, (NetworkMessage.GetInfoResponse)message));
            else
                throw new Exception("Unknown network message received " + message.GetType());
        }
#endif
        private void process(FMIMessage message)
        {
            Logger.Log(string.Format("{0,-18}", "Database") + "Process " + message);
            lock (messageQueue.LockObject)
            {
                if(message is FMIMessage.CardChange)
                    cardChangeProc((FMIMessage.CardChange)message);
                else if(message is FMIMessage.ApplyFilter)
                    applyFilterProc((FMIMessage.ApplyFilter)message);
                else if(message is FMIMessage.SetLibraries)
                    setLibrariesProc((FMIMessage.SetLibraries)message);
#if !NO_NETWORK
                else if(message is FMIMessage.NewConnection)
                    connectProc((FMIMessage.NewConnection)message);
                else if(message is FMIMessage.Disconnect)
                {
                    if(connection == ((FMIMessage.Disconnect)message).Connection)
                        disconnectProc();
                }
                else if(message is FMIMessage.GetInfoResponse)
                {
                    if(connection == ((FMIMessage.GetInfoResponse)message).Connection)
                        getInfoResponseProc((FMIMessage.GetInfoResponse)message);
                }
#endif
                else
                    throw new Exception("Unknown fmi message received " + message.GetType());
            }
        }

        //public void SetLibrariesAndFilter(Filter filter, params string[] libraries)
        //{
        //    bool filterChange = false;
        //    bool libraryChange = false;
        //    if(!Filter.Equivalent(filterMessage.Filter, filter))
        //        filterChange = true;
            
        //    for(int i = 0; i < libraries.Length; i++)
        //    {
        //        if(!Settings.CheckLibraryName(libraries[i])) throw new ArgumentException("Invalid library name");
        //        for(int j = 0; j < libraries.Length; j++)
        //            if(j != i && libraries[i].ToLower() == libraries[j].ToLower()) throw new ArgumentException("Duplicate library found");
        //        if(!libraryChange && !activeLibraries.Keys.ContainsIgnoreCase(libraries[i])) libraryChange = true;
        //    }
        //    if(!libraryChange)
        //        foreach(string library in activeLibraries.Keys)
        //            if(!libraries.ContainsIgnoreCase(library))
        //            {
        //                libraryChange = true;
        //                break;
        //            }

        //    FMIMessage.SetLibrariesAndFilter message;
        //    if(filterChange)
        //    {
        //        if(libraryChange)
        //            message = new FMIMessage.SetLibrariesAndFilter(filter, libraries.ToList());
        //        else
        //            message = new FMIMessage.SetLibrariesAndFilter(filter);
        //    }
        //    else if(libraryChange)
        //        message = new FMIMessage.SetLibrariesAndFilter(libraries.ToList());
        //    else return;

        //    messageQueue.Enqueue(message);
        //    message.Wait();
        //}

        public void SetLibraries(params string[] libraries)
        {
            for(int i = 0; i < libraries.Length; i++)
            {
                if(!Settings.CheckLibraryName(libraries[i])) throw new ArgumentException("Invalid library name");
                for(int j = 0; j < libraries.Length; j++)
                    if(j != i && libraries[i].ToLower() == libraries[j].ToLower()) throw new ArgumentException("Duplicate library found");
            }

            FMIMessage.SetLibraries message = new FMIMessage.SetLibraries(libraries.ToList());
            messageQueue.Enqueue(message);
            message.Wait();
        }
        private void setLibrariesProc(FMIMessage.SetLibraries message)
        {
            bool createdLibrary = false;
            List<string> newLibraries = new List<string>();
            List<string> removedLibraries = new List<string>();

            foreach(string library in message.Libraries)
                if(!activeLibraries.Keys.ContainsIgnoreCase(library))
                    newLibraries.Add(library);

            foreach(string library in activeLibraries.Keys)
                if(!message.Libraries.ContainsIgnoreCase(library))
                    removedLibraries.Add(library);

            List<Card> addedCards = new List<Card>();
            List<Card> removedCards = new List<Card>();
            Parallel.Invoke(() =>
            {
                Parallel.ForEach(newLibraries, (name) =>
                {
                    IEnumerable<Card> libCards;
                    Library library = new Library(this, name, filterMessage, out libCards);
                    lock (addedCards) addedCards.AddRange(libCards);
                    lock (activeLibraries) activeLibraries.Add(name, library);
                    lock (libraries) if(libraries.Add(name) && !createdLibrary) createdLibrary = true;
                    //Logger.Log("Library \"" + name + "\" Initialized");
                });
            },
            () =>
            {
                Parallel.ForEach(removedLibraries, (library) =>
                {
                    Library lib = activeLibraries[library];
                    lib.Close();
                    foreach(Card card in lib.Cards)
                        if(Filter.Evaluate(Filter, library, card.Data))
                            removedCards.Add(card);
                });
            });

            if(addedCards.Count > 0 || removedCards.Count > 0)
                invokerQueue.Enqueue(() =>
                {
                    foreach(Card card in removedCards)
                        filteredCards.Remove(card);
                    foreach(Card card in addedCards)
                        filteredCards.Add(card);
                });

            foreach(string library in removedLibraries)
                activeLibraries.Remove(library);

            if(newLibraries.Count > 0 || removedLibraries.Count > 0)
                propertyChangedAsync("ActiveLibraries", "Connected", "Synced", "Status");
            if(createdLibrary)
                propertyChangedAsync("Libraries");

            message.Set();
        }

        public void ApplyFilter(Filter filter)
        {
            if(Filter.Equivalent(filterMessage.Filter, filter)) return;
            FMIMessage.ApplyFilter message = new FMIMessage.ApplyFilter(filter);
            messageQueue.Enqueue(message);
            message.Wait();
        }

        private void applyFilterProc(FMIMessage.ApplyFilter message)
        {
            if(message.Filter == filterMessage.Filter) { message.Set(); return; }

            filterMessage = message;

            List<Card> addedCards = new List<Card>();
            Parallel.ForEach(activeLibraries.Values, (library) =>
            {
                IEnumerable<Card> newCards = library.FilterCards(message);
                lock (addedCards) addedCards.AddRange(newCards);
            });

            invokerQueue.Enqueue(() =>
            {
                filteredCards.Clear();
                foreach(Card card in addedCards)
                    filteredCards.Add(card);
            });

            message.Set();
        }

        private void cardChangeProc(FMIMessage.CardChange message)
        {
            if(filterMessage != message.FilterMessage) return;

            if( (message.NewData != null && Filter.Evaluate(filterMessage.Filter, message.Card.Library.Name, message.NewData))
                && 
                (message.OldData == null || !Filter.Evaluate(filterMessage.Filter, message.Card.Library.Name, message.OldData)))
            {
                invokerQueue.Enqueue(() =>
                {
                    filteredCards.Add(message.Card);
                });
            }
            else if((message.OldData != null && Filter.Evaluate(filterMessage.Filter, message.Card.Library.Name, message.OldData))
                &&
                (message.NewData == null || !Filter.Evaluate(filterMessage.Filter, message.Card.Library.Name, message.NewData)))
            {
                invokerQueue.Enqueue(() =>
                {
                    filteredCards.Remove(message.Card);
                });
            }
        }

#if !NO_NETWORK
        private void getInfoResponseProc(FMIMessage.GetInfoResponse message)
        {
            invokerQueue.Enqueue(() =>
            {
                libraries.Clear();
                foreach(string library in message.Libraries)
                    libraries.Add(library);
                quizzes.Clear();
                foreach(string quiz in message.Quizzes)
                    quizzes.Add(quiz);
                if(message.Quizzes.Count > 0)
                   propertyChanged("Libraries", "Quizzes");
                else
                    propertyChanged("Libraries");

                //bool changed = false;
                //foreach(string library in message.Libraries)
                //    if(!libraries.ContainsIgnoreCase(library))
                //    {
                //        changed = true;
                //        libraries.Add(library);
                //    }
                //foreach(string library in libraries)
                //    if(!message.Libraries.ContainsIgnoreCase(library))
                //    {
                //        changed = true;
                //        libraries.Remove(library);
                //    }
                //if(changed)
                //    propertyChanged("Libraries");

                //changed = false;

                //foreach(string quiz in message.Quizzes)
                //    if(!quizzes.Contains(quiz))
                //    {
                //        changed = true;
                //        quizzes.Add(quiz);
                //    }
                //foreach(string quiz in quizzes)
                //    if(!message.Quizzes.Contains(quiz))
                //    {
                //        changed = true;
                //        quizzes.Remove(quiz);
                //    }

                //if(changed)
                //    propertyChanged("Quizzes");
            });
        }

        private void connectProc(FMIMessage.NewConnection message)
        {
            if(connection != null) disconnectProc();
            connection = message.Connection;
            connection.OnReceive += networkMessage;
            connection.OnTimeout += networkTimeout;
            connection.OnDisconnect += networkDisconnect;
            connection.Start();

            connection.Send(NetworkMessage.Encode(NetworkMessage.GetInfoRequest.Instance));

            propertyChangedAsync("Connected", "Synced", "Status");
        }
        private void disconnectProc()
        {
            connection.Dispose();
            connection = null;

            propertyChangedAsync("Connected", "Synced", "Status");
        }
#endif
    }
}